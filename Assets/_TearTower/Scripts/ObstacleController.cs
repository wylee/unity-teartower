﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {

    public Section m_section;
    const float YOffset = 20f;
    public bool m_isPopStarted = false;
    public bool m_isPoped = false;
    const float PopTime = 0.05f;
    public float m_lerpTime = 0;
    Vector3 m_startPosition;

	// Use this for initialization
	void Start () {
        StartCoroutine(CheckY());
    }

    public void Initialize()
    {
        GetComponent<Renderer>().enabled = true;
        transform.GetChild(0).GetComponent<Renderer>().enabled = false;
        transform.GetChild(1).GetComponent<Renderer>().enabled = false;
        transform.GetChild(2).GetComponent<Renderer>().enabled = false;

        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().Sleep();
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        m_isPopStarted = false;
        m_isPoped = false;
        m_lerpTime = 0;

        //StartCoroutine("obstacleUpdate");
    }

    private void OnEnable() {
        StartCoroutine("obstacleUpdate");
    }

    private void OnDisable() {
        StopAllCoroutines();
    }


    float target;

    IEnumerator obstacleUpdate(){
        while(true){
            if (m_isPopStarted==true&&m_isPoped==false) {
                switch (m_section) {
                    case Section.Back:
                        target=Mathf.Lerp(m_startPosition.z, -20.5f, m_lerpTime);
                        if (m_lerpTime<1) {
                            transform.position=new Vector3(transform.position.x, transform.position.y, target);
                            m_lerpTime+=Time.deltaTime/PopTime;
                        }
                        else {
                            target=-20.5f;
                            transform.position=new Vector3(transform.position.x, transform.position.y, target);
                            PopEnd();
                        }
                        break;
                    case Section.Right:
                        target=Mathf.Lerp(m_startPosition.x, 20.5f, m_lerpTime);
                        if (m_lerpTime<1) {
                            transform.position=new Vector3(target, transform.position.y, transform.position.z);
                            m_lerpTime+=Time.deltaTime/PopTime;
                        }
                        else {
                            target=20.5f;
                            transform.position=new Vector3(target, transform.position.y, transform.position.z);
                            PopEnd();
                        }
                        break;
                    case Section.Forward:
                        target=Mathf.Lerp(m_startPosition.z, 0.5f, m_lerpTime);
                        if (m_lerpTime<1) {
                            transform.position=new Vector3(transform.position.x, transform.position.y, target);
                            m_lerpTime+=Time.deltaTime/PopTime;
                        }
                        else {
                            target=0.5f;
                            transform.position=new Vector3(transform.position.x, transform.position.y, target);
                            PopEnd();
                        }
                        break;
                    case Section.Left:
                        target=Mathf.Lerp(m_startPosition.x, -0.5f, m_lerpTime);
                        if (m_lerpTime<1) {
                            transform.position=new Vector3(target, transform.position.y, transform.position.z);
                            m_lerpTime+=Time.deltaTime/PopTime;
                        }
                        else {
                            target=-0.5f;
                            transform.position=new Vector3(target, transform.position.y, transform.position.z);
                            PopEnd();
                        }
                        break;
                    }

                }
                yield return new WaitForFixedUpdate();
            }
    }


    public void PopStart()
    {
        m_isPopStarted = true;
        m_startPosition = transform.position;
    }

    public void PopEnd()
    {
        m_isPoped = true;
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().WakeUp();
    }

    IEnumerator CheckY()
    {
        while(transform.position.y > GameController._instance.m_character.transform.position.y - YOffset)
        {
            yield return new WaitForSeconds(0.5f);
        }
        StopCoroutine("obstacleUpdate");
        gameObject.SetActive(false);
    }

   
}
