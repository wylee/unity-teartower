﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclePatternBuilder : MonoBehaviour {

	//public static List<ObstaclePattern> BuildObstaclePattern()
 //   {
 //       List<ObstaclePattern> obstaclePatterns = new List<ObstaclePattern>();

 //       ObstaclePattern obstaclePattern;
 //       int difficulty = 0;
 //       //posX 1 ~ 19
 //       //posY 1 ~ 10

 //       // difficulty 0
 //       //obstaclePattern = new ObstaclePattern();
 //       //obstaclePattern.difficulty = 0;
 //       //obstaclePatterns.Add(obstaclePattern);

 //       difficulty = 0;
 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 19f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 17f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 15f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 13f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 11f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       // 5~ difficulty 1 start =========================
 //       difficulty = 1;

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 9f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       //obstaclePattern = new ObstaclePattern();
 //       //obstaclePattern.difficulty = difficulty;
 //       //obstaclePattern.Obstacles.Add(new Vector2( 7f, 1f ));
 //       //obstaclePatterns.Add(obstaclePattern);

 //       //obstaclePattern = new ObstaclePattern();
 //       //obstaclePattern.difficulty = difficulty;
 //       //obstaclePattern.Obstacles.Add(new Vector2( 5f, 1f ));
 //       //obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 17f, 2f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 15f, 2f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 13f, 2f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 11f, 2f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       // difficulty 1 end   =========================

 //       // 10~ difficulty 2 start =========================
 //       difficulty = 2;

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 17f, 1f ));
 //       obstaclePattern.Obstacles.Add(new Vector2( 11f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 15f, 1f ));
 //       obstaclePattern.Obstacles.Add(new Vector2( 9f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 17f, 5f ));
 //       obstaclePattern.Obstacles.Add(new Vector2( 11f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 15f, 5f ));
 //       obstaclePattern.Obstacles.Add(new Vector2( 9f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       // difficulty 2 end   =========================

 //       // 14~ difficulty 3 start =========================
 //       difficulty = 3;

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 17f, 3f ));
 //       obstaclePattern.Obstacles.Add(new Vector2( 13f, 1f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2(17f, 2f));
 //       obstaclePattern.Obstacles.Add(new Vector2(13f, 3f));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 14f, 1f ));
 //       obstaclePattern.Obstacles.Add(new Vector2( 13f, 5f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 11f, 1f ));
 //       obstaclePattern.Obstacles.Add(new Vector2( 10f, 5f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 13f, 1f ));
 //       obstaclePattern.Obstacles.Add(new Vector2( 14f, 3f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 17f, 2f ));
 //       obstaclePattern.Obstacles.Add(new Vector2( 11f, 2.5f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2( 15f, 2f ));
 //       obstaclePattern.Obstacles.Add(new Vector2( 9f, 2.5f ));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2(13f, 1f));
 //       obstaclePattern.Obstacles.Add(new Vector2(14f, 3f));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2(14f, 1f));
 //       obstaclePattern.Obstacles.Add(new Vector2(13f, 3f));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2(9f, 1f));
 //       obstaclePattern.Obstacles.Add(new Vector2(8f, 3f));
 //       obstaclePattern.Obstacles.Add(new Vector2(19f, 2f));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2(7f, 1f));
 //       obstaclePattern.Obstacles.Add(new Vector2(11f, 1f));
 //       obstaclePattern.Obstacles.Add(new Vector2(15f, 1f));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2(5f, 1f));
 //       obstaclePattern.Obstacles.Add(new Vector2(11f, 1f));
 //       obstaclePattern.Obstacles.Add(new Vector2(17f, 1f));
 //       obstaclePatterns.Add(obstaclePattern);

 //       obstaclePattern = new ObstaclePattern();
 //       obstaclePattern.difficulty = difficulty;
 //       obstaclePattern.Obstacles.Add(new Vector2(7f, 4f));
 //       obstaclePattern.Obstacles.Add(new Vector2(11f, 1f));
 //       obstaclePattern.Obstacles.Add(new Vector2(17f, 2f));
 //       obstaclePatterns.Add(obstaclePattern);

 //       // difficulty 3 end   =========================

 //       // difficulty 4 start =========================
 //       difficulty = 4;

        

 //       // difficulty 4 end   =========================

 //       return obstaclePatterns;
 //   }
}
