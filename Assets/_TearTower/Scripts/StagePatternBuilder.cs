﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StagePatternBuilder : MonoBehaviour {

    public static List<int[]> BuildStagePattern () {
        List<int[]> stagePattern = new List<int[]>();
        //stagePattern.Add(new int[] { 0, 0, 0, 0, 0, 0, 0, 1 });
        //stagePattern.Add(new int[] { 0, 0, 0, 0, 0, 0, 1, 1 });
        //stagePattern.Add(new int[] { 0, 0, 0, 0, 0, 1, 1, 1 });
        //stagePattern.Add(new int[] { 0, 0, 0, 0, 1, 1, 1, 1 });
        //stagePattern.Add(new int[] { 0, 0, 0, 1, 1, 1, 1, 1 });
        //stagePattern.Add(new int[] { 0, 0, 0, 0, 1, 1, 1, 2 });
        //stagePattern.Add(new int[] { 0, 0, 0, 1, 1, 1, 1, 1 });
        //stagePattern.Add(new int[] { 0, 0, 0, 1, 1, 1, 1, 2 });
        //stagePattern.Add(new int[] { 0, 0, 0, 1, 2, 1, 1, 2 });
        //stagePattern.Add(new int[] { 0, 0, 0, 1, 1, 1, 1, 2 });
        //stagePattern.Add(new int[] { 0, 0, 1, 1, 2, 1, 2, 2 });
        //stagePattern.Add(new int[] { 0, 0, 0, 1, 1, 1, 1, 2 });
        //stagePattern.Add(new int[] { 0, 1, 2, 1, 2, 1, 2, 2 });
        //stagePattern.Add(new int[] { 0, 0, 0, 1, 2, 1, 1, 2 });
        //stagePattern.Add(new int[] { 0, 0, 0, 1, 2, 1, 1, 2 });
        stagePattern.Add(new int[] { 1, 1, 2, 2, 1, 2, 2, 2 });
        stagePattern.Add(new int[] { 1, 1, 1, 1, 2, 1, 1, 3 });
        stagePattern.Add(new int[] { 1, 1, 1, 1, 2, 1, 1, 2 });
        stagePattern.Add(new int[] { 1, 1, 1, 2, 2, 1, 1, 2 });
        stagePattern.Add(new int[] { 1, 1, 1, 2, 2, 1, 1, 3 });
        stagePattern.Add(new int[] { 1, 1, 1, 2, 2, 2, 2, 2 });
        stagePattern.Add(new int[] { 1, 1, 2, 2, 2, 2, 3, 3 });
        stagePattern.Add(new int[] { 1, 2, 2, 2, 2, 3, 3, 3 });
        
        stagePattern.Add(new int[] { 1, 1, 2, 1, 2, 1, 2, 2 });
        stagePattern.Add(new int[] { 1, 2, 1, 1, 2, 1, 2, 2 });
        stagePattern.Add(new int[] { 1, 1, 2, 2, 1, 2, 2, 2 });
        stagePattern.Add(new int[] { 1, 3, 1, 1, 2, 1, 2, 3 });
        stagePattern.Add(new int[] { 1, 2, 3, 1, 2, 1, 2, 2 });
        stagePattern.Add(new int[] { 2, 1, 1, 3, 2, 1, 2, 2 });
        stagePattern.Add(new int[] { 2, 1, 1, 2, 3, 1, 2, 3 });
        stagePattern.Add(new int[] { 2, 1, 3, 2, 2, 2, 2, 2 });
        stagePattern.Add(new int[] { 2, 3, 2, 2, 2, 2, 3, 3 });
        stagePattern.Add(new int[] { 2, 3, 2, 3, 2, 3, 3, 3 });

        stagePattern.Add(new int[] { 2, 2, 2, 2, 3, 3, 3, 3 });
        //stagePattern.Add(new int[] { 0, 1, 2, 2, 2, 2, 2, 3 }); //0

        //stagePattern.Add(new int[] { 2, 2, 2, 2, 2, 2, 2, 3 }); //1
        //stagePattern.Add(new int[] { 2, 2, 2, 2, 2, 2, 3, 3 }); //2
        //stagePattern.Add(new int[] { 2, 2, 2, 2, 2, 3, 3, 3 }); //3
        stagePattern.Add(new int[] { 2, 2, 2, 2, 3, 3, 3, 3 }); //4
        stagePattern.Add(new int[] { 2, 2, 2, 3, 3, 3, 3, 3 }); //5
        stagePattern.Add(new int[] { 2, 2, 2, 2, 3, 3, 3, 4 }); //6
        stagePattern.Add(new int[] { 2, 2, 2, 3, 3, 3, 3, 4 }); //7
        stagePattern.Add(new int[] { 2, 2, 3, 3, 3, 3, 3, 4 }); //8
        stagePattern.Add(new int[] { 2, 3, 3, 3, 3, 3, 3, 4 }); //9
        stagePattern.Add(new int[] { 3, 3, 3, 3, 3, 3, 3, 4 }); //10
        // 21~40
        stagePattern.Add(new int[] { 3, 3, 3, 3, 3, 3, 3, 4 }); //1
        stagePattern.Add(new int[] { 3, 3, 3, 3, 3, 3, 4, 4 }); //2
        stagePattern.Add(new int[] { 3, 3, 3, 3, 3, 4, 4, 4 }); //3
        stagePattern.Add(new int[] { 3, 3, 3, 3, 4, 4, 4, 4 }); //4
        stagePattern.Add(new int[] { 3, 3, 3, 4, 4, 4, 4, 4 }); //5
        stagePattern.Add(new int[] { 3, 3, 3, 3, 4, 4, 4, 5 }); //6
        stagePattern.Add(new int[] { 3, 3, 3, 4, 4, 4, 4, 5 }); //7
        stagePattern.Add(new int[] { 3, 3, 4, 4, 4, 4, 4, 5 }); //8
        stagePattern.Add(new int[] { 3, 4, 4, 4, 4, 4, 4, 5 }); //9
        stagePattern.Add(new int[] { 4, 4, 4, 4, 4, 4, 4, 5 }); //10

        stagePattern.Add(new int[] { 4, 4, 4, 4, 4, 4, 4, 5 }); //1
        stagePattern.Add(new int[] { 4, 4, 4, 4, 4, 4, 5, 5 }); //2
        stagePattern.Add(new int[] { 4, 4, 4, 4, 4, 5, 5, 5 }); //3
        stagePattern.Add(new int[] { 4, 4, 4, 4, 5, 5, 5, 5 }); //4
        stagePattern.Add(new int[] { 4, 4, 4, 5, 5, 5, 5, 5 }); //5
        stagePattern.Add(new int[] { 4, 4, 4, 4, 5, 5, 5, 6 }); //6
        stagePattern.Add(new int[] { 4, 4, 4, 5, 5, 5, 5, 6 }); //7
        stagePattern.Add(new int[] { 4, 4, 5, 5, 5, 5, 5, 6 }); //8
        stagePattern.Add(new int[] { 4, 5, 5, 5, 5, 5, 5, 6 }); //9
        stagePattern.Add(new int[] { 5, 5, 5, 5, 5, 5, 5, 6 }); //10

        stagePattern.Add(new int[] { 5, 5, 5, 5, 5, 5, 5, 6 }); //1
        stagePattern.Add(new int[] { 5, 5, 5, 5, 5, 5, 6, 6 }); //2
        stagePattern.Add(new int[] { 5, 5, 5, 5, 5, 6, 6, 6 }); //3
        stagePattern.Add(new int[] { 5, 5, 5, 5, 6, 6, 6, 6 }); //4
        stagePattern.Add(new int[] { 5, 5, 5, 6, 6, 6, 6, 6 }); //5
        stagePattern.Add(new int[] { 5, 5, 5, 5, 6, 6, 6, 7 }); //6
        stagePattern.Add(new int[] { 5, 5, 5, 6, 6, 6, 6, 7 }); //7
        stagePattern.Add(new int[] { 5, 5, 6, 6, 6, 6, 6, 7 }); //8
        stagePattern.Add(new int[] { 5, 6, 6, 6, 6, 6, 6, 7 }); //9
        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 6, 6, 7 }); //10

        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 6, 6, 7 }); //1
        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 6, 7, 7 }); //2
        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 7, 7, 7 }); //3
        stagePattern.Add(new int[] { 6, 6, 6, 6, 7, 7, 7, 7 }); //4
        stagePattern.Add(new int[] { 6, 6, 6, 6, 7, 7, 7, 7 });
        stagePattern.Add(new int[] { 6, 6, 6, 7, 7, 7, 7, 7 }); //5
        stagePattern.Add(new int[] { 6, 6, 6, 6, 7, 7, 7, 8 }); //6
        stagePattern.Add(new int[] { 6, 6, 6, 7, 7, 7, 7, 8 }); //7
        stagePattern.Add(new int[] { 6, 6, 7, 7, 7, 7, 7, 8 }); //8
        stagePattern.Add(new int[] { 6, 6, 7, 7, 7, 7, 7, 8 });
        stagePattern.Add(new int[] { 6, 7, 7, 7, 7, 7, 7, 8 }); //9
        stagePattern.Add(new int[] { 7, 7, 7, 7, 7, 7, 7, 8 }); //10

        stagePattern.Add(new int[] { 7, 7, 7, 7, 7, 7, 7, 8 }); //1
        stagePattern.Add(new int[] { 7, 7, 7, 7, 7, 7, 8, 8 }); //2
        stagePattern.Add(new int[] { 7, 7, 7, 7, 7, 8, 8, 8 }); //3
        stagePattern.Add(new int[] { 7, 7, 7, 7, 7, 8, 8, 8 });
        stagePattern.Add(new int[] { 7, 7, 7, 7, 8, 8, 8, 8 }); //4
        stagePattern.Add(new int[] { 7, 7, 7, 8, 8, 8, 8, 8 }); //5
        stagePattern.Add(new int[] { 7, 7, 7, 7, 8, 8, 8, 9 }); //6
        stagePattern.Add(new int[] { 7, 7, 7, 7, 8, 8, 8, 9 });
        stagePattern.Add(new int[] { 7, 7, 7, 8, 8, 8, 8, 9 }); //7
        stagePattern.Add(new int[] { 7, 7, 8, 8, 8, 8, 8, 9 }); //8
        stagePattern.Add(new int[] { 7, 8, 8, 8, 8, 8, 8, 9 }); //9
        stagePattern.Add(new int[] { 7, 8, 8, 8, 8, 8, 8, 9 });
        stagePattern.Add(new int[] { 8, 8, 8, 8, 8, 8, 8, 9 }); //10

        stagePattern.Add(new int[] { 8, 8, 8, 8, 8, 8, 8, 9 }); //1
        stagePattern.Add(new int[] { 8, 8, 8, 8, 8, 8, 9, 9 }); //2
        stagePattern.Add(new int[] { 8, 8, 8, 8, 8, 8, 9, 9 });
        stagePattern.Add(new int[] { 8, 8, 8, 8, 8, 9, 9, 9 }); //3
        stagePattern.Add(new int[] { 8, 8, 8, 8, 9, 9, 9, 9 }); //4
        stagePattern.Add(new int[] { 8, 8, 8, 8, 9, 9, 9, 9 });
        stagePattern.Add(new int[] { 8, 8, 8, 9, 9, 9, 9, 9 }); //5
        stagePattern.Add(new int[] { 8, 8, 8, 8, 9, 9, 9, 10 }); //6
        stagePattern.Add(new int[] { 8, 8, 8, 8, 9, 9, 9, 10 });
        stagePattern.Add(new int[] { 8, 8, 8, 9, 9, 9, 9, 10 }); //7
        stagePattern.Add(new int[] { 8, 8, 9, 9, 9, 9, 9, 10 }); //8
        stagePattern.Add(new int[] { 8, 8, 9, 9, 9, 9, 9, 10 });
        stagePattern.Add(new int[] { 8, 9, 9, 9, 9, 9, 9, 10 }); //9
        stagePattern.Add(new int[] { 8, 9, 9, 9, 9, 9, 9, 10 });
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 9, 9, 10 }); //10

        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 9, 9, 10 }); //1
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 9, 9, 10 }); //1
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 9, 10, 10 }); //2
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 9, 10, 10 }); //2
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 10, 10, 10 }); //3
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 10, 10, 10 }); //3
        stagePattern.Add(new int[] { 9, 9, 9, 9, 10, 10, 10, 10 }); //4
        stagePattern.Add(new int[] { 9, 9, 9, 9, 10, 10, 10, 10 }); //4
        stagePattern.Add(new int[] { 9, 9, 9, 10, 10, 10, 10, 10 }); //5
        stagePattern.Add(new int[] { 9, 9, 9, 10, 10, 10, 10, 10 }); //5
        stagePattern.Add(new int[] { 9, 9, 9, 9, 10, 10, 10, 11 }); //6
        stagePattern.Add(new int[] { 9, 9, 9, 9, 10, 10, 10, 11 }); //6
        stagePattern.Add(new int[] { 9, 9, 9, 10, 10, 10, 10, 11 }); //7
        stagePattern.Add(new int[] { 9, 9, 9, 10, 10, 10, 10, 11 }); //7
        stagePattern.Add(new int[] { 9, 9, 10, 10, 10, 10, 10, 11 }); //8
        stagePattern.Add(new int[] { 9, 9, 10, 10, 10, 10, 10, 11 }); //8
        stagePattern.Add(new int[] { 9, 10, 10, 10, 10, 10, 10, 11 }); //9
        stagePattern.Add(new int[] { 9, 10, 10, 10, 10, 10, 10, 11 }); //9
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 10, 11 }); //10
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 10, 11 }); //10

        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 10, 11 }); //1
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 10, 11 }); //1
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 10, 11 }); //1
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 11, 11 }); //2
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 11, 11 }); //2
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 11, 11 }); //2
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 11, 11, 11 }); //3
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 11, 11, 11 }); //3
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 11, 11, 11 }); //3
        stagePattern.Add(new int[] { 10, 10, 10, 10, 11, 11, 11, 11 }); //4
        stagePattern.Add(new int[] { 10, 10, 10, 10, 11, 11, 11, 11 }); //4
        stagePattern.Add(new int[] { 10, 10, 10, 10, 11, 11, 11, 11 }); //4
        stagePattern.Add(new int[] { 10, 10, 10, 11, 11, 11, 11, 11 }); //5
        stagePattern.Add(new int[] { 10, 10, 10, 11, 11, 11, 11, 11 }); //5
        stagePattern.Add(new int[] { 10, 10, 10, 11, 11, 11, 11, 11 }); //5
        stagePattern.Add(new int[] { 10, 10, 10, 10, 11, 11, 11, 12 }); //6
        stagePattern.Add(new int[] { 10, 10, 10, 10, 11, 11, 11, 12 }); //6
        stagePattern.Add(new int[] { 10, 10, 10, 10, 11, 11, 11, 12 }); //6
        stagePattern.Add(new int[] { 10, 10, 10, 11, 11, 11, 11, 12 }); //7
        stagePattern.Add(new int[] { 10, 10, 10, 11, 11, 11, 11, 12 }); //7
        stagePattern.Add(new int[] { 10, 10, 10, 11, 11, 11, 11, 12 }); //7
        stagePattern.Add(new int[] { 10, 10, 11, 11, 11, 11, 11, 12 }); //8
        stagePattern.Add(new int[] { 10, 10, 11, 11, 11, 11, 11, 12 }); //8
        stagePattern.Add(new int[] { 10, 10, 11, 11, 11, 11, 11, 12 }); //8
        stagePattern.Add(new int[] { 10, 11, 11, 11, 11, 11, 11, 12 }); //9
        stagePattern.Add(new int[] { 10, 11, 11, 11, 11, 11, 11, 12 }); //9
        stagePattern.Add(new int[] { 10, 11, 11, 11, 11, 11, 11, 12 }); //9
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 11, 12 }); //10
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 11, 12 }); //10
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 11, 12 }); //10

        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 11, 12 }); //1
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 11, 12 }); //1
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 11, 12 }); //1
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 11, 12 }); //1
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 12, 12 }); //2
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 12, 12 }); //2
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 12, 12 }); //2
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 11, 12, 12 }); //2
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 12, 12, 12 }); //3
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 12, 12, 12 }); //3
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 12, 12, 12 }); //3
        stagePattern.Add(new int[] { 11, 11, 11, 11, 11, 12, 12, 12 }); //3
        stagePattern.Add(new int[] { 11, 11, 11, 11, 12, 12, 12, 12 }); //4
        stagePattern.Add(new int[] { 11, 11, 11, 11, 12, 12, 12, 12 }); //4
        stagePattern.Add(new int[] { 11, 11, 11, 11, 12, 12, 12, 12 }); //4
        stagePattern.Add(new int[] { 11, 11, 11, 11, 12, 12, 12, 12 }); //4
        stagePattern.Add(new int[] { 11, 11, 11, 12, 12, 12, 12, 12 }); //5
        stagePattern.Add(new int[] { 11, 11, 11, 12, 12, 12, 12, 12 }); //5
        stagePattern.Add(new int[] { 11, 11, 11, 12, 12, 12, 12, 12 }); //5
        stagePattern.Add(new int[] { 11, 11, 11, 12, 12, 12, 12, 12 }); //5
        stagePattern.Add(new int[] { 11, 11, 11, 11, 12, 12, 12, 13 }); //6
        stagePattern.Add(new int[] { 11, 11, 11, 11, 12, 12, 12, 13 }); //6
        stagePattern.Add(new int[] { 11, 11, 11, 11, 12, 12, 12, 13 }); //6
        stagePattern.Add(new int[] { 11, 11, 11, 11, 12, 12, 12, 13 }); //6
        stagePattern.Add(new int[] { 11, 11, 11, 12, 12, 12, 12, 13 }); //7
        stagePattern.Add(new int[] { 11, 11, 11, 12, 12, 12, 12, 13 }); //7
        stagePattern.Add(new int[] { 11, 11, 11, 12, 12, 12, 12, 13 }); //7
        stagePattern.Add(new int[] { 11, 11, 11, 12, 12, 12, 12, 13 }); //7
        stagePattern.Add(new int[] { 11, 11, 12, 12, 12, 12, 12, 13 }); //8
        stagePattern.Add(new int[] { 11, 11, 12, 12, 12, 12, 12, 13 }); //8
        stagePattern.Add(new int[] { 11, 11, 12, 12, 12, 12, 12, 13 }); //8
        stagePattern.Add(new int[] { 11, 11, 12, 12, 12, 12, 12, 13 }); //8
        stagePattern.Add(new int[] { 11, 12, 12, 12, 12, 12, 12, 13 }); //9
        stagePattern.Add(new int[] { 11, 12, 12, 12, 12, 12, 12, 13 }); //9
        stagePattern.Add(new int[] { 11, 12, 12, 12, 12, 12, 12, 13 }); //9
        stagePattern.Add(new int[] { 11, 12, 12, 12, 12, 12, 12, 13 }); //9
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 13 }); //10
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 13 }); //10
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 13 }); //10
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 13 }); //10

        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 13 }); //1
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 13 }); //1
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 13 }); //1
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 13 }); //1
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 13 }); //1
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 13, 13 }); //2
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 13, 13 }); //2
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 13, 13 }); //2
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 13, 13 }); //2
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 13, 13 }); //2
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 13, 13, 13 }); //3
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 13, 13, 13 }); //3
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 13, 13, 13 }); //3
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 13, 13, 13 }); //3
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 13, 13, 13 }); //3
        stagePattern.Add(new int[] { 12, 12, 12, 12, 13, 13, 13, 13 }); //4
        stagePattern.Add(new int[] { 12, 12, 12, 12, 13, 13, 13, 13 }); //4
        stagePattern.Add(new int[] { 12, 12, 12, 12, 13, 13, 13, 13 }); //4
        stagePattern.Add(new int[] { 12, 12, 12, 12, 13, 13, 13, 13 }); //4
        stagePattern.Add(new int[] { 12, 12, 12, 12, 13, 13, 13, 13 }); //4
        stagePattern.Add(new int[] { 12, 12, 12, 13, 13, 13, 13, 13 }); //5
        stagePattern.Add(new int[] { 12, 12, 12, 13, 13, 13, 13, 13 }); //5
        stagePattern.Add(new int[] { 12, 12, 12, 13, 13, 13, 13, 13 }); //5
        stagePattern.Add(new int[] { 12, 12, 12, 13, 13, 13, 13, 13 }); //5
        stagePattern.Add(new int[] { 12, 12, 12, 13, 13, 13, 13, 13 }); //5

        List<int[]> resultStagePattern = new List<int[]>();


        for(int i = 0; i < stagePattern.Count; i++){
            resultStagePattern.Add(stagePattern[i]);
        }

        return resultStagePattern;

        /*
        for (int i=0; i<20; i++)
        {
            resultStagePattern.Add(stagePattern[i]);
        }

        for (int i = 20; i < 60; i++)
        {
            for (int k = 0; k < 2; k++)
            {
                resultStagePattern.Add(stagePattern[i]);
            }
        }

        for (int i = 60; i < 100; i++)
        {
            for (int k = 0; k < 3; k++)
            {
                resultStagePattern.Add(stagePattern[i]);
            }
        }

        for (int i = 100; i < 150; i++)
        {
            for (int k = 0; k < 4; k++)
            {
                resultStagePattern.Add(stagePattern[i]);
            }
        }

        for (int i = 150; i < stagePattern.Count; i++)
        {
            for (int k = 0; k < 5; k++)
            {
                resultStagePattern.Add(stagePattern[i]);
            }
        }
        */

       // return resultStagePattern;
    }
}
