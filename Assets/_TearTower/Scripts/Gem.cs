﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour{

    public void Initialize()
    {
        GetComponent<Renderer>().enabled = false;
    }

    public void SetEnable(){
        this.GetComponent<Renderer>().enabled = true;
    }


    private void OnTriggerEnter(Collider other){
        if (GetComponent<Renderer>().enabled && other.name.Equals("Character")){
            int gem= PlayerPrefs.GetInt(Values.USER_GET_GEMS);
            ++gem;
            PlayerPrefs.SetInt(Values.USER_GET_GEMS,gem);
            PlayerPrefs.Save();
            this.GetComponent<Renderer>().enabled = false;
            this.GetComponent<ParticleSystem>().Play();
            GameController._instance.PlayGemSound();
        }
    }
}
