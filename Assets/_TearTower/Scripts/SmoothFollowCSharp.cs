﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden

using UnityEngine;
using System.Collections;

// Place the script in the Camera-Control group in the component menu
[AddComponentMenu("Camera-Control/Smooth Follow CSharp")]

public class SmoothFollowCSharp : MonoBehaviour
{
    /*
    This camera smoothes out rotation around the y-axis and height.
    Horizontal Distance to the target is always fixed.

    There are many different ways to smooth the rotation but doing it this way gives you a lot of control over how the camera behaves.

    For every of those smoothed values we calculate the wanted value and the current value.
    Then we smooth it using the Lerp function.
    Then we apply the smoothed values to the transform's position.
    */

    // The target we are following
    public Transform target;
    // The distance in the x-z plane to the target
    public float distance = 10.0f;
    // the height we want the camera to be above the target
    public float height = 5.0f;
    // How much we 
    public float heightDamping = 2.0f;
    public float rotationDamping = 3.0f;

    public float wantedRotationAngleOffset = 0f;
    float lookAtOffsetFactor = 0f;
    const float lookAtOffsetFactorMax = 4.0f;
    const float lookAtOffsetFactorDamping = 0.05f;
    const float lookAtOffsetUp = 3.0f;
    //Section lastSection = Section.Left;
    Section lastSection = Section.Back;
    bool test = false;


    public void InitializeCamera()
    {
        if(!GameController._instance.IsRevived())
        {
            transform.position = new Vector3(-7.8f, 1.8f, -27.3f);
            transform.rotation = Quaternion.Euler(346.3f, 46.8f, 0f);
        }
        lookAtOffsetFactor = 0f;
        lastSection = Section.Back;
        isStarted = false;
    }

    const float WantedRotationAngleAdjust = 20f;
    bool isStarted = false;

    void Update()
    {
    
        // Early out if we don't have a target
        if (!target)
            return;
    
        // Calculate the current rotation angles
        //float wantedRotationAngle = target.eulerAngles.y;
        float wantedRotationAngle = 90f + wantedRotationAngleOffset;
        //Vector3 lookAtOffset = Vector3.right * Mathf.Min(lookAtOffsetFactor, remainingDistance);
        Vector3 lookAtOffset = Vector3.right * lookAtOffsetFactor;
    
        Section section = target.gameObject.GetComponent<CharacterControl>().GetSection();
        
        if(isStarted == false)
        {
            if(section == target.gameObject.GetComponent<CharacterControl>().GetStartSection())
            {
                isStarted = true;
            } else
            {
                section = target.gameObject.GetComponent<CharacterControl>().GetStartSection();
            }
        }
    
        float remainingDistance = target.gameObject.GetComponent<CharacterControl>().GetRemainingDistance();
        float percent1 = ((21f - lookAtOffsetFactorMax) - (remainingDistance - lookAtOffsetFactorMax)) / (21f - lookAtOffsetFactorMax);
        float percent2 = (lookAtOffsetFactorMax - remainingDistance) / lookAtOffsetFactorMax;
    
        switch (section)
        {
            case Section.Back:
                //if (lastSection == Section.Left)
                //{
                //    lookAtOffsetFactor = 0f;
                //    lookAtOffset = Vector3.zero;
                //}
                //lastSection = Section.Back;
    
                //wantedRotationAngle = 0f + wantedRotationAngleOffset; //90f;
    
                //wantedRotationAngle = Mathf.Lerp(0f, 0f - WantedRotationAngleAdjust, target.gameObject.GetComponent<CharacterControl>().GetProgressPercentage()) + wantedRotationAngleOffset; //90f;
                //lookAtOffset = Vector3.right * Mathf.Min(lookAtOffsetFactor, remainingDistance);
                if (lookAtOffsetFactorMax < remainingDistance)
                {
                    if (lookAtOffsetFactor < lookAtOffsetFactorMax && isStarted)
                    {
                        lookAtOffsetFactor += lookAtOffsetFactorDamping;
                    }
                    wantedRotationAngle = Mathf.Lerp(0f, 0f - WantedRotationAngleAdjust, percent1) + wantedRotationAngleOffset; //90f;
                } else
                {
                    wantedRotationAngle = Mathf.Lerp(0f - WantedRotationAngleAdjust, 0f - 90f, percent2) + wantedRotationAngleOffset; //90f;
                    lookAtOffsetFactor = remainingDistance;
                }
                lookAtOffset = Vector3.right * lookAtOffsetFactor;
    
                //lookAtOffset = Vector3.right * lookAtOffsetFactor;
                break;
            case Section.Right:
                //if (lastSection == Section.Back)
                //{
                //    lookAtOffsetFactor = 0f;
                //    lookAtOffset = Vector3.zero;
                //}
                //lastSection = Section.Right;
    
                //wantedRotationAngle = 270f + wantedRotationAngleOffset; //0f;
    
                //wantedRotationAngle = Mathf.Lerp(270f, 270f - WantedRotationAngleAdjust, target.gameObject.GetComponent<CharacterControl>().GetProgressPercentage()) + wantedRotationAngleOffset; //90f;
                //lookAtOffset = Vector3.forward * Mathf.Min(lookAtOffsetFactor, remainingDistance);
                if (lookAtOffsetFactorMax < remainingDistance)
                {
                    if (lookAtOffsetFactor < lookAtOffsetFactorMax && isStarted)
                    {
                        lookAtOffsetFactor += lookAtOffsetFactorDamping;
                    }
                    wantedRotationAngle = Mathf.Lerp(270f, 270f - WantedRotationAngleAdjust, percent1) + wantedRotationAngleOffset; //90f;
                }
                else
                {
                    wantedRotationAngle = Mathf.Lerp(270f - WantedRotationAngleAdjust, 270f - 90f, percent2) + wantedRotationAngleOffset; //90f;
                    lookAtOffsetFactor = remainingDistance;
                }
                lookAtOffset = Vector3.forward * lookAtOffsetFactor;
    
                //lookAtOffset = Vector3.forward * lookAtOffsetFactor;
                break;
            case Section.Forward:
                //if (lastSection == Section.Right)
                //{
                //    lookAtOffsetFactor = 0f;
                //    lookAtOffset = Vector3.zero;
                //}
                //lastSection = Section.Forward;
    
                //wantedRotationAngle = 180f + wantedRotationAngleOffset; //270f;
    
                //wantedRotationAngle = Mathf.Lerp(180f, 180f - WantedRotationAngleAdjust, target.gameObject.GetComponent<CharacterControl>().GetProgressPercentage()) + wantedRotationAngleOffset; //90f;
                //lookAtOffset = Vector3.left * Mathf.Min(lookAtOffsetFactor, remainingDistance);
                if (lookAtOffsetFactorMax < remainingDistance)
                {
                    if (lookAtOffsetFactor < lookAtOffsetFactorMax && isStarted)
                    {
                        lookAtOffsetFactor += lookAtOffsetFactorDamping;
                    }
                    wantedRotationAngle = Mathf.Lerp(180f, 180f - WantedRotationAngleAdjust, percent1) + wantedRotationAngleOffset; //90f;
                }
                else
                {
                    wantedRotationAngle = Mathf.Lerp(180f - WantedRotationAngleAdjust, 180f - 90f, percent2) + wantedRotationAngleOffset; //90f;
                    lookAtOffsetFactor = remainingDistance;
                }
                lookAtOffset = Vector3.left * lookAtOffsetFactor;
    
                //lookAtOffset = Vector3.left * lookAtOffsetFactor;
                break;
            case Section.Left:
                //if (lastSection == Section.Forward)
                //{
                //    lookAtOffsetFactor = 0f;
                //    lookAtOffset = Vector3.zero;
                //}
                //lastSection = Section.Left;
    
                //wantedRotationAngle = 90f + wantedRotationAngleOffset; //180f;
    
                //wantedRotationAngle = Mathf.Lerp(90f, 90f - WantedRotationAngleAdjust, target.gameObject.GetComponent<CharacterControl>().GetProgressPercentage()) + wantedRotationAngleOffset; //90f;
                //lookAtOffset = Vector3.back * Mathf.Min(lookAtOffsetFactor, remainingDistance);
                if (lookAtOffsetFactorMax < remainingDistance)
                {
                    if (lookAtOffsetFactor < lookAtOffsetFactorMax && isStarted)
                    {
                        lookAtOffsetFactor += lookAtOffsetFactorDamping;
                    }
                    wantedRotationAngle = Mathf.Lerp(90f, 90f - WantedRotationAngleAdjust, percent1) + wantedRotationAngleOffset; //90f;
                }
                else
                {
                    wantedRotationAngle = Mathf.Lerp(90f - WantedRotationAngleAdjust, 90f - 90f, percent2) + wantedRotationAngleOffset; //90f;
                    lookAtOffsetFactor = remainingDistance;
                }
                lookAtOffset = Vector3.back * lookAtOffsetFactor;
    
                //lookAtOffset = Vector3.back * lookAtOffsetFactor;
                break;
        }
        lookAtOffset += Vector3.up * lookAtOffsetUp;
        
        //if (lookAtOffsetFactor < lookAtOffsetFactorMax && isStarted)
        //    lookAtOffsetFactor += lookAtOffsetFactorDamping;
    
        float wantedHeight = target.position.y + height;
        float currentRotationAngle = transform.eulerAngles.y;
        float currentHeight = transform.position.y;
    
        // Damp the rotation around the y-axis
        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
    
        // Damp the height
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);
    
        // Convert the angle into a rotation
        Quaternion currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
    
        // Set the position of the camera on the x-z plane to:
        // distance meters behind the target
        transform.position = target.position + lookAtOffset;
        //transform.position = target.position;
        //transform.position -= Vector3.forward * distance;
        transform.position -= currentRotation * Vector3.forward * distance;
        //transform.position -= currentRotation * Vector3.forward * (distance + target.gameObject.GetComponent<CharacterControl>().GetHeight());
    
        // Set the height of the camera
        transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);
    
        // Always look at the target
        transform.LookAt(target.position + lookAtOffset);
        //transform.LookAt(target.position);
        //transform.LookAt(target);
    
        if(test == false)
        {
            //Debug.Log("camera : "+transform.position.ToString());
            //Debug.Log("camera rot: " + transform.rotation.eulerAngles.ToString());
            test = true;
        }
    }
}