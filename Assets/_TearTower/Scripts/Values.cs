﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Values {

    public const string stage = "Stage";
    public const string canvas = "Canvas";
    public const string towerPiece = "TowerPiece";
    public const string finish = "Finish";
    public const string obstacle = "Obstacle";
    public const string obstacles = "Obstacles";
    public const string character = "Character";
    public const string pillar = "Pillar";
    public const string plane = "Plane";
    public const string stairs = "Stairs";
    public const string sound = "Sound";
    public const string notification = "Notification";
    public const string backgroundTowers = "Background Towers";
    public const string gem = "Gem";
    public const string characterIndex = "CharacterIndex";
    public const string noAds = "NoAds";
    public const string achievementDied = "AchievementDied";
    public const string achievementGem = "AchievementGem";
    public const string firstLoadDateTime = "FirstLoadDateTime";
    public const string day = "Day";

    public const string selectIndex = "SelectIndex";
    public const string unlock = "Unlock";
	public const string boostbox = "boostbox";
	public const string slowbox = "slowbox";

    public const string unlockCharacter = "UNLOCK_C";
    public const string NOADS_START = "NOADS_START";
    public const string NOADS_END = "NOADS_END";
    public const string NOADS_REWARD = "NOADS_REWARD";
    public const string ADS_COUNT = "VC_";
    public const string USER_GET_GEMS = "GET_GEM";
}
