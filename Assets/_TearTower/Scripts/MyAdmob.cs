﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;


public class MyAdmob : MonoBehaviour{
    // Start is called before the first frame update

    public bool isNetworkError = false;

    public bool isDebugMode = false;

    public static MyAdmob myAdmob = null;
    private void Awake() {

        if (myAdmob != null){
            Destroy(this.gameObject);
            return;
        }
        
        myAdmob=this;
        DontDestroyOnLoad(this.gameObject);
    }

    public static RewardedAd rewardBasedVideo = null;
    public static InterstitialAd interstitial = null;
    public static BannerView bannerView = null;
    public static RewardedAd doubleRewardVideo = null;
    public static RewardedAd mainChanceRewarded = null;

    //string appId = string.Empty;

    string bannerId = string.Empty;
    string interstialId = string.Empty;
    string rewardDieId = string.Empty;
    string rewardMainId = string.Empty;
    string rewardDoubleId = string.Empty;
    // Use this for initialization

    private void Start() {
        //MobileAds.Initialize(initStatus => { });

#if UNITY_ANDROID
        rewardMainId="ca-app-pub-3779186367163749/4690687034";
        if (isDebugMode) {
            rewardMainId="ca-app-pub-3940256099942544/5224354917";
        }
#elif UNITY_IPHONE
        rewardMainId = "ca-app-pub-3779186367163749/3108698974";
        if(isDebugMode){
            rewardMainId = "ca-app-pub-3940256099942544/1712485313";
        }
#endif
        // 죽었을때 광고.

#if UNITY_ANDROID
        rewardDieId="ca-app-pub-3779186367163749/9751442023";

        if (isDebugMode) {
            rewardDieId="ca-app-pub-3940256099942544/5224354917";
        }

        rewardDieId = "ca-app-pub-3940256099942544/1712485313";
#elif UNITY_IPHONE
        rewardDieId = "ca-app-pub-3779186367163749/2639238767";
        if(isDebugMode)
        {
            rewardDieId = "ca-app-pub-3940256099942544/1712485313";
        }

#endif

        // 클리어 더블 보상
#if UNITY_ANDROID

        rewardDoubleId="ca-app-pub-3779186367163749/7125278681";

        if (isDebugMode) {
            rewardDoubleId="ca-app-pub-3940256099942544/5224354917";
        }

#elif UNITY_IPHONE
        rewardDoubleId = "ca-app-pub-3779186367163749/7699993758";
        if(isDebugMode){
            rewardDoubleId = "ca-app-pub-3940256099942544/1712485313";
        }

#endif
        //mainChanceRewarded = requestVideoAds(rewardMainId, "MAIN_CHANCE");
        //rewardBasedVideo = requestVideoAds(rewardDieId, "DIE_CHANCE");
        //doubleRewardVideo = requestVideoAds(rewardDoubleId, "DOUBLE");
        //RequestInterstitial();
        //RequestBanner();
        //hideBanner();
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdFailedToLoadEventArgs args){
        MonoBehaviour.print(
            "HandleRewardedAdFailedToLoad event received with message: "
                             + args.LoadAdError);
    }
    public bool isLoadedMainChance() {
        if (mainChanceRewarded == null)  return false;
        return mainChanceRewarded.IsLoaded();
    }

    public bool isLoadDieChance() {
        if (rewardBasedVideo == null) return false;
         return rewardBasedVideo.IsLoaded();
    }

    public bool isDoubleRewardVideo() {
        if (doubleRewardVideo == null)  return false;
        return doubleRewardVideo.IsLoaded();

    }

    public bool isLoadedInterstitial() {
        if (interstitial==null) return false;
        return interstitial.IsLoaded();
    }

    public RewardedAd requestVideoAds(string appid, string target) {
        return null;
    }

    public void RequestBanner() {


        if (isDebugMode) {
            // Debug Mode.
#if UNITY_ANDROID
            bannerId="ca-app-pub-3940256099942544/6300978111";
#elif UNITY_IPHONE
        bannerId= "ca-app-pub-3940256099942544/2934735716";
#endif
        }
        else {
#if UNITY_ANDROID
            bannerId="ca-app-pub-3779186367163749/8526791625";
#elif UNITY_IPHONE
            bannerId = "ca-app-pub-3779186367163749/4395974924";
#endif
        }
        //bannerView = new BannerView(bannerId, AdSize.Banner, AdPosition.Top);

        //AdRequest req = new AdRequest.Builder().Build();
        //bannerView.LoadAd(req);
    }
    public void showBanner() {
        //bannerView.Show();
    }
    public void hideBanner() {
        //bannerView.Hide();
    }


    public void ShowMainChanceVideo() {
       return;
    }


    public void ShowReviveAd() {
      return;
    }

    public void ShowInterstitial() {
        return;
    }

    public void RequestAds() {
        //RequestRewardBasedVideo();
        //RequestInterstitial();
    }

    public void ShowDoubleRewardAds() {
       return;
    }


    private void RequestDouble() {
        return;
#if UNITY_ANDROID
        rewardDoubleId="ca-app-pub-3779186367163749/7125278681";
        if (isDebugMode) {
            rewardDoubleId="ca-app-pub-3940256099942544/5224354917";
        }
#elif UNITY_IPHONE
        rewardDoubleId = "ca-app-pub-3779186367163749/7699993758";
        if(isDebugMode)
        {
            rewardDoubleId = "ca-app-pub-3940256099942544/1712485313";
        }
#endif
        // doubleRewardVideo = new RewardedAd(rewardDoubleId);

        // doubleRewardVideo.OnUserEarnedReward+=HandleDoubleRewarded;
        // doubleRewardVideo.OnAdClosed+=HandleDoubleClosed;
        // AdRequest request = new AdRequest.Builder().Build();
        // doubleRewardVideo.LoadAd(request);
    }


    private void RequestRewardBasedVideo() {

        return;
#if UNITY_ANDROID
        rewardDieId="ca-app-pub-3779186367163749/9751442023";
        if (isDebugMode) {
            rewardDieId="ca-app-pub-3940256099942544/5224354917";
        }
#elif UNITY_IPHONE
        rewardDieId= "ca-app-pub-3779186367163749/2639238767";
        if(isDebugMode)
        {
            rewardDieId = "ca-app-pub-3940256099942544/1712485313";
        }
#endif
        // rewardBasedVideo=new RewardedAd(rewardDieId);

        // // Called when the user should be rewarded for watching a video.
        // rewardBasedVideo.OnUserEarnedReward+=HandleRewardBasedVideoRewarded;
        // // Called when the ad is closed.
        // rewardBasedVideo.OnAdClosed+=HandleRewardBasedVideoClosed;

        // // Create an empty ad request.
        // AdRequest request = new AdRequest.Builder().Build();
        // // Load the rewarded video ad with the request.
        // rewardBasedVideo.LoadAd(request);
    }

    private void RequestInterstitial() {
        return;
#if UNITY_ANDROID
        interstialId="ca-app-pub-3779186367163749/5481025187";
        if (isDebugMode) {
            interstialId="ca-app-pub-3940256099942544/1033173712";
        }
#elif UNITY_IPHONE
        interstialId= "ca-app-pub-3779186367163749/3381475452";
        if(isDebugMode)
        {
            interstialId= "ca-app-pub-3940256099942544/4411468910";
        }

#endif

        // if (interstitial!=null) {
        //     interstitial.Destroy();
        // }

        // // Initialize an InterstitialAd.
        // interstitial=new InterstitialAd(interstialId);

        // // Called when the ad is closed.
        // interstitial.OnAdClosed+=HandleOnAdClosed;

        // // Create an empty ad request.
        // AdRequest request = new AdRequest.Builder().Build();
        // // Load the interstitial with the request.
        // interstitial.LoadAd(request);
    }

    public void HandleOnAdClosed(object sender, EventArgs args) {
        //RequestInterstitial();
    }

    public void HandleDoubleRewarded(object sender, Reward args) {
        GameController._instance.CLEAR_UI_STATUS=GameController.ClearUI_STATUS.DOUBLE_REWARD;
    }

    public void HandleDoubleClosed(object sender, EventArgs args) {
#if UNITY_ANDROID
        rewardDoubleId="ca-app-pub-3779186367163749/7125278681";
        if (isDebugMode) {
            rewardDoubleId="ca-app-pub-3940256099942544/5224354917";
        }
#elif UNITY_IPHONE
        rewardDoubleId = "ca-app-pub-3779186367163749/7699993758";
        if(isDebugMode) {
            rewardDoubleId= "ca-app-pub-3940256099942544/1712485313";
        }
#endif
        rewardBasedVideo=requestVideoAds(rewardDoubleId, "DOUBLE");
    }
    public void HandleRewardBasedVideoRewarded(object sender, Reward args) {
        //GameController._instance.ReviveAdSuccess();
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args) {

#if UNITY_ANDROID
        rewardDieId="ca-app-pub-3779186367163749/9751442023";
        if (isDebugMode) {
            rewardDieId="ca-app-pub-3940256099942544/5224354917";
        }
#elif UNITY_IPHONE
        rewardDieId = "ca-app-pub-3779186367163749/2639238767";
        if(isDebugMode)
        {
            rewardDieId = "ca-app-pub-3940256099942544/1712485313";
        }
#endif
        rewardBasedVideo=requestVideoAds(rewardDieId, "DIE_CHANCE");
    }
    public void HandleMainChanceRewarded(object sender, Reward args) {
        GameController._instance.mainAdsSuccess();
    }

    public void HandleMainChanceClosed(object sender, EventArgs args) {

#if UNITY_ANDROID
        rewardMainId="ca-app-pub-3779186367163749/4690687034";
        if (isDebugMode) {
            rewardMainId="ca-app-pub-3940256099942544/5224354917";
        }
#elif UNITY_IPHONE
        rewardMainId = "ca-app-pub-3779186367163749/3108698974";
        if(isDebugMode) {
            rewardMainId = "ca-app-pub-3940256099942544/1712485313";
        }
#endif
        mainChanceRewarded=requestVideoAds(rewardMainId, "MAIN_CHANCE");
    }
}
