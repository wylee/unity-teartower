﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Video;

using UnityEngine.UI;

public class CrossCode : MonoBehaviour
{
    // Start is called before the first frame update

    public VideoClip[] videoClip;


    public RawImage raw;
    public VideoPlayer video;

    public Text NameText;

    //string gameName = "More Fast";

    public void startVideo()
    {
        StartCoroutine(playVideo());
    }

    public void stopVideo()
    {
        StopCoroutine(playVideo());
    }

    IEnumerator playVideo()
    {
        video.Prepare();

        WaitForSeconds wait = new WaitForSeconds(1);

        while (!video.isPrepared)
        {

            yield return wait;
            break;
        }
        raw.texture = video.texture;
        video.Play();
    }

}