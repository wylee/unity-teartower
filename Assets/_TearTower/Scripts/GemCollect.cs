﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GemCollect : MonoBehaviour{
    public GameObject targetImage;

    private Vector3 myPosition;

    public void SetGemPosition(Vector3 pos){
        this.myPosition = pos;
    }

    private void OnEnable() {
        if(PlayerPrefs.GetInt(Values.sound).Equals(0)){
            this.GetComponent<AudioSource>().Play();
        }
        StartCoroutine("collectTarget");
    }
   private void OnDisable() {
       isGone = true;
   }

   bool isGone = true;

    IEnumerator collectTarget(){
        while(true){

            if (isGone) {

                isGone = false;
                yield return new WaitForSeconds(0.8f);

            }
            else{
                Vector3 target = targetImage.transform.position- this.transform.position;
                if(target.x < 20){
                    int a = int.Parse(targetImage.transform.parent.transform.GetChild(1).GetComponent<Text>().text);
                    a++;
                    targetImage.transform.parent.transform.GetChild(1).GetComponent<Text>().text = a.ToString();
                    this.gameObject.SetActive(false);
                    break;
                }
                else{
                  this.transform.position = Vector3.Lerp(this.transform.position, targetImage.transform.position+ new Vector3(0,5,0), 0.12f);
                }

           }
            yield return new WaitForSeconds(0.03f);
        }
    }
}

