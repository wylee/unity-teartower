﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour{



    public GameObject GameControllObject;
    public GameObject target;
    private CharacterControl CC;
    private GameController GameC;
    
    float yAngle = 0f;

    float timer = 0f;

    public enum RotateStatus {NONE, ACTION, RUNNING };
    public RotateStatus RS = RotateStatus.NONE;

    // Start is called before the first frame update

    bool isFlip = false;

    private void Awake() {
        Application.targetFrameRate = 60;
    // 자동차 선택을 위해 모든객체를 초기화
        for(int i =0; i< this.transform.childCount; i++){
            this.transform.GetChild(i).gameObject.SetActive(false);
        }
        int characterNumber = PlayerPrefs.GetInt(Values.characterIndex);

        this.transform.GetChild(characterNumber).gameObject.SetActive(true);
    }

    void Start(){
        CC = target.GetComponent<CharacterControl>();
        GameC = GameControllObject.GetComponent<GameController>();
       
    }


    // Update is called once per frame
    void Update(){
    // 0번 자동차는 기본자동차라 제외
        if(PlayerPrefs.GetInt(Values.characterIndex) <= 10){
            for (int i = 1; i<5; i++) {
                this.transform.GetChild(PlayerPrefs.GetInt(Values.characterIndex)).transform.GetChild(i).transform.Rotate(40, 0, 0);
            }
        }

        if (GameC.m_gameState.Equals(GameState.Playing)){
            if (CC.m_jumpState.Equals(JumpState.Ground)){

    
                switch (CC.m_section) {
                case Section.Back:
                       yAngle = 0f;
                       this.transform.rotation = Quaternion.Euler(0,0,14f);
                    break;
                case Section.Right:
                        yAngle=-90f;
                        this.transform.rotation=Quaternion.Euler(0, -90f, 14f);
                    break;
                case Section.Forward:
    
                    if (!this.transform.rotation.x.Equals(-180)) {
                        yAngle=-180f;
                        this.transform.rotation=Quaternion.Euler(0, -180f, 14f);
                    }
                    break;
                case Section.Left:
                    if (!this.transform.rotation.x.Equals(-270)) {
                        yAngle=-270f;
                        this.transform.rotation=Quaternion.Euler(0, -270f, 14f);
                    }
                    break;
                };
            }
        }
        else if (GameC.m_gameState.Equals(GameState.Ready)){
            this.transform.rotation=Quaternion.Euler(new Vector3(0, 0, 0));
    
            switch (CC.m_section) {
            case Section.Back:
                this.transform.rotation=Quaternion.Euler(0, -90f, 14f);
                break;
            case Section.Right:
                this.transform.rotation=Quaternion.Euler(0, -180f, 14f);
                break;
            case Section.Forward:
    
                if (!this.transform.rotation.x.Equals(-180)) {
                    this.transform.rotation=Quaternion.Euler(0, -270f, 14f);
                }
                break;
            case Section.Left:
                if (!this.transform.rotation.x.Equals(-270)) {
                    this.transform.rotation=Quaternion.Euler(0, 0, 14f);
                }
                break;
            };
    
    
    
            timer = 0f;
    
           if (PlayerPrefs.GetInt(Values.characterIndex)>=4) {
               for (int i = 6; i<8; i++) {
                   if (this.transform.GetChild(PlayerPrefs.GetInt(Values.characterIndex)).transform.GetChild(i).gameObject.activeSelf) {
                       this.transform.GetChild(PlayerPrefs.GetInt(Values.characterIndex)).transform.GetChild(i).gameObject.SetActive(false);
                   }
               }
           }
    
    
        }
        else if (GameC.m_gameState.Equals(GameState.EndSuccess)) {
            StopAllCoroutines();
        }
      
        this.transform.position = target.transform.position;
    }


    public void flipTheCar(){
        StopCoroutine("flipCorutine");
        StartCoroutine("flipCorutine");
    }

    public void stopFlip(){
        StopCoroutine("flipCorutine");
    }

    IEnumerator flipCorutine(){
        while(!CC.m_jumpState.Equals(JumpState.Ground)){
            this.transform.Rotate(new Vector3(0,0,-20f));
            yield return new WaitForEndOfFrame();
        }

    }
  

}
