﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingController : MonoBehaviour{

	public GameObject slider;

	enum LOAD_STATUS { NONE,MAIN_REWARD, DIE_REWARD, DOUBLE_REWARD, BANNER, INTERSITIAL, SHOP_REWARD_1, SHOP_REWARD_2, DONE, FREEZE};
	LOAD_STATUS LOADING= LOAD_STATUS.NONE;

	//float time = 0.0f;

	Ping ping;
	float pingStartTime;

	string pingAdress = "8.8.8.8"; // google public DNS Server
	string pingAdress2 = "1.1.1.1";
	bool firstAttempt;
	bool secondsAttempt;

	float waitingTime = 2.0f;

	void Start(){
		StartCoroutine(AdsLoadCorutine());
	}

	private bool networkStatus(){

		if(Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork){
			return true;
		}
		else if(Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork){
			return true;
		}
		else {
			return false;
		}
	}

	IEnumerator failLoad(){
		while(true){
			yield return new WaitForSeconds(1f);
			//MyAdmob.myAdmob.isNetworkError=true;
			SceneManager.LoadScene(1);
			LOADING=LOAD_STATUS.DONE;
			break;
		}

	}

	int tickCount = 0;
	IEnumerator AdsLoadCorutine(){
		while(true){
			yield return new WaitForSeconds(1.5f);
			SceneManager.LoadScene(1);
		}
	}
}
