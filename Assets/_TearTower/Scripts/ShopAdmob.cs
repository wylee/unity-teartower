﻿using System;
using UnityEngine;
using GoogleMobileAds.Api;



public class ShopAdmob : MonoBehaviour{
    public GameObject ShopControllObject;
    private ShopController ShopCon;
    string adUnitId;
    public static RewardedAd oneVideo;
    public static RewardedAd twoVideo;
    
    private void Start() {
        if (MyAdmob.myAdmob.isDebugMode) {
            Debug.Log("디버그 모드입니다.");
#if UNITY_ANDROID
            adUnitId="ca-app-pub-3940256099942544/5224354917";
#elif UNITY_IPHONE
            adUnitId = "ca-app-pub-3940256099942544/1712485313";
#endif
        }
        else {
#if UNITY_ANDROID
            adUnitId="ca-app-pub-3779186367163749/5289453499";
#elif UNITY_IPHONE
            adUnitId = "ca-app-pub-3779186367163749/7257141285";
#endif
        }
        ShopCon=ShopControllObject.GetComponent<ShopController>();

        oneVideo=createRewardAds(adUnitId, 0);
        twoVideo=createRewardAds(adUnitId, 1);
    }


    public bool isLoadReward1(){
        return oneVideo.IsLoaded();
    }
    public bool isLoadReward2(){
        return twoVideo.IsLoaded();
    }


    public void showRewardOne() {
#if UNITY_EDITOR
    testShot_1();
#else

         if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
            if (!ShopCon.networkDialog.gameObject.activeSelf) {
                ShopCon.networkDialog.gameObject.SetActive(true);
            }
        }
        else{
            if (oneVideo.IsLoaded()) {
                    oneVideo.Show();
            }
        }
#endif
    }
    public void showRewardTwo(){

#if UNITY_EDITOR
        testShot_2();
#else
        if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
            if (!ShopCon.networkDialog.gameObject.activeSelf) {
                ShopCon.networkDialog.gameObject.SetActive(true);
            }
        }
        else{
            if (twoVideo.IsLoaded()) {
                twoVideo.Show();
            }
        }
#endif
    }


    public RewardedAd createRewardAds(string adUnit,int number){
        RewardedAd rd = new RewardedAd(adUnit);
    
        switch(number){
    
        case 0:
            rd.OnAdClosed+=handleOneButtonAdsClosed;
            rd.OnUserEarnedReward+=handleOneButtonRewardEarn;
            break;
    
        case 1:
            rd.OnAdClosed+=handleTwoButtonAdsClosed;
            rd.OnUserEarnedReward+=handleTwoButtonRewardEarn;
            break;
        };
        AdRequest req = new AdRequest.Builder().Build();
        rd.LoadAd(req);
        return rd;
    }
    
    
    public void handleOneButtonAdsClosed(object sender, EventArgs args) {
        oneVideo = createRewardAds(adUnitId,0);
    
    }
    public void handleTwoButtonAdsClosed(object sender, EventArgs args) {
       twoVideo=createRewardAds(adUnitId, 1);
    
    }
    public void handleOneButtonRewardEarn(object sender, Reward args) {
        string number = Values.unlockCharacter + "10";
    
       
        string name = Values.ADS_COUNT + 0.ToString();
    
        int count = PlayerPrefs.GetInt(name);
    
        if (count.Equals( 1)){
            ShopCon.specialGroup.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(4).gameObject.GetComponent<ParticleSystem>().Play();
            for (int i = 0; i<4; i++) {
                ShopCon.specialGroup.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(4).GetChild(i).GetComponent<ParticleSystem>().Play();
            }
            PlayerPrefs.SetInt(number, 10);
            PlayerPrefs.Save();
        }
        else {
            count++;
    
            PlayerPrefs.SetInt(name,count);
            PlayerPrefs.Save();
        }
    }
    
    public void handleTwoButtonRewardEarn(object sender, Reward args) {
        string number = Values.unlockCharacter + "11";
        string name = Values.ADS_COUNT+"1";
        int count = PlayerPrefs.GetInt(name);
        if (count.Equals(2)) {
    
            ShopCon.specialGroup.transform.GetChild(0).GetChild(0).GetChild(1).GetChild(4).gameObject.GetComponent<ParticleSystem>().Play();
    
            for (int i = 0; i<4; i++) {
                ShopCon.specialGroup.transform.GetChild(0).GetChild(0).GetChild(1).GetChild(4).GetChild(i).GetComponent<ParticleSystem>().Play();
            }
            PlayerPrefs.SetInt(number, 10);
            PlayerPrefs.Save();
        }
        else {
            count++;
            PlayerPrefs.SetInt(name, count);
            PlayerPrefs.Save();
        }
    }


    public void testShot_1() {
        string number = Values.unlockCharacter+"10";
        string name = Values.ADS_COUNT+"0";
        int count = PlayerPrefs.GetInt(name);
        if (count.Equals(1)) {
            ShopCon.specialGroup.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(4).gameObject.GetComponent<ParticleSystem>().Play();
            for (int i = 0; i<4; i++) {
                ShopCon.specialGroup.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(4).GetChild(i).GetComponent<ParticleSystem>().Play();
            }

            PlayerPrefs.SetInt(number, 10);
            PlayerPrefs.Save();
        }
        else {
            count++;
            PlayerPrefs.SetInt(name, count);
            PlayerPrefs.Save();
        }
    }

    public void testShot_2() {
        string number = Values.unlockCharacter+"11";
        string name = Values.ADS_COUNT+"1";
        int count = PlayerPrefs.GetInt(name);
        if (count.Equals(2)) {
            ShopCon.specialGroup.transform.GetChild(0).GetChild(0).GetChild(1).GetChild(4).gameObject.GetComponent<ParticleSystem>().Play();
            for(int i =0;i< 4; i++){
                ShopCon.specialGroup.transform.GetChild(0).GetChild(0).GetChild(1).GetChild(4).GetChild(i).GetComponent<ParticleSystem>().Play();
            }

            PlayerPrefs.SetInt(number, 10);
            PlayerPrefs.Save();
        }
        else {
            count++;
            PlayerPrefs.SetInt(name, count);
            PlayerPrefs.Save();
        }
    }
}
