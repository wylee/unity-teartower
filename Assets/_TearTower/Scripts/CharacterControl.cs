using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public enum Section
{
    Back,
    Right,
    Forward,
    Left
}

public enum JumpState
{
    Ground,
    FirstJumping,
    SecondJumping
}

[HideInInspector]
public enum DebuffState {NONE,SLOW_DEBUFFED,BOOST};

public enum ConnerState {NONE_BUFF, BOOST_BUFF, SLOW_BUFF};

public class CharacterControl : MonoBehaviour {
	public List<GameObject> obstacleObject;


	public enum JumpTimming { 
		NONE,
		JUMP
	}
	// 현재방향을 비추는 라이트
	public GameObject gameLight_1;

	// 케릭터를 비추는 라이트 
	public GameObject chacterLight;


	// 폭발 이펙트 부모 객체
	public GameObject explosionParent;

	// 자동차객체 부모
	public GameObject CarParent;

	private CarController CarCon;

	public JumpTimming JT = JumpTimming.NONE;


	AudioSource boostSound;
	AudioSource slowSound;
	AudioSource itemGetSound;
	AudioSource deadlySound;
	AudioSource jumpSucSound;

    const float CORNER_X_MIN = -0.5f;
    const float CORNER_Z_MIN = -20.5f;
    const float CORNER_X_MAX = 20.5f;
    const float CORNER_Z_MAX = 0.5f;

	public float gravitySlow = -11f;
	public float gravityBoost = -100f;

    Rigidbody m_rigidBody;
    public float m_torque = 30.0f; //30.0f
    public float m_jumpForce = 6.0f;
    public Section m_section;
    Section m_startSection;
    public JumpState m_jumpState = JumpState.Ground;
    bool m_tryToJump = false;
    Vector3 m_characterStartPosition = new Vector3(-0.5f, 1.2f, -20.5f);

    Vector3 m_jumpSectionBack;
    Vector3 m_jumpSectionRight;
    Vector3 m_jumpSectionForward;
    Vector3 m_jumpSectionLeft;

	public float boostOffset = 3f;


	/**
	 * 
	 * 
	 * 
	 * 
	 **/


	 string jumpDialog;
	 public Text jumpTextObject;
	 private int jumpCounter = 0;
	 public GameObject BoostEffectPrefabs;
	 public GameObject SlowEffectPreFabs;

	 public float boostJumpOffset = 1.4f;

     public ConnerState ConnerBuffState = ConnerState.NONE_BUFF;
	 public DebuffState DEBUFF = DebuffState.NONE;

	 int effectNumber = 0;
	 private GameObject effectParent;

	 int buffcount =0;

	 int carNumber = 0;

	private void Awake() {

		obstacleObject =new List<GameObject>();
		// ObsaclePosition을 담는 리스트.
		//int charNu
		carNumber=PlayerPrefs.GetInt(Values.characterIndex);

		for(int i =0; i< this.transform.childCount; i++){
			if(this.transform.GetChild(i).gameObject.activeSelf){
				this.transform.GetChild(i).gameObject.SetActive(false);
			}
		}


		if (carNumber<=4) {

			if(!this.transform.GetChild(0).gameObject.activeSelf){
				this.transform.GetChild(0).gameObject.SetActive(true);
			}
			effectParent = this.transform.GetChild(0).transform.GetChild(0).gameObject;
		}
		else{
			if (!this.transform.GetChild(1).gameObject.activeSelf) {
				this.transform.GetChild(1).gameObject.SetActive(true);
			}
			effectParent=this.transform.GetChild(1).transform.GetChild(0).gameObject;
		}
	}

	Vector3 allowVector = Vector3.zero;

	void Start () {
		
		
		boostSound =GetComponents<AudioSource>()[0];
		slowSound =GetComponents<AudioSource>()[1];
		itemGetSound =GetComponents<AudioSource>()[2];
		deadlySound = GetComponents<AudioSource>()[3];
		jumpSucSound = GetComponents<AudioSource>()[4];

		CarCon = CarParent.GetComponent<CarController>();


		// 이곳에서 RigidBody 를 설정합니다.

		m_rigidBody= GetComponent<Rigidbody>();
        m_rigidBody.maxAngularVelocity = 25f; // 25f
        //m_rigidBody.maxAngularVelocity = m_torque;

        InitializeCharacter();

        m_jumpSectionBack = (Vector3.right + Vector3.up + Vector3.up + Vector3.up);
        m_jumpSectionRight = (Vector3.forward + Vector3.up + Vector3.up + Vector3.up);
        m_jumpSectionForward = (Vector3.left + Vector3.up + Vector3.up + Vector3.up);
        m_jumpSectionLeft = (Vector3.back + Vector3.up + Vector3.up + Vector3.up);

		if(BoostEffectPrefabs.gameObject.activeSelf){
			BoostEffectPrefabs.gameObject.SetActive(false);
		}
		if(SlowEffectPreFabs.gameObject.activeSelf){
			SlowEffectPreFabs.gameObject.SetActive(false);
		}


    }

	IEnumerator initCharacterSpeed(int time){

		while(true){
			yield return new WaitForSeconds(1f);

			if (buffcount >=time){
				buffcount = 0;
				m_rigidBody.maxAngularVelocity=25f;

				DEBUFF = DebuffState.NONE;
				ConnerBuffState = ConnerState.NONE_BUFF;

				StopCoroutine("boostGradientColor");

				// Initialize Gradient 
				Gradient gr = new Gradient();
				gr.SetKeys(
					new GradientColorKey[] { new GradientColorKey(Color.red, 0.0f), new GradientColorKey(Color.red, 0.5f), new GradientColorKey(Color.red, 1f) },
					new GradientAlphaKey[] { new GradientAlphaKey(0f, 0.0f), new GradientAlphaKey(1.0f, 0.5f), new GradientAlphaKey(0f, 1f) }
				);
				CarParent.transform.GetChild(carNumber).transform.GetChild(5).GetComponent<TrailRenderer>().colorGradient=gr;


				ParticleSystem ps = effectParent.transform.GetChild(0).GetComponent<ParticleSystem>();
				ParticleSystem.MainModule psModule = ps.main;
				psModule.startColor=Color.white;

				if (effectParent.transform.GetChild(0).gameObject.activeSelf) {
					effectParent.transform.GetChild(0).gameObject.SetActive(false);
				}

				if (BoostEffectPrefabs.gameObject.activeSelf) {
					BoostEffectPrefabs.gameObject.SetActive(false);

				}

				if (SlowEffectPreFabs.gameObject.activeSelf) {
					SlowEffectPreFabs.gameObject.SetActive(false);
				}

				if(PlayerPrefs.GetInt(Values.characterIndex) >=4) {
					for (int i = 6; i<8; i++) {
						if (CarParent.transform.GetChild(PlayerPrefs.GetInt(Values.characterIndex)).transform.GetChild(i).gameObject.activeSelf) {
							CarParent.transform.GetChild(PlayerPrefs.GetInt(Values.characterIndex)).transform.GetChild(i).gameObject.SetActive(false);
						}
					}
				}


				break;
			}
			else {
				buffcount++;
			}
		}
	}

	void boostCharacter(){

		if (PlayerPrefs.GetInt(Values.sound).Equals(0)) {

			boostSound.Play();
		}

		m_rigidBody.maxAngularVelocity=50f;
		DEBUFF = DebuffState.BOOST;


		ConnerBuffState = ConnerState.BOOST_BUFF;
		m_rigidBody.velocity=m_rigidBody.velocity*2f;

		StopCoroutine("boostGradientColor");
		StartCoroutine("initCharacterSpeed",2f);
		StartCoroutine("boostGradientColor");

		

		if(carNumber.Equals(4)){
			if (BoostEffectPrefabs.gameObject.activeSelf) {
				BoostEffectPrefabs.gameObject.SetActive(false);
			}
		}
		else {
			if(!BoostEffectPrefabs.gameObject.activeSelf){
			BoostEffectPrefabs.gameObject.SetActive(true);
		
			}
			if (!effectParent.transform.GetChild(0).gameObject.activeSelf) {
				effectParent.transform.GetChild(0).gameObject.SetActive(true);
			}

		}

		// Set color Change 
		ParticleSystem ps = effectParent.transform.GetChild(0).GetComponent<ParticleSystem>();
		ParticleSystem.MainModule psModule = ps.main;
		psModule.startColor=Color.white;

		
	}

	IEnumerator boostGradientColor(){
		// 부스트발동 무지개 꼬리 컬러링 해주는 부분.

		Gradient gr = new Gradient();
		GradientAlphaKey[] alphaChannel = {new GradientAlphaKey(0f, 0.0f),new GradientAlphaKey(0.5f, 0.094f),	new GradientAlphaKey(1.0f, 0.465f), new GradientAlphaKey(0.5f, 0.9f), new GradientAlphaKey(0f, 1f) };
		// Gradient Alpha Channel;

		GradientColorKey front_red = new GradientColorKey(Color.red,0);
		// Don't Edit Color Fixed Front Position
		float[] timeValue = new float[6];
		GradientColorKey yellow = new GradientColorKey(Color.yellow,0.16f);
		GradientColorKey green = new GradientColorKey(Color.green,0.32f);
		GradientColorKey cyan = new GradientColorKey(Color.cyan,0.48f);
		GradientColorKey blue = new GradientColorKey(new Color(0,0.5373f,1),0.64f);
		GradientColorKey pupple = new GradientColorKey(new Color(1,0,1),0.80f);
		GradientColorKey red = new GradientColorKey(Color.red, 1f);

		timeValue[0] = yellow.time;
		timeValue[1] = green.time;
		timeValue[2] = cyan.time;
		timeValue[3] = blue.time;
		timeValue[4] = pupple.time;
		timeValue[5] = red.time;


		while (true){

			if (DEBUFF.Equals(DebuffState.NONE)) {
				gr.SetKeys(
					new GradientColorKey[] { new GradientColorKey(Color.red, 0.0f), new GradientColorKey(Color.red, 0.5f), new GradientColorKey(Color.red, 1f) },
					new GradientAlphaKey[] { new GradientAlphaKey(0f, 0.0f), new GradientAlphaKey(1.0f, 0.5f), new GradientAlphaKey(0f, 1f) }
				);

				CarParent.transform.GetChild(carNumber).transform.GetChild(5).GetComponent<TrailRenderer>().colorGradient=gr;
				
				break;
			}

			for (int i =0; i< 6; i++){
				if(timeValue[i] <= 0.01f){
					timeValue[i] = 1f;
				}
				else{
					timeValue[i] -= 5f * Time.deltaTime;
				}
			}
			
			yellow.time=timeValue[0];
			green.time=timeValue[1];
			cyan.time=timeValue[2];
			blue.time=timeValue[3];
			pupple.time=timeValue[4];
			red.time=timeValue[5];

			 gr.SetKeys(
					new GradientColorKey[] {
							front_red,
							yellow,
							green,
							cyan,
							blue,
							pupple,
							red
						}
						,
					alphaChannel
				);
			CarParent.transform.GetChild(carNumber).transform.GetChild(5).GetComponent<TrailRenderer>().colorGradient=gr;
			yield return new WaitForFixedUpdate();
		}
	}
	void slowCharacter(){
		if (PlayerPrefs.GetInt(Values.sound).Equals(0)) {
			slowSound.Play();
		}
		StopCoroutine("boostGradientColor");

		if (!SlowEffectPreFabs.gameObject.activeSelf) {
			SlowEffectPreFabs.gameObject.SetActive(true);
		}

		m_rigidBody.maxAngularVelocity = 5f;
		//m_rigidBody.velocity = m_rigidBody.velocity * sv ;
		DEBUFF=DebuffState.SLOW_DEBUFFED;
		ConnerBuffState = ConnerState.SLOW_BUFF;
		StartCoroutine("initCharacterSpeed",0f);

		// Slow Gradient
		Gradient gr = new Gradient();
		gr.SetKeys(
			new GradientColorKey[] { new GradientColorKey(Color.blue, 0.0f), new GradientColorKey(Color.blue, 0.5f), new GradientColorKey(Color.blue, 1f) },
			new GradientAlphaKey[] { new GradientAlphaKey(0f, 0.0f), new GradientAlphaKey(1.0f, 0.5f), new GradientAlphaKey(0f, 1f) }
		);
		CarParent.transform.GetChild(carNumber).transform.GetChild(5).GetComponent<TrailRenderer>().colorGradient=gr;
		ParticleSystem ps = effectParent.transform.GetChild(0).GetComponent<ParticleSystem>();
		ParticleSystem.MainModule psModule = ps.main;
		psModule.startColor=Color.blue;
		
		if (!effectParent.transform.GetChild(0).gameObject.activeSelf) {
			effectParent.transform.GetChild(0).gameObject.SetActive(true);
		}
	}

	int count = 0;
	 private void OnCollisionEnter(Collision collision) {

		float target = 0f;

		if (collision.gameObject.tag.Equals(Values.slowbox)) {
			if (collision.gameObject.activeSelf) {
				collision.gameObject.SetActive(false);

			}
		
			slowCharacter();
			buffcount=0;

		}else if (collision.gameObject.tag.Equals(Values.boostbox)){
			if(collision.gameObject.activeSelf){
				collision.gameObject.SetActive(false);

			}


			if (PlayerPrefs.GetInt(Values.characterIndex)>=4) {

				for (int i = 6; i<8; i++) {
					if (!CarParent.transform.GetChild(PlayerPrefs.GetInt(Values.characterIndex)).transform.GetChild(i).gameObject.activeSelf) {
						CarParent.transform.GetChild(PlayerPrefs.GetInt(Values.characterIndex)).transform.GetChild(i).gameObject.SetActive(true);
					}
				}
			}

			if (PlayerPrefs.GetInt(Values.sound).Equals(0)) {
				itemGetSound.Play();
			}
			buffcount= 0;
			boostCharacter();
		}
		if (collision.gameObject.tag == Values.stairs) {


		// 조명 조절해주는곳.
			switch(m_section){
			
				case Section.Back:
					gameLight_1.transform.rotation = Quaternion.Euler(0,0,0);
					chacterLight.transform.rotation = Quaternion.Euler(45f,0,0);
					//0 -45 0
				break;
			
				case Section.Right:
					gameLight_1.transform.rotation = Quaternion.Euler(0,-60f,0);
					chacterLight.transform.rotation=Quaternion.Euler(45f, -90f, 0);

				//0 -60 0
				break;
				case Section.Forward:
					gameLight_1.transform.rotation=Quaternion.Euler(0, -150f, 0);
					chacterLight.transform.rotation=Quaternion.Euler(45f, -180f, 0);

				break;
				//0-130,0
			
				case Section.Left:
					gameLight_1.transform.rotation=Quaternion.Euler(0, -240f, 0);
				chacterLight.transform.rotation=Quaternion.Euler(45f, -270f, 0);

				//0 45 0
				break;
			}

			int game_stage = PlayerPrefs.GetInt(Values.stage);


			if(game_stage > 0){
				if(game_stage%5 != 0 ){
					if (!(m_jumpState.Equals(JumpState.Ground))) {

						for (int i = 0; i<obstacleObject.Count; i++) {
							if (obstacleObject[i].gameObject.activeSelf) {

								if (m_section.Equals(Section.Back)&&
										obstacleObject[i].GetComponent<ObstacleController>().m_section.Equals(Section.Back)) {

									if (obstacleObject[i].GetComponent<ObstacleController>().m_isPoped) {

										target=obstacleObject[i].transform.position.x-this.transform.position.x;

									

										if (target>=-7.5f&&target<=-0.5f) {
											effectAction();
										}
									}
								}
								else if (m_section.Equals(Section.Right)&&
										obstacleObject[i].GetComponent<ObstacleController>().m_section.Equals(Section.Right)) {

									if (obstacleObject[i].GetComponent<ObstacleController>().m_isPoped) {

										target=obstacleObject[i].transform.position.z-this.transform.position.z;
										
										if (target>=-7.5f&&target<=-0.5f) {
											effectAction();

										}
									}
								}
								else if (m_section.Equals(Section.Forward)&&
									obstacleObject[i].GetComponent<ObstacleController>().m_section.Equals(Section.Forward)) {

									if (obstacleObject[i].GetComponent<ObstacleController>().m_isPoped) {

										target=this.transform.position.x-obstacleObject[i].transform.position.x;
									
										if (target>=-7.5f&&target<=-0.5f) {
											effectAction();
										}
									}
								}
								else if (m_section.Equals(Section.Left)&&
									obstacleObject[i].GetComponent<ObstacleController>().m_section.Equals(Section.Left)) {

									if (obstacleObject[i].GetComponent<ObstacleController>().m_isPoped) {

										target=this.transform.position.z-obstacleObject[i].transform.position.z;
									
										if (target>=-7.5f&&target<=-0.5f) {
											effectAction();

										}

									}
								}
							}

						}
					}

				}
				
			}
			else{
				if (!(m_jumpState.Equals(JumpState.Ground))) {

					for (int i = 0; i<obstacleObject.Count; i++) {
						if (obstacleObject[i].gameObject.activeSelf) {

							if (m_section.Equals(Section.Back)&&
									obstacleObject[i].GetComponent<ObstacleController>().m_section.Equals(Section.Back)) {

								if (obstacleObject[i].GetComponent<ObstacleController>().m_isPoped) {

									target=obstacleObject[i].transform.position.x-this.transform.position.x;

									if (target>=-7.5f&&target<=-0.5f) {
										effectAction();
									}
								}
							}
							else if (m_section.Equals(Section.Right)&&
									obstacleObject[i].GetComponent<ObstacleController>().m_section.Equals(Section.Right)) {

								if (obstacleObject[i].GetComponent<ObstacleController>().m_isPoped) {

									target=obstacleObject[i].transform.position.z-this.transform.position.z;

									if (target>=-7.5f&&target<=-0.5f) {
										effectAction();
									}
								}
							}
							else if (m_section.Equals(Section.Forward)&&
								obstacleObject[i].GetComponent<ObstacleController>().m_section.Equals(Section.Forward)) {

								if (obstacleObject[i].GetComponent<ObstacleController>().m_isPoped) {

									target=this.transform.position.x-obstacleObject[i].transform.position.x;

									if (target>=-7.5f&&target<=-0.5f) {
										effectAction();
									}
								}
							}
							else if (m_section.Equals(Section.Left)&&
								obstacleObject[i].GetComponent<ObstacleController>().m_section.Equals(Section.Left)) {

								if (obstacleObject[i].GetComponent<ObstacleController>().m_isPoped) {

									target=this.transform.position.z-obstacleObject[i].transform.position.z;

									if (target>=-7.5f&&target<=-0.5f) {
										effectAction();
									}

								}
							}
						}

					}
				}
			}

			
			m_jumpState= JumpState.Ground;
			// Here's Come Effect;

		}
        if(collision.gameObject.tag == Values.obstacles) {
			CarCon.stopFlip();

			explosionParent.transform.position = this.transform.position;
			if(!explosionParent.gameObject.activeSelf){
				explosionParent.SetActive(true);
			}

			GameController._instance.GameEndFail();
			StartCoroutine("CodeadlySound");

			if (BoostEffectPrefabs.gameObject.activeSelf) {
				BoostEffectPrefabs.gameObject.SetActive(false);
			}
			if (SlowEffectPreFabs.gameObject.activeSelf) {
				SlowEffectPreFabs.gameObject.SetActive(false);
			}
		}

    }


	private void effectAction(){
		int rdnum = Random.Range(0, 2);

		if (!jumpTextObject.gameObject.activeSelf) {
			jumpTextObject.gameObject.SetActive(true);
		}

		if (rdnum.Equals(0)) {
			jumpTextObject.transform.GetComponent<RectTransform>().localPosition=new Vector3(-500f, 0, 0);
		}
		else {
			jumpTextObject.transform.GetComponent<RectTransform>().localPosition=new Vector3(500f, 0, 0);
		}


		if (jumpCounter<=3) {
			jumpDialog="Cool";

		}
		else if (jumpCounter.Equals(4)) {
			if (rdnum.Equals(0)) {
				jumpDialog="Great";

			}
			else {
				jumpDialog="Awesome";

			}
			jumpCounter=0;
		}
				   		
		jumpTextObject.text=jumpDialog;
		jumpCounter++;
		StopCoroutine("JumpDialogEffect");
		jumpTextObject.GetComponent<Text>().fontSize=40;
		StartCoroutine("JumpDialogEffect");
		// Here Jump Suc Sound
		if (PlayerPrefs.GetInt(Values.sound)==0) {
			jumpSucSound.Play();
		}

	}


	IEnumerator CodeadlySound(){
		while(true){
			yield return new WaitForSeconds(2f);
			if (PlayerPrefs.GetInt(Values.sound)==0) {
				deadlySound.Play();
			}
			break;
		}
	}


	public void CharacterChangeSetup(int index){
		//effectParent=this.transform.GetChild(index).transform.GetChild(0).gameObject;
		DEBUFF=DebuffState.NONE;
		ConnerBuffState=ConnerState.NONE_BUFF;

	}


	public JumpState GetJumpState()
    {
        return m_jumpState;
    }


    public Section GetSection()
    {
        return m_section;
    }

    public Section GetStartSection()
    {
        return m_startSection;
    }



    public void InitializeCharacter()
    {
		

		chacterLight.transform.rotation=Quaternion.Euler(45f, 0, 0);
		gameLight_1.transform.rotation = Quaternion.Euler(45f,0,0);

		if (explosionParent.gameObject.activeSelf) {
			explosionParent.SetActive(false);
		}

		deadlySound.Stop();

		DEBUFF= DebuffState.NONE;
		ConnerBuffState = ConnerState.NONE_BUFF;
		
		StopCoroutine("initCharacterSpeed");
		buffcount = 0;

		if (carNumber<=4) {
			if (!this.transform.GetChild(0).gameObject.activeSelf) {
				this.transform.GetChild(0).gameObject.SetActive(true);
			}
			effectParent=this.transform.GetChild(0).transform.GetChild(0).gameObject;
		}
		else {
			if (!this.transform.GetChild(1).gameObject.activeSelf) {
				this.transform.GetChild(1).gameObject.SetActive(true);
			}
			effectParent=this.transform.GetChild(1).transform.GetChild(0).gameObject;
		}
		m_rigidBody.maxAngularVelocity = 25f;

		
		if (effectParent.transform.GetChild(0).gameObject.activeSelf) {
			effectParent.transform.GetChild(0).gameObject.SetActive(false);

		}
		ParticleSystem ps = effectParent.transform.GetChild(0).GetComponent<ParticleSystem>();
		ParticleSystem.MainModule psModule = ps.main;
		psModule.startColor=new Color(0, 0, 0, 1f);


		m_rigidBody.velocity = Vector3.zero;
        m_rigidBody.angularVelocity = Vector3.zero;
        m_rigidBody.constraints = RigidbodyConstraints.FreezePositionZ;
        m_jumpState = JumpState.SecondJumping;
        m_remainingDistance = 21f;

       if(GameController._instance.IsRevived())
       {
           m_startSection = m_section;
       } else
       {
           m_startSection = Section.Back;
           m_section = Section.Left;
       }
		int stage = PlayerPrefs.GetInt(Values.stage);

		if ((stage%5).Equals(0)) {

			switch (m_startSection) {
			case Section.Back:
				m_section=Section.Left;
				transform.position=new Vector3(CORNER_X_MIN, GameController._instance.GetCurrentBonusFloor()*5.0f+1.35f, CORNER_Z_MIN);
				break;
			case Section.Right:
				m_section=Section.Back;
				transform.position=new Vector3(CORNER_X_MAX, GameController._instance.GetCurrentBonusFloor()*5.0f+1.35f, CORNER_Z_MIN);
				break;
			case Section.Forward:
				m_section=Section.Right;
				transform.position=new Vector3(CORNER_X_MAX, GameController._instance.GetCurrentBonusFloor()*5.0f+1.35f, CORNER_Z_MAX);
				break;
			case Section.Left:
				m_section=Section.Forward;
				transform.position=new Vector3(CORNER_X_MIN, GameController._instance.GetCurrentBonusFloor()*5.0f+1.35f, CORNER_Z_MAX);
				break;
			}

		}
		else {

			// 다음 방향 알려주는거.
			switch (m_startSection)
			{
				case Section.Back:
				    m_section=Section.Left;
				transform.position=new Vector3(CORNER_X_MIN, GameController._instance.GetCurrentFloor()*5.0f+1.35f, CORNER_Z_MIN);
				break;
				case Section.Right:
					m_section=Section.Back;
				transform.position=new Vector3(CORNER_X_MAX, GameController._instance.GetCurrentFloor()*5.0f+1.35f, CORNER_Z_MIN);
				break;
				case Section.Forward:
					m_section=Section.Right;
				transform.position=new Vector3(CORNER_X_MAX, GameController._instance.GetCurrentFloor()*5.0f+1.35f, CORNER_Z_MAX);
				break;
				case Section.Left:
					m_section=Section.Forward;
				transform.position=new Vector3(CORNER_X_MIN, GameController._instance.GetCurrentFloor()*5.0f+1.35f, CORNER_Z_MAX);
				break;
			}

		    
        }
		effectNumber = 1;
		for (int i =1; i< effectParent.transform.childCount; i++){
			if(effectParent.transform.GetChild(i).gameObject.activeSelf){
				effectParent.transform.GetChild(i).gameObject.SetActive(false);
			}
		}
    }


	float m_remainingDistance = 21f;
    public float GetRemainingDistance(){
        return m_remainingDistance;
    }


    public float GetProgressPercentage(){
        return (21f-m_remainingDistance) / 21f;
    }


    bool m_awakeStart = false;

    private void FixedUpdate()
    {
	
		if(GameController._instance == null){
			return;
		}
		else{

			if (GameController._instance.m_gameState != GameState.Playing){
			    return;
			}
		}
		//gravity
		if (DEBUFF.Equals(DebuffState.SLOW_DEBUFFED)){
			GetComponent<Rigidbody>().AddForce(new Vector3(0.0f, gravitySlow, 0.0f), ForceMode.Acceleration);
		}
		else if (DEBUFF.Equals(DebuffState.BOOST)) {
			GetComponent<Rigidbody>().AddForce(new Vector3(0.0f, gravityBoost, 0.0f), ForceMode.Acceleration);
		}
		else {
			GetComponent<Rigidbody>().AddForce(new Vector3(0.0f, -39.81f, 0.0f), ForceMode.Acceleration);
		}


		if (m_awakeStart && m_jumpState == JumpState.Ground)
        {
            m_isReadyToRoll = true;
            m_awakeStart = false;
          
        }
		

        switch (m_section)
        {
            case Section.Back:

                Jump(m_jumpSectionBack);

                if (transform.position.x > CORNER_X_MAX - 0.005f)
                {
					GameController._instance.IncreaseFloor();


					//DEBUFF = DebuffState.NONE;

                    m_awakeStart = true;
                    m_isReadyToRoll = false;
                    transform.position = new Vector3(CORNER_X_MAX, transform.position.y, transform.position.z);
                    m_rigidBody.velocity = Vector3.zero;
                    m_rigidBody.angularVelocity = Vector3.zero;
                    m_section = Section.Right;
                    m_rigidBody.constraints = RigidbodyConstraints.FreezePositionX;
                
                    if (GameController._instance.m_isTestingLevel && GameController._instance.m_isTestingSecondAlso == false)
                    {
                        GameController._instance.GameEndSuccess();
                    }

                    m_remainingDistance = CORNER_Z_MAX - transform.position.z;
                } else
                {
				
					if (DEBUFF.Equals(DebuffState.BOOST)) {
						m_rigidBody.AddForce(Vector3.back*boostOffset, ForceMode.Acceleration);
					}

					if (m_isReadyToRoll) {
						m_rigidBody.AddTorque(Vector3.back*m_torque, ForceMode.Impulse);
					}

					
					
					m_remainingDistance=CORNER_X_MAX-transform.position.x;

				}

				break;

            case Section.Right:

                Jump(m_jumpSectionRight);

                if (transform.position.z > CORNER_Z_MAX - 0.005f)
                {


				GameController._instance.IncreaseFloor();


					m_awakeStart= true;
                    m_isReadyToRoll = false;
                    transform.position = new Vector3(transform.position.x, transform.position.y, CORNER_Z_MAX);
                    m_rigidBody.velocity = Vector3.zero;
                    m_rigidBody.angularVelocity = Vector3.zero;
                    m_section = Section.Forward;
                    m_rigidBody.constraints = RigidbodyConstraints.FreezePositionZ;
                  
                    if (GameController._instance.m_isTestingLevel && GameController._instance.m_isTestingSecondAlso)
                    {
                        GameController._instance.GameEndSuccess();
                    }

                    m_remainingDistance = transform.position.x - CORNER_X_MIN;
                }
                else
                {

					if (ConnerBuffState.Equals(ConnerState.BOOST_BUFF)) {
						DEBUFF=DebuffState.BOOST;
					}
					else if (ConnerBuffState.Equals(ConnerState.SLOW_BUFF)) {
						DEBUFF=DebuffState.SLOW_DEBUFFED;
					}


					if (DEBUFF.Equals(DebuffState.BOOST)) {
						m_rigidBody.AddForce(Vector3.right*boostOffset, ForceMode.Acceleration);
					}

					if (m_isReadyToRoll){
						m_rigidBody.AddTorque(Vector3.right*m_torque, ForceMode.Impulse);
					}
					m_remainingDistance = CORNER_Z_MAX - transform.position.z;
				}

				break;

            case Section.Forward:

                Jump(m_jumpSectionForward);

                if (transform.position.x < CORNER_X_MIN + 0.005f)
                {
				GameController._instance.IncreaseFloor();



				m_awakeStart= true;
                    m_isReadyToRoll = false;
                    transform.position = new Vector3(CORNER_X_MIN, transform.position.y, transform.position.z);
                    m_rigidBody.velocity = Vector3.zero;
                    m_rigidBody.angularVelocity = Vector3.zero;
                    m_section = Section.Left;
                    m_rigidBody.constraints = RigidbodyConstraints.FreezePositionX;
                    m_remainingDistance = transform.position.z - CORNER_Z_MIN;
                }
                else
                {
                    
					if (DEBUFF.Equals(DebuffState.BOOST)) {
						m_rigidBody.AddForce(Vector3.forward*boostOffset, ForceMode.Acceleration);
					}
					
					if (m_isReadyToRoll){
						m_rigidBody.AddTorque(Vector3.forward*m_torque, ForceMode.Impulse);
					}
					m_remainingDistance=transform.position.x-CORNER_X_MIN;
				}

                break;

            case Section.Left:

                if (GameController.m_frameCount > 1)
                {
                    Jump(m_jumpSectionLeft);
                }

                if (transform.position.z < CORNER_Z_MIN + 0.005f)
                {

				if (CheckStageClear()) {
					return;
				}
				else{
					GameController._instance.IncreaseFloor();
				}

                    m_awakeStart = true;
                    m_isReadyToRoll = false;
                    transform.position = new Vector3(transform.position.x, transform.position.y, CORNER_Z_MIN);
                    m_rigidBody.velocity = Vector3.zero;
                    m_rigidBody.angularVelocity = Vector3.zero;
                    m_section = Section.Back;
                    m_rigidBody.constraints = RigidbodyConstraints.FreezePositionZ;
                
                    if(GameController._instance.m_isInfiniteMode)
                        GameController._instance.CreateTowerPiece();

                    m_remainingDistance = CORNER_X_MAX - transform.position.x;
                }
                else
                {
					if (DEBUFF.Equals(DebuffState.BOOST)) {
						m_rigidBody.AddForce(Vector3.left*boostOffset, ForceMode.Acceleration);
					}

					if (m_isReadyToRoll){
						m_rigidBody.AddTorque(Vector3.left*m_torque, ForceMode.Impulse);
					}
					m_remainingDistance=transform.position.z-CORNER_Z_MIN;

				}

				break;
        }

        Physics.Simulate(Time.deltaTime);
    }

    bool CheckStageClear()
    {
        if (!GameController._instance.m_isInfiniteMode && (GameController._instance.IsLastFloor() || GameController._instance.isLastBonusFloor()))
        {
            GameController._instance.GameEndSuccess();
            return true;
        } else
        {
            return false;
        }
    }

    bool m_isReadyToRoll = false;
    public void SetIsReadyToRoll()
    {
        m_isReadyToRoll = true;
    }


	

    void Jump(Vector3 direction){
	

		if (m_awakeStart)
        {
            return;
        }

        if (m_tryToJump)
        {
            bool canJump = false;
            if(m_jumpState == JumpState.Ground)
            {
                m_jumpState = JumpState.FirstJumping;
                canJump = true;
            } else if(m_jumpState == JumpState.FirstJumping)
            {
                m_jumpState = JumpState.SecondJumping;
                canJump = true;
            }

            if(canJump)
            {

				if (effectNumber>=16) {
					effectNumber=1;
				}
				else if (effectNumber.Equals(1)) {
					effectNumber++;

				}
				else {
					effectNumber++;
					if (effectParent.transform.GetChild(effectNumber-1).gameObject.activeSelf) {
						effectParent.transform.GetChild(effectNumber-1).gameObject.SetActive(false);
					}
				}
				if (!effectParent.transform.GetChild(effectNumber).gameObject.activeSelf) {
					effectParent.transform.GetChild(effectNumber).gameObject.SetActive(true);
				}


				m_rigidBody.velocity = Vector3.zero;

				
				if(DEBUFF.Equals(DebuffState.SLOW_DEBUFFED)){
					m_rigidBody.AddForce(direction* m_jumpForce * 0.5f, ForceMode.Impulse);
				}
				else if(DEBUFF.Equals(DebuffState.BOOST)){
					m_rigidBody.AddForce((direction)*m_jumpForce*boostJumpOffset, ForceMode.Impulse);
				}
				else {
					m_rigidBody.AddForce(direction* m_jumpForce, ForceMode.Impulse);
				}

				if (PlayerPrefs.GetInt(Values.sound) == 0)
				{
				    GameController._instance.GetSoundJump().Play();
				}


				// Flip Action;

				CarCon.flipTheCar();

			}
			m_tryToJump = false;
        }
    }
	IEnumerator JumpDialogEffect() {

		while (true) {
			if (jumpTextObject.GetComponent<Text>().fontSize>=70) {
				jumpTextObject.gameObject.SetActive(false);
				jumpTextObject.GetComponent<Text>().fontSize=50;
				break;
			}
			else {
				jumpTextObject.GetComponent<Text>().fontSize+=4;
			}
			yield return new WaitForSeconds(0.03f);
		}
	}


	private void Update()
    {
		if(GameController._instance == null){
			return;
		}
		else {
			if (GameController._instance.m_gameState!=GameState.Playing) {
				return;
			}
		}

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    InputBegan(touch.position);
                    break;

                case TouchPhase.Moved:
                    InputMoved(touch.position);
                    break;

                case TouchPhase.Stationary:
                    InputMoved(touch.position);
                    break;

                case TouchPhase.Ended:
                    InputEnded(touch.position);
                    break;

                case TouchPhase.Canceled:
                    InputCanceled(touch.position);
                    break;
            }
        }

#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)
        if (Input.GetMouseButtonDown(0))
        {
            InputBegan(Input.mousePosition);
        }
        if (Input.GetMouseButton(0))
        {
            InputMoved(Input.mousePosition);
        }
        if (Input.GetMouseButtonUp(0))
        {
            InputEnded(Input.mousePosition);
        }
#endif
    }

    void InputBegan(Vector3 position)
    {
        if(m_awakeStart)
        {
            return;
        }

        m_tryToJump = true;
    }

    void InputMoved(Vector3 position)
    {

    }

    void InputEnded(Vector3 position)
    {
        
    }

    void InputCanceled(Vector3 position)
    {
        
    }
}
