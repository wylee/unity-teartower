﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ShopController : MonoBehaviour{


    int nomalLimitNumber = 4;
    int uniqueLimitNumber = 9;
    int specialLimitNumber = 11;

    // Start is called before the first frame update
    public GameObject bgmObject;
    public GameObject sfxObject;

    private AudioSource bgm;
    private AudioSource equipSound;
    private AudioSource purchasedSound;
    private AudioSource selectItem;


    [Header("Admob Object")]
    
    public GameObject ShopAdmob;


    public bool isSelectedCatBtn = false;
    public GameObject networkDialog;


    [HideInInspector]
    public Text currentGem;


    // 우측상단에 사용되는 이미지를 모음
    public Sprite unlockImage;
    public Sprite lockImage;
    public Sprite selectImage;

    // Scrollobject는 스크롤객체의 최상위 뜻한다.
    public GameObject scrolObject;

    // scrollbar 는 Scroll Object 차일드로 있는 스크롤바 객체이다.
    public GameObject scrollbar;



    public GameObject previewObject;
    public GameObject Canvas;

    // Preview에서 자동차가 보여지는 액션을 주기위해 사용되는 enum 공용체이다.
    public enum SELECT {NONE, SELECT,WAIT ,CAR_GONE, NEW_CAR_COME, DONE};
    public SELECT SELECT_STATUS = SELECT.NONE;


    //화살표 버튼, 카테고리를 이동할때 코루틴이 중복으로 동작을 막기위해 사용되는 enum 공용체이다.
    [HideInInspector]
    public enum MOVE_CATEGORY { NONE , PREV, NEXT}
    public MOVE_CATEGORY MOVE_CAT= MOVE_CATEGORY.NONE;


    


    // 케릭터 선택할때 사용되는 변수 이다.
    int selectNumber = 0;
    // preview 에서 보여지는 자동차 오브젝트이다. selectNumber를 이용하여 객체를 찾아 관리.
    GameObject targetCar;


    /* Item Group을 나누어 관리.
     * nomal 그룹
     * special 그룹은 보상형 광고 그룹임.
     * */


    public GameObject nomalGroup;
    public GameObject uniqueGroup;
    public GameObject specialGroup;



     int gems;


    [HideInInspector]
    public int carNumber = 0;

    /*
     언락여부를 확인하는 인트배열
     값이 10이면 오픈(사용가능)
     그 외 값은 오픈되지 않는것임.

     "UNLOCK_C" + 숫자 로 사용됨
     예)

     int a = 3;
     string u3 = "UNLOCK_C"+a.Tostring();
          or  
     Values.unlockCharacter + "3";

     사용가능

    */
    int[] unlockChar;

    // 최종 장착한 녀석
    int equipNumber = 0;

    public Button purchaseBtn;
    public Button adsBtn;
    public Button startPlayBtn;


    public Sprite[] carUnloockSprite;
    public Sprite[] carLockedSprite;
    public Sprite[] buttonImage;
    
    int[] priceSet = { 0, 70, 150, 180, 210,
                     200, 450, 700, 850, 1000};
    
    int[] adsCountSet = { 0, 0, 0, 0 };
    // 광고본 횟수를 담는 배열.

    public GameObject[] catbuttonList;
    // Catetory button 

    /*
     *0 번째 Nomal Category
     *1 번째 SPORTS Category
     *2 번째 VVIP Category
     */

    public void setEquipNumber(int number){
       this.equipNumber = number;
    }

    public bool isPurchasedAndEquip = false;

    int[] firstUnlock;

    public void equipSoundOn() {
        if(PlayerPrefs.GetInt(Values.sound).Equals(0)){
            equipSound.Play();
        }
    }
    public void purchaseSoundOn(){
        if (PlayerPrefs.GetInt(Values.sound).Equals(0)) {
            purchasedSound.Play();
        }
    }
    public void selectSoundOn() {
        if (PlayerPrefs.GetInt(Values.sound).Equals(0)) {
            selectItem.Play();
        }
    }

    private void Awake() {

        PlayerPrefs.SetInt("UNLOCK_C0",10);
        PlayerPrefs.Save();

         bgm= bgmObject.GetComponent<AudioSource>();

         purchasedSound= sfxObject.GetComponents<AudioSource>()[0];
         equipSound=sfxObject.GetComponents<AudioSource>()[1];
         selectItem = sfxObject.GetComponents<AudioSource>()[2];
         StartCoroutine("bgmController");


        for(int i =0;i< 2; i++){
            string ads = Values.ADS_COUNT + i.ToString();
            adsCountSet[i] = PlayerPrefs.GetInt(ads);
        }


        unlockChar = new int[12];
        if(purchaseBtn.gameObject.activeSelf){
            purchaseBtn.gameObject.SetActive(false);
        }
        if(adsBtn.gameObject.activeSelf){
            adsBtn.gameObject.SetActive(false);
        }
       

        if(!startPlayBtn.gameObject.activeSelf){
            startPlayBtn.gameObject.SetActive(true);
        }

        equipNumber = PlayerPrefs.GetInt(Values.characterIndex);
        gems=PlayerPrefs.GetInt(Values.gem);
      
        targetCar=previewObject.transform.GetChild(PlayerPrefs.GetInt(Values.characterIndex)).gameObject;
        targetCar.gameObject.SetActive(true);
        targetCar.transform.localPosition = new Vector3(0,0,0);

        selectNumber = PlayerPrefs.GetInt(Values.characterIndex);
        carNumber = selectNumber;


        if (equipNumber<=nomalLimitNumber) {
            nomalGroup.gameObject.SetActive(true);
        }
        else if (equipNumber<=uniqueLimitNumber) {
            uniqueGroup.gameObject.SetActive(true);
        }
        else {
            specialGroup.gameObject.SetActive(true);
        }



        unlockChar[0]=10;
        // 0번째 Default 값은 오픈시키기위해 값을 지정.

        for (int i = 1; i<12; i++) {
            string ulchar = Values.unlockCharacter+i.ToString();
            unlockChar[i]=PlayerPrefs.GetInt(ulchar);

            string itemName = string.Empty;
            if (i<=4) {
                itemName = "NOC_"+i.ToString();
            }
            else if(i <= 9){
                itemName="SPC_"+i.ToString();
            }
            else {
                itemName="UNIC_"+i.ToString();
            }
            
            
            
            
            if (!Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
                if(unlockChar[i].Equals(0)){
                    Firebase.Analytics.FirebaseAnalytics.LogEvent(itemName, "LOCKED", unlockChar[i]);
                }
                else {
                    Firebase.Analytics.FirebaseAnalytics.LogEvent(itemName,"UNLOCKED",unlockChar[i]);
                }
            }
        }

        int analyticsGem = PlayerPrefs.GetInt(Values.gem);
        // 보유중인 젬 상황 상점들오면 이벤트 발생.
       if (!Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
       
          Firebase.Analytics.FirebaseAnalytics.LogEvent(
              Firebase.Analytics.FirebaseAnalytics.EventEarnVirtualCurrency,
              new Firebase.Analytics.Parameter[] {
                new Firebase.Analytics.Parameter(
                  Firebase.Analytics.FirebaseAnalytics.ParameterValue, analyticsGem),
                new Firebase.Analytics.Parameter(
                  Firebase.Analytics.FirebaseAnalytics.ParameterVirtualCurrencyName, "GEM"),
              }
          );
       }
    }

    private void Start() {
        Application.targetFrameRate=60;
    }

    IEnumerator bgmController(){

        if(PlayerPrefs.GetInt( Values.sound).Equals(0)){
            if(!bgm.isPlaying){
                bgm.Play();
            }
        }
        else{
            if (bgm.isPlaying) {
                bgm.Stop();
            }
        }

        yield return new WaitForSeconds(1f);

    }

   
    // 해당 아이템을 얻기위한 광고 횟수를 받는 배열.



    private void Update() {

        if(Input.GetKey(KeyCode.Escape)){
            SceneManager.LoadScene(1);
        }



        if (PlayerPrefs.GetInt("VVIP_CLICK").Equals(1)){
            if(catbuttonList[2].transform.GetChild(1).gameObject.activeSelf){
                catbuttonList[2].transform.GetChild(1).gameObject.SetActive(false);
            }
        }
        else{
            if (!catbuttonList[2].transform.GetChild(1).gameObject.activeSelf) {
                catbuttonList[2].transform.GetChild(1).gameObject.SetActive(true);
            }
        }

       // 이것도 동일.
        for (int i = 1; i<=2; i++) {
            string st = "VVIP_"+i.ToString();
            if(PlayerPrefs.GetInt(st).Equals(1)){
                if(specialGroup.transform.GetChild(0).transform.GetChild(0).GetChild(i-1).transform.GetChild(2).gameObject.activeSelf){
                    specialGroup.transform.GetChild(0).transform.GetChild(0).GetChild(i-1).transform.GetChild(2).gameObject.SetActive(false);
                }
            }
        }

        // 느낌표 없애주는곳
        for(int i =10; i< 12; i++){
            string st = Values.unlockCharacter + i.ToString();
            if (PlayerPrefs.GetInt(st).Equals(10)){
                if(specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(6).gameObject.activeSelf){
                    specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(6).gameObject.SetActive(false);
                }
            }
        }


        for (int i = 0; i<2; i++) {
            string ads = Values.ADS_COUNT+i.ToString();
            adsCountSet[i]=PlayerPrefs.GetInt(ads);
        }

        for(int i =0;i< 2; i++){
             GameObject vipObject = specialGroup.transform.GetChild(0).GetChild(0).GetChild(i).gameObject;

            string va = Values.unlockCharacter + "1" + i.ToString();

            if(PlayerPrefs.GetInt(va).Equals(10)){
                if(vipObject.transform.GetChild(3).GetChild(0).gameObject.activeSelf){
                    vipObject.transform.GetChild(3).GetChild(0).gameObject.SetActive(false);
                }
                if (vipObject.transform.GetChild(3).GetChild(1).gameObject.activeSelf) {
                    vipObject.transform.GetChild(3).GetChild(1).gameObject.SetActive(false);
                }
                if (vipObject.transform.GetChild(3).GetChild(2).gameObject.activeSelf) {
                    vipObject.transform.GetChild(3).GetChild(2).gameObject.SetActive(false);
                }
            }

            if (i.Equals(0) && adsCountSet[i] <= 2) {
                vipObject.transform.GetChild(3).GetChild(0).GetComponent<Text>().text=adsCountSet[i].ToString();
            }
            else if(i.Equals(1) && adsCountSet[i] <= 3){
                 vipObject.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = adsCountSet[i].ToString();
            }
        }

        int nomalCount = 0;
        int sportCount = 0;

        gems=PlayerPrefs.GetInt(Values.gem);
        for(int i =1; i<= uniqueLimitNumber; i++){
            string ulchar = Values.unlockCharacter+i.ToString();
            unlockChar[i]=PlayerPrefs.GetInt(ulchar);

            if(unlockChar[i].Equals(0)){

                if (i<=nomalLimitNumber) {
                    if( gems >= priceSet[i] ){
                        nomalCount++;
                        if(!nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetChild(6).gameObject.activeSelf){
                            nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetChild(6).gameObject.SetActive(true);
                        }
                        // 상품가격 표기
                        nomalGroup.transform.GetChild(0).transform.GetChild(0).GetChild(i).GetChild(3).GetChild(1).GetComponent<Text>().color=Color.white;
                        nomalGroup.transform.GetChild(0).transform.GetChild(0).GetChild(i).GetChild(3).GetChild(1).GetComponent<Text>().text=priceSet[i].ToString();
                    }
                    else {
                        // 상품가격 표기
                        nomalGroup.transform.GetChild(0).transform.GetChild(0).GetChild(i).GetChild(3).GetChild(1).GetComponent<Text>().color = Color.gray;
                        nomalGroup.transform.GetChild(0).transform.GetChild(0).GetChild(i).GetChild(3).GetChild(1).GetComponent<Text>().text = priceSet[i].ToString();

                        if (nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetChild(6).gameObject.activeSelf) {
                            nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetChild(6).gameObject.SetActive(false);
                        }

                    }
                }
                else{
                    if (gems>=priceSet[i]) {
                        // 상품가격 표기
                        uniqueGroup.transform.GetChild(0).transform.GetChild(0).GetChild(i-5).GetChild(3).GetChild(1).GetComponent<Text>().color=Color.white;
                        uniqueGroup.transform.GetChild(0).transform.GetChild(0).GetChild(i-5).GetChild(3).GetChild(1).GetComponent<Text>().text=priceSet[i].ToString();

                        sportCount++;
                        if (!uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetChild(6).gameObject.activeSelf) {
                            uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetChild( 6).gameObject.SetActive(true);
                        }
                    }
                    else {

                        // 상품가격 표기
                        uniqueGroup.transform.GetChild(0).transform.GetChild(0).GetChild(i-5).GetChild(3).GetChild(1).GetComponent<Text>().color=Color.gray;
                        uniqueGroup.transform.GetChild(0).transform.GetChild(0).GetChild(i-5).GetChild(3).GetChild(1).GetComponent<Text>().text=priceSet[i].ToString();


                        if (uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetChild(6).gameObject.activeSelf) {
                            uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetChild(6).gameObject.SetActive(false);
                        }

                    }
                }

            }
            else{
            // 구매가 되었을경우엔 느낌표 제거.
                if (i<=nomalLimitNumber) {
                    if (nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetChild(6).gameObject.activeSelf) {
                        nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetChild(6).gameObject.SetActive(false);
                    }

                    for(int j =0;j< 2; j++){
                        if(nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetChild(3).transform.GetChild(j).gameObject.activeSelf){
                            nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetChild(3).transform.GetChild(j).gameObject.SetActive(false);
                        }
                    }


                }
                else {
                    if (uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetChild(6).gameObject.activeSelf) {
                        uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetChild(6).gameObject.SetActive(false);
                    }
                    for (int j = 0; j<2; j++) {
                        if (uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetChild(3).transform.GetChild(j).gameObject.activeSelf) {
                            uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetChild(3).transform.GetChild(j).gameObject.SetActive(false);
                        }
                    }

                }

            }

        }

        if(nomalCount >= 1){
            if(!catbuttonList[0].transform.GetChild(1).gameObject.activeSelf){
                catbuttonList[0].transform.GetChild(1).gameObject.SetActive(true);
            }
        }
        else{
            if (catbuttonList[0].transform.GetChild(1).gameObject.activeSelf) {
                catbuttonList[0].transform.GetChild(1).gameObject.SetActive(false);
            }
        }
        
        if(sportCount >= 1){
            if(!catbuttonList[1].transform.GetChild(1).gameObject.activeSelf){
                catbuttonList[1].transform.GetChild(1).gameObject.SetActive(true);
            }
        }
        else{
            if (catbuttonList[1].transform.GetChild(1).gameObject.activeSelf) {
                catbuttonList[1].transform.GetChild(1).gameObject.SetActive(false);
            }
        }




        currentGem.text = gems.ToString();
        equipNumber = PlayerPrefs.GetInt(Values.characterIndex);
        // SWIPE LOGICAL
        itemSpriteStatusLogic();
        buttonPriceLogic();


        // CAR SELECT_SEQUENCE
        if (SELECT_STATUS.Equals(SELECT.NONE)) {
            previewProductRotate();
        }
        else if(SELECT_STATUS.Equals(SELECT.SELECT)){
            targetCar=previewObject.transform.GetChild(selectNumber).gameObject;
            targetCar.transform.parent.transform.rotation = Quaternion.Euler(0,0,0);
            StartCoroutine("goneCorutine");
            SELECT_STATUS = SELECT.WAIT;

            selectNumber = PlayerPrefs.GetInt(Values.selectIndex);
            // Button 부분하는 거다.
        }
        else if(SELECT_STATUS.Equals(SELECT.NEW_CAR_COME)){
            selectNumber = PlayerPrefs.GetInt(Values.selectIndex);
            targetCar=previewObject.transform.GetChild(selectNumber).gameObject;
            targetCar.gameObject.SetActive(true);
            StartCoroutine("selectCarCome");
            SELECT_STATUS = SELECT.WAIT;
        }
        else if(SELECT_STATUS.Equals(SELECT.DONE)){
            targetCar.transform.localPosition = new Vector3(0,0,0);
            targetCar.transform.parent.transform.rotation = Quaternion.Euler(0,0,0);
            SELECT_STATUS = SELECT.NONE;
        }
    }

    void buttonPriceLogic(){
  // Eqip 사용
        for (int i = 1; i<12; i++) {
            string ulchar = Values.unlockCharacter+i.ToString();
            unlockChar[i]=PlayerPrefs.GetInt(ulchar);
        }
        if(isSelectedCatBtn) return; 


        if (unlockChar[selectNumber].Equals(10)) {
        // 자동차가 언락 여부를 확인

            if(equipNumber.Equals(carNumber)){
                // 장착한 녀석이 현재 카번호랑 동일하냐?
                if(!startPlayBtn.gameObject.activeSelf){
                    startPlayBtn.gameObject.SetActive(true);
                }
                // 동일하면 장착 버튼을 사라지게하고, 플레이 버튼을 보여지게하기
            }
            else {
            
               if(startPlayBtn.gameObject.activeSelf){
                   startPlayBtn.gameObject.SetActive(false);
               }
            }


            if(purchaseBtn.gameObject.activeSelf){    
                purchaseBtn.gameObject.SetActive(false);
            }
            if(adsBtn.gameObject.activeSelf){
                adsBtn.gameObject.SetActive(false);
            }
        }
        else{
            if (selectNumber <= uniqueLimitNumber){

                if (!purchaseBtn.gameObject.activeSelf){
                    purchaseBtn.gameObject.SetActive(true);
                }
                else{
                    // 글자 색을 변화시켜주는 곳임.
                    if(gems >= priceSet[selectNumber] ) {
                        //purchaseBtn.transform.GetChild(0).GetComponent<Text>().color=Color.green;

                        purchaseBtn.transform.GetComponent<Image>().color = Color.white;
                        purchaseBtn.transform.GetChild(0).GetComponent<Text>().color = Color.white;
                        purchaseBtn.GetComponent<Animator>().enabled=true;
                        purchaseBtn.transform.GetChild(1).gameObject.SetActive(true);
                    }
                    else {

                        purchaseBtn.transform.GetComponent<Image>().color=Color.gray;
                        purchaseBtn.transform.GetChild(0).GetComponent<Text>().color=Color.gray;
                        purchaseBtn.GetComponent<Animator>().enabled = false;
                        purchaseBtn.transform.GetChild(1).gameObject.SetActive(false);
                    }
                }

                purchaseBtn.transform.GetChild(0).GetComponent<Text>().text="UNLOCK";

               

                if(startPlayBtn.gameObject.activeSelf){
                    startPlayBtn.gameObject.SetActive(false);
                }

                if(adsBtn.gameObject.activeSelf){
                    adsBtn.gameObject.SetActive(false);
                }
            }
            else{
                
                if (purchaseBtn.gameObject.activeSelf) {
                    purchaseBtn.gameObject.SetActive(false);
                }

                if (!adsBtn.gameObject.activeSelf) {
                    adsBtn.gameObject.SetActive(true);
                }

                if (startPlayBtn.gameObject.activeSelf) {
                    startPlayBtn.gameObject.SetActive(false);
                }

            }

        }

    }


    void itemSpriteStatusLogic(){
    // Item Select, Unlock, Locked Sprite 변경해주는곳.
        for(int i =0 ; i <  unlockChar.Length ; i++){

            if(selectNumber.Equals(i)){

                if( i<= nomalLimitNumber){
                    nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).transform.GetChild(5).gameObject.SetActive(true);
                }
                else if(i<= uniqueLimitNumber){
                    uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).transform.GetChild(5).gameObject.SetActive(true);
                }
                else {
                    specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(5).gameObject.SetActive(true);
                }
            }
            else{
                if (i<=nomalLimitNumber) {
                    if (nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).transform.GetChild(5).gameObject.activeSelf) {
                        nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).transform.GetChild(5).gameObject.SetActive(false);
                    }
                }
                else if (i<=uniqueLimitNumber) {
                    if (uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).transform.GetChild(5).gameObject.activeSelf) {
                        uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).transform.GetChild(5).gameObject.SetActive(false);
                    }
                }
                else {
                    if (specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(5).gameObject.activeSelf) {
                        specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(5).gameObject.SetActive(false);
                    }
                }

            }





            if(equipNumber.Equals(i)){
                if (i<=nomalLimitNumber) {
                    nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).transform.GetChild(1).GetComponent<Image>().sprite=selectImage;
                    nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).transform.GetChild(0).GetComponent<Image>().sprite=carUnloockSprite[i];
                    nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetComponent<Image>().sprite=buttonImage[0];


                }
                else if (i<=uniqueLimitNumber) {
                    uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).transform.GetChild(1).GetComponent<Image>().sprite=selectImage;
                    uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetComponent<Image>().sprite=buttonImage[0];
                    uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).transform.GetChild(0).GetComponent<Image>().sprite=carUnloockSprite[i];

                }
                else {
                    specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(0).GetComponent<Image>().sprite=carUnloockSprite[i];
                    specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(1).GetComponent<Image>().sprite=selectImage;
                    specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).GetComponent<Image>().sprite=buttonImage[0];
                }
            }
            else{
                if (i<=nomalLimitNumber) {
                    if (unlockChar[i].Equals(10)) {
                        nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).transform.GetChild(0).GetComponent<Image>().sprite=carUnloockSprite[i];
                        nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).transform.GetChild(1).GetComponent<Image>().sprite=unlockImage;
                        nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetComponent<Image>().sprite = buttonImage[0];

                       

                    }
                    else {
                        nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).transform.GetChild(0).GetComponent<Image>().sprite=carLockedSprite[i];
                        nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).transform.GetChild(1).GetComponent<Image>().sprite=lockImage;
                        nomalGroup.transform.GetChild(0).GetChild(0).GetChild(i).GetComponent<Image>().sprite=buttonImage[1];

                        
                    }
                }
                else if (i<=uniqueLimitNumber) {
                    if (unlockChar[i].Equals(10)) {
                        uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetComponent<Image>().sprite=buttonImage[0];
                        uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).transform.GetChild(0).GetComponent<Image>().sprite=carUnloockSprite[i];
                        uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).transform.GetChild(1).GetComponent<Image>().sprite=unlockImage;

                     

                    }
                    else {
                        uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).transform.GetChild(0).GetComponent<Image>().sprite=carLockedSprite[i];
                        uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).transform.GetChild(1).GetComponent<Image>().sprite=lockImage;
                        uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(i-5).GetComponent<Image>().sprite=buttonImage[1];

                    }
                }
                else {
                    if (unlockChar[i].Equals(10)) {
                        specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(0).GetComponent<Image>().sprite=carUnloockSprite[i];
                        specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(1).GetComponent<Image>().sprite=unlockImage;
                        specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).GetComponent<Image>().sprite = buttonImage[0];

                       
                    }
                    else {
                        specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(0).GetComponent<Image>().sprite=carLockedSprite[i];
                        specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).transform.GetChild(1).GetComponent<Image>().sprite=lockImage;
                        specialGroup.transform.GetChild(0).GetChild(0).GetChild(i-10).GetComponent<Image>().sprite=buttonImage[1];


                    }
                }
            }
        }
        
    }
 

 
    void previewProductRotate(){
        previewObject.transform.Rotate(0,-50f * Time.deltaTime,0);
    }


    IEnumerator goneCorutine(){
        
        while(true){
            if(targetCar.transform.position.x < 30f){
                targetCar.transform.position += new Vector3(300f * Time.deltaTime, 0,0);
            }
            else{
                SELECT_STATUS = SELECT.NEW_CAR_COME;
                targetCar.gameObject.SetActive(false);
                targetCar.transform.localPosition= new Vector3(-20f,0,0);
                break;
            }

            yield return new WaitForSeconds(0.03f);
        }
    }

    IEnumerator selectCarCome(){

        while (true) {
            if (targetCar.transform.position.x < -3f) {
                targetCar.transform.position+=new Vector3(300f*Time.deltaTime, 0, 0);
            }
            else {
                SELECT_STATUS=SELECT.DONE;
                break;
            }

            yield return new WaitForSeconds(0.03f);
        }

    }



}
