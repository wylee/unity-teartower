using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Analytics;
#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#elif UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System.Threading.Tasks;
#endif
using UnityEngine.SocialPlatforms;
//using Facebook.Unity;
public enum GameState
{
    Ready,
    Playing,
    EndFail,
    EndSuccess,
    SHOW_CLEAR_UI,
    WAIT_FOR_INPUT
}

public class GameController : MonoBehaviour
{

    [Header("Debugmode")]
    public bool isClearDebugmode = false;
    public int DebugNumber;

    //Ŭ����������, UI ���� ��Ʈ�� enum ��
    /*
     * 
     * NONE : * �ƹ��͵� ����
     * SHOW_DIALOG : * �ؽ�Ʈ�� ������
     * GEM_INCREASE : * ����ڰ����� �� ������ ī������ (�ڷ�ƾ ����� ���� �������ͽ� 1ȸ�� �������ͽ� -> Wait �� �̵�)
     * WAIT * �ؽ�Ʈ ī���� �ڷ�ƾ�� ����ɶ�, ���߱� ���� ��� ���ߴ� ����
     * GEM_UPRISE : ī���õǰ� �� ��������Ʈ�� �����ϰ� �̵����ִ� ����(1ȸ�� �������ͽ� -> Wait �� �̵�)
     * SAVE_DATA : gemSpawn �ڷ�ƾ�� �Ϸ�Ǹ� ���� ���� ���� �����͸� ������(1ȸ�� �������ͽ� -> Save_Delay�� �̵�)
     * Save_DELAY : 0.5�� ������ �ִ� ���� (�ڷ�ƾ�� ����� , �̱����� �������� �߰��� ������ ���������� ������� ������ ��� ���� ��������)
     * NEXT_LEVEL : ���� ���������� �̵��ϱ����� ��� �����͸� Init ����
     * DONE : Ư���� ����� ����������, ������Ʈ���� ���⶧���� ����ġ ���� ��Ȳ�� ���ϱ����� �������ͽ��� Done�� �����ص�(������ ����)
     * 
     * */

    public enum ClearUI_STATUS {NONE,SHOW_DIALOG, GEM_INCREASE, WAIT, DOUBLE_REWARD ,GEM_UPRISE, SAVE_DATA, SAVE_DELAY ,NEXT_LEVEL,DONE  };
    public ClearUI_STATUS CLEAR_UI_STATUS = ClearUI_STATUS.NONE;
    public GameObject noadsChanceBtn;
    
    // �� �θ� ��ü�� ( UI )
    public GameObject gemStorage;



    // ���ͳ� �����϶� �����ִ� ��.
    public GameObject networkDialog;


    int game_stage = 1;

    public GameObject loadingPannel;


    [Header("NO ADS TIMED")]
    public DateTime noAdsBuffTime;

    // ���� �ð��� ���� ����� ���� span ����
    public TimeSpan buffTimeSpan;


    public DateTime buffEndTime;
    public DateTime buffStartTime;

    // ��ư�� �������� Ȯ��
    int chancePercent = 0;
    // 1�� , 3�� , 5�� ���� Ȯ��
    /*      100%
     *  1��  65%
     *  3��  30%
     *  5��  5%
     * */

     string chanceText = String.Empty;
    /*
     * ���� ����  ( ���� ��ư ���� Ȯ��)
     * -> 
     *  if stage > 2        
     *     1/10  Ȯ���� ���� ��ư ���� 
     *     if  ���� ��ư �� ���� !  -> ���� ��ư�� ������   3�ʵ�  ���� ����ŸƮ
     *     else  ������ư�� ���� �������� ->  2�� �� ���� ����ŸƮ -> ���� ������.
     * 
     *  elif stage == 2     => 100% ���� ��ư ���� ���� ��ư ������. 3�ʵ� ����ŸƮ
     *  
     *  else 
     *      ���� ��ư ���� �ʰ� 2�ʵ� �� �� �� ��ŸƮ
     * 
     * 
     * 
     * */

    public GameObject btnShop;
    public GameObject carParent;

	public GameObject OceanObject;
	CharacterControl charController;

	public GameObject waterWave;
	// Wave Object

    //ui related
    Transform m_canvas;
    public List<ColorGroup> m_colorGroups;


    GameObject bonusGemsPrefab;


    public bool m_isInfiniteMode = false;
    public GameState m_gameState = GameState.Ready;
    GameObject m_towerPiecePrefab;
    GameObject m_towerPieceFinishPrefab;
    GameObject m_obstaclePrefab;
    GameObject m_towerTextPrefab;
    public GameObject m_character;
    GameObject m_pillar;
    GameObject m_plane;
    GameObject m_backgroundTowers;
    GameObject m_gemPrefab;
    List<GameObject> m_gems;


	/**
	 *  Boost Box
	 *  Slow Box
	 **/
	 List<GameObject> slowBoxList;
	 List<GameObject> boostBoxList;

	 GameObject slowBoxPrefab;
	 GameObject boostBoxPrefab;


    GameObject m_stair1;
    GameObject m_stair2;

    public List<BonusGemsPatternGroup> b_bonusPatternGroups;
    
    List<BonusFloor> b_floors;
    List<int[]> b_stagePatterns;
    int b_currentFloorIndexToCreate = 0;
    int b_indexOfCurrentFloor= 0;
    public List<ObstaclePatternGroup> m_obstaclePatternGroups;

    List<Floor> m_floors;
    List<int[]> m_stagePatterns;

    int m_towerPieceCount = 0;
    int m_currentFloorIndexToCreate = 0;
    
    int m_indexOfCurrentFloor = 0;
    public static int m_frameCount = 0;

    public bool m_isDebuging;
    public bool m_isTestingLevel;
    public bool m_isTestingSecondAlso;
    public bool m_isTestingCertainLevel;
    public int m_testingLevelDifficulty;
    public int m_testingLevelIndex;
    [Header("x:1~19 y:1~10")]
    public List<Vector2> m_obstaclePatternTest = new List<Vector2>();

    //Sound related
    AudioSource m_music;
    AudioSource m_soundPop;
    AudioSource m_soundJump;
    AudioSource m_soundFail;
    AudioSource m_soundSuccess;
    AudioSource m_soundStart;
    AudioSource m_soundGem;


    AudioSource clear_textCountSound;

    // ��Ż ��ƼŬ ������ ���� �ۺ��� ��ü
    public GameObject warpParticle;


    //Ads related
    const int ADS_PLAY_COUNT_MAX = 0;
    int m_adsPlayCount = 0;
    const float ADS_PLAY_TIME_MAX = 0f;
    float m_adsPlayTime = 0f;

    public static GameController _instance = null;

    // ���� ���õ� �ڵ��� �ε������� ���Ѵ�.
    int carNumber =0;

    // ��ʸ� ����� ���� bool ������
    bool isBannerShowing = false;

    //��Ż ������Ʈ ��Ʈ���� ���� ��������
    GameObject finishObject = null;


    // ������ �׾�����, �������� ���� �ð�  1,3, 5 min  ���´�.
    public int USER_GET_CHANE_TIME = 0;




    //Ŭ���� ������, ������Ʈ ������ enum
    public enum CLEAR_END_SEQ { INIT, MOVE, WAIT, EFFECT , DONE};
    public CLEAR_END_SEQ CLEAR_END = CLEAR_END_SEQ.INIT;
    
    // 2�� ������ �޾Ҵ��� Ȯ���ϱ� ���� bool ������
    public bool isRewarded = false;



    enum LOAD_STATUS{START,DOUBLE, MAIN_REWARD, INTERSTIAL, DIE_REWARD, DONE};
    // Canvas Child count 25
    LOAD_STATUS L_STATUS = LOAD_STATUS.START;



    private void Awake()
    {
       if (_instance != null){
           Destroy(gameObject);
           return;
       }
       _instance = this;
        
       Screen.sleepTimeout=SleepTimeout.NeverSleep;
       //FB.Init();
       SocialInit();
       DontDestroyOnLoad(gameObject);


    }


    public Image mainExclamationMark;
    // Ȩ����ǥ ��ũ

    // �ڵ��� ������ ������.
    int[] carlist = new int[12];

    int[] priceSet = {0,70,150,180,210,200,450,700,850,1000};
    int userGem = 0;
    void userGetCarList(){


        bool isOpened = false;
        userGem = PlayerPrefs.GetInt(Values.gem);

        for(int i =1; i<= 9; i++){
            string num = Values.unlockCharacter +i.ToString();
            carlist[i]= PlayerPrefs.GetInt(num);
            if(carlist[i].Equals(0)){
                if( userGem >= priceSet[i]){
                    isOpened = true;
                }
            }

        }

        if (isOpened) {
            // �̰����� ����ǥ �� ����!
            if (!mainExclamationMark.gameObject.activeSelf) {
                mainExclamationMark.gameObject.SetActive(true);
            }
        }
        else {
            if (mainExclamationMark.gameObject.activeSelf) {
                mainExclamationMark.gameObject.SetActive(false);
            }
        }



    }


    void Start(){
        StartCoroutine("fixedCorutine");

        game_stage= PlayerPrefs.GetInt(Values.stage);
        carNumber = PlayerPrefs.GetInt(Values.characterIndex);
		charController=m_character.GetComponent<CharacterControl>();
		m_canvas= GameObject.Find(Values.canvas).transform;
        m_towerPiecePrefab = Resources.Load(Values.towerPiece) as GameObject;
        m_towerPieceFinishPrefab = Resources.Load(Values.finish) as GameObject;
        m_obstaclePrefab = Resources.Load(Values.obstacle) as GameObject;


        clear_textCountSound = m_canvas.transform.GetChild(3).GetComponent<AudioSource>();

        bonusGemsPrefab = Resources.Load(Values.gem) as GameObject;


        m_towerTextPrefab = Resources.Load("TowerText") as GameObject;
        m_gemPrefab = Resources.Load(Values.gem) as GameObject;
        m_character = GameObject.Find(Values.character);
        m_pillar = GameObject.Find(Values.pillar);
        m_plane = GameObject.Find(Values.plane);
        m_backgroundTowers = GameObject.Find(Values.backgroundTowers);

		slowBoxPrefab = Resources.Load(Values.slowbox) as GameObject;
		boostBoxPrefab = Resources.Load(Values.boostbox) as GameObject;

        m_music = GetComponents<AudioSource>()[0];
        m_soundPop = GetComponents<AudioSource>()[1];
        m_soundJump = GetComponents<AudioSource>()[2];
        m_soundFail = GetComponents<AudioSource>()[3];
        m_soundSuccess = GetComponents<AudioSource>()[4];
        m_soundStart = GetComponents<AudioSource>()[5]; 
        m_soundGem = GetComponents<AudioSource>()[6];

        m_stagePatterns = StagePatternBuilder.BuildStagePattern();
        b_stagePatterns = StagePatternBuilder.BuildStagePattern();



        m_stair1 = Instantiate(m_towerPiecePrefab, new Vector3(0f, 0f, 0f), m_towerPiecePrefab.transform.rotation);
        m_stair1.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair1.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair1.transform.GetChild(2).GetChild(0).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair1.transform.GetChild(3).GetChild(0).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair1.transform.GetChild(4).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair1.transform.GetChild(5).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair1.transform.GetChild(6).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair1.transform.GetChild(7).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_gems = new List<GameObject>();
        m_gems.Add(Instantiate(m_gemPrefab, m_stair1.transform.GetChild(0).GetChild(0).position + new Vector3(0f, 0.75f, 0f), m_gemPrefab.transform.rotation));
        

        m_gems.Add(Instantiate(m_gemPrefab, m_stair1.transform.GetChild(1).GetChild(0).position + new Vector3(0f, 0.75f, 0f), m_gemPrefab.transform.rotation));
        m_gems.Add(Instantiate(m_gemPrefab, m_stair1.transform.GetChild(2).GetChild(0).position + new Vector3(0f, 0.75f, 0f), m_gemPrefab.transform.rotation));
        m_gems.Add(Instantiate(m_gemPrefab, m_stair1.transform.GetChild(3).GetChild(0).position + new Vector3(0f, 0.75f, 0f), m_gemPrefab.transform.rotation));


        if(m_gems[0].gameObject.activeSelf){
            m_gems[0].gameObject.SetActive(false);
        }


        m_stair2 = Instantiate(m_towerPiecePrefab, new Vector3(0f, 20f, 0f), m_towerPiecePrefab.transform.rotation);
        m_stair2.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair2.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair2.transform.GetChild(2).GetChild(0).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair2.transform.GetChild(3).GetChild(0).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair2.transform.GetChild(4).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair2.transform.GetChild(5).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair2.transform.GetChild(6).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_stair2.transform.GetChild(7).gameObject.GetComponent<Renderer>().material.color = m_stairColor;
        m_gems.Add(Instantiate(m_gemPrefab, m_stair2.transform.GetChild(0).GetChild(0).position + new Vector3(0f, 0.75f, 0f), m_gemPrefab.transform.rotation));
        m_gems.Add(Instantiate(m_gemPrefab, m_stair2.transform.GetChild(1).GetChild(0).position + new Vector3(0f, 0.75f, 0f), m_gemPrefab.transform.rotation));
        m_gems.Add(Instantiate(m_gemPrefab, m_stair2.transform.GetChild(2).GetChild(0).position + new Vector3(0f, 0.75f, 0f), m_gemPrefab.transform.rotation));
        m_gems.Add(Instantiate(m_gemPrefab, m_stair2.transform.GetChild(3).GetChild(0).position + new Vector3(0f, 0.75f, 0f), m_gemPrefab.transform.rotation));

        //Setup Boost Box & SlowBox

        for(int i =0; i< m_gems.Count; i++){
             m_gems[i].gameObject.GetComponent<Renderer>().enabled=  false;
        }

       

        boostBoxList = new List<GameObject>();
		slowBoxList = new List<GameObject>();

		int gameLevel = PlayerPrefs.GetInt(Values.stage);


		// Setup BoostBox & SlowBox

		CreateBoostBox();
		CreateSlowBox();


        if(game_stage > 0){
            if((game_stage%5).Equals(0)){
                initializeBonusGemPool();
            }
            else{
                InitializeObstaclePool();
            }
        }
        else{
            InitializeObstaclePool();
        }

        InitializeTowerTextPool();

        InitializeGame();


        if (PlayerPrefs.GetInt(Values.sound).Equals(0)){
            SetSoundOn();
        }
        else{
            SetSoundOff();
        }

        if (PlayerPrefs.GetInt(Values.notification).Equals( 0)){
            SetNotificationOn();
        }
        else{
            SetNotificationOff();
        }



      Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
      Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
     
      Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
          var dependencyStatus = task.Result;
          if (dependencyStatus == Firebase.DependencyStatus.Available)
          {
            
          }
          else
          {
             
          }
      });

        if(m_canvas.GetChild((int)CanvasChild.Debug).gameObject.activeSelf)
        {
            m_isDebuging = true;
        }
        
        StartCoroutine(AfterStart());

        if (PlayerPrefs.GetString(Values.firstLoadDateTime).Equals(""))
        {
            PlayerPrefs.SetString(Values.firstLoadDateTime, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        }
        DateTime lastTime = Convert.ToDateTime(PlayerPrefs.GetString(Values.firstLoadDateTime));
        TimeSpan timeDiff = DateTime.Now - lastTime;
        PlayerPrefs.SetInt(Values.day, (int) Math.Truncate(timeDiff.TotalDays));
        int day = PlayerPrefs.GetInt(Values.day);
        if (PlayerPrefs.GetInt("D"+day)==0&&day<=1) {
            PlayerPrefs.SetInt("D"+day, 1);
            Firebase.Analytics.FirebaseAnalytics.LogEvent("D"+day);
        }



        // �ð� ��� , ������ �����ִ� �༮��.
       string startData = PlayerPrefs.GetString(Values.NOADS_START);
       //
       if(!startData.Equals(String.Empty)){
            buffStartTime=Convert.ToDateTime(PlayerPrefs.GetString(Values.NOADS_START));
            buffEndTime=Convert.ToDateTime(PlayerPrefs.GetString(Values.NOADS_END));
            noAdsBuffObject.gameObject.SetActive(true);
            StartCoroutine("timeScheduler");
        }
        else{
            noAdsBuffObject.gameObject.SetActive(false);
        }

       

    }

    public void resetTime(){
        PlayerPrefs.SetString(Values.NOADS_START, string.Empty);
        PlayerPrefs.SetString(Values.NOADS_END, string.Empty);
        PlayerPrefs.Save();

    }

    public void mainNoadsBuffActivator() {
        return;


        // if (!noAdsBuffObject.gameObject.activeSelf) {
        //     noAdsBuffObject.gameObject.SetActive(true);
        // }
        // string startData = PlayerPrefs.GetString(Values.NOADS_START);

        // if (startData.Equals(String.Empty)) {
        //     buffStartTime=DateTime.Now;
        //     buffEndTime=buffStartTime.AddSeconds(USER_GET_CHANE_TIME*60f);

        //     PlayerPrefs.SetInt(Values.NOADS_REWARD, 10);
        //     PlayerPrefs.SetString(Values.NOADS_START, buffStartTime.ToString());
        //     PlayerPrefs.SetString(Values.NOADS_END, buffEndTime.ToString());
        //     PlayerPrefs.Save();
        // }
        // else {
        //     buffStartTime=Convert.ToDateTime(PlayerPrefs.GetString(Values.NOADS_START));
        //     buffEndTime=Convert.ToDateTime(PlayerPrefs.GetString(Values.NOADS_END));

        //     TimeSpan smp = buffEndTime-buffStartTime;

        //     if (smp.TotalSeconds>=600) {
        //         TimeSpan sp = new TimeSpan(0, 10, 0);
        //         buffEndTime=DateTime.Now+sp;
        //     }
        //     else {
        //         buffEndTime=buffEndTime.AddSeconds(USER_GET_CHANE_TIME*60f);
        //     }
        //     PlayerPrefs.SetInt(Values.NOADS_REWARD, 10);
        //     PlayerPrefs.SetString(Values.NOADS_START, buffStartTime.ToString());
        //     PlayerPrefs.SetString(Values.NOADS_END, buffEndTime.ToString());
        //     PlayerPrefs.Save();
        // }
        // buffTimeSpan=buffEndTime-buffStartTime;

        // stopTimeScedule();
        // StartCoroutine("timeScheduler");
        // m_canvas.GetChild(24).gameObject.SetActive(false);
    }


    public void noAdsBuffActivator() {
        // �̰� ����Ǵ� ��ư.

        // if (!noAdsBuffObject.gameObject.activeSelf) {
        //     noAdsBuffObject.gameObject.SetActive(true);
        // }
        // string startData = PlayerPrefs.GetString(Values.NOADS_START);

        // if(startData.Equals(String.Empty)){
        //     buffStartTime = DateTime.Now;
        //     buffEndTime = buffStartTime.AddSeconds(USER_GET_CHANE_TIME*60f);

        //     PlayerPrefs.SetInt(Values.NOADS_REWARD,10);
        //     PlayerPrefs.SetString(Values.NOADS_START, buffStartTime.ToString());
        //     PlayerPrefs.SetString(Values.NOADS_END,buffEndTime.ToString());
        //     PlayerPrefs.Save();
        // }
        // else{
        //     buffStartTime = Convert.ToDateTime( PlayerPrefs.GetString(Values.NOADS_START));
        //     buffEndTime = Convert.ToDateTime( PlayerPrefs.GetString(Values.NOADS_END));



        //     TimeSpan smp = buffEndTime - buffStartTime;

        //     if(smp.TotalSeconds >= 600){
        //         TimeSpan sp = new TimeSpan(0, 10, 0);
        //         buffEndTime=   DateTime.Now + sp;
        //     }
        //     else{
        //         buffEndTime=buffEndTime.AddSeconds(USER_GET_CHANE_TIME*60f);
        //     }
        //     PlayerPrefs.SetInt(Values.NOADS_REWARD, 10);
        //     PlayerPrefs.SetString(Values.NOADS_START, buffStartTime.ToString());
        //     PlayerPrefs.SetString(Values.NOADS_END, buffEndTime.ToString());
        //     PlayerPrefs.Save();
        // }
        // buffTimeSpan=buffEndTime-buffStartTime;

        // //stopTimeScedule();
        // StartCoroutine("timeScheduler");
        // noadsChanceBtn.gameObject.SetActive(false);
    }

    void stopTimeScedule(){
       // StopCoroutine("timeScheduler");
    }

    IEnumerator timeScheduler(){

        while(true ){
            buffTimeSpan=buffEndTime-DateTime.Now;
            if(buffTimeSpan.TotalSeconds <= 0){

                PlayerPrefs.SetInt(Values.NOADS_REWARD, 0);
                PlayerPrefs.SetString(Values.NOADS_START, String.Empty);
                PlayerPrefs.SetString(Values.NOADS_END, String.Empty);
                PlayerPrefs.Save();
                if(noAdsBuffObject.gameObject.activeSelf){
                    noAdsBuffObject.gameObject.SetActive(false);
                }
                break;
            }

           
            if (int.Parse(buffTimeSpan.ToString("%m")) >= 8){
                noAdsBuffObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = Color.blue;
            }
            else if(int.Parse(buffTimeSpan.ToString("%m")) >= 6){
                noAdsBuffObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().color=Color.green;
            }
            else if(int.Parse(buffTimeSpan.ToString("%m")) >= 4){
                noAdsBuffObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = new Color(255,128,0);
            }
            else if(int.Parse(buffTimeSpan.ToString("%m")) >= 2){
                noAdsBuffObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = Color.yellow;
            }
            else {
                noAdsBuffObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = Color.red;
            }
            string minute = buffTimeSpan.ToString("%m");
            int  secon = int.Parse(buffTimeSpan.ToString("ss"));
            int min = int.Parse(minute);


            if (min.Equals(0)) {
                noAdsBuffObject.transform.GetChild(0).GetChild(1).GetComponent<Text>().text=secon.ToString() +" s";
            }
            else {

                if(min >= 10){
                    noAdsBuffObject.transform.GetChild(0).GetChild(1).GetComponent<Text>().text="MAX";
                }
                else {
                    noAdsBuffObject.transform.GetChild(0).GetChild(1).GetComponent<Text>().text=minute +"min";
                }
            }

            noAdsBuffObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().fillAmount = Mathf.Lerp(secon/60f,0,0.03f);

            yield return new WaitForSeconds(1f);
        }
    }




    IEnumerator AfterStart()
    {
        yield return new WaitForSeconds(0.1f);

        if (PlayerPrefs.GetInt("isSocialLoggedIn") == 1)
        {
            Social.localUser.Authenticate((bool success) =>
            {

            });
        }
    }

    const int m_obstaclePoolInitialCount = 32;
    public List<GameObject> m_obstaclePool;

    const int b_bonusGemPoolInitialCount = 32;
    public List<GameObject> b_bonusGemPool;

    void InitializeObstaclePool(){
	
        m_obstaclePool.Clear();

		if(charController.obstacleObject.Count != 0){
			charController.obstacleObject.Clear();
		}

		for (int i=0; i<m_obstaclePoolInitialCount; i++)
        {
            GameObject obstacle = Instantiate(m_obstaclePrefab);
            obstacle.SetActive(false);
            m_obstaclePool.Add(obstacle);
			charController.obstacleObject.Add(obstacle.gameObject);
        }
    }

    
    void initializeBonusGemPool(){
        for(int i =0; i< b_bonusGemPoolInitialCount; i++){
           GameObject bonus = Instantiate(bonusGemsPrefab);
           bonus.SetActive(false);
           b_bonusGemPool.Add(bonus);

        }
    }

    void ResetBonusGemPool(){
        for(int i =0;i < b_bonusGemPool.Count; i++){
            b_bonusGemPool[i].GetComponent<Gem>().SetEnable();
            b_bonusGemPool[i].SetActive(false);
        }

    }


    void ResetObstaclePool()
    {
        charController.obstacleObject.Clear();

        for (int i = 0; i < m_obstaclePool.Count; i++)
        {
            m_obstaclePool[i].GetComponent<ObstacleController>().Initialize();
            m_obstaclePool[i].SetActive(false);

            charController.obstacleObject.Add(m_obstaclePool[i].gameObject);
        }
    }

    GameObject GetObstacleFromPool()
    {
        for(int i=0; i< m_obstaclePool.Count; i++)
        {
            if(!m_obstaclePool[i].activeInHierarchy)
            {
                return m_obstaclePool[i];
            }
        }

        GameObject obstacle = Instantiate(m_obstaclePrefab);
        obstacle.SetActive(false);
        m_obstaclePool.Add(obstacle);
        return obstacle;
    }

    GameObject GetBonusGemFromPool(){

        for(int i =0; i<b_bonusGemPool.Count; i++){

            if(!b_bonusGemPool[i].activeInHierarchy){
                return b_bonusGemPool[i];
            }

        }
        GameObject gem = Instantiate(bonusGemsPrefab);
        gem.name = "BONUS_GEM_DSU";

        gem.SetActive(false);
        b_bonusGemPool.Add(gem);
        return gem;
    }


    const int m_towerTextPoolInitialCount = 32;
    public List<GameObject> m_towerTextPool;
    void InitializeTowerTextPool()
    {
        for (int i = 0; i < m_towerTextPoolInitialCount; i++)
        {
            GameObject towerText = Instantiate(m_towerTextPrefab);
            towerText.SetActive(false);
            m_towerTextPool.Add(towerText);
        }
    }

    void ResetTowerTextPool()
    {
        for (int i = 0; i < m_towerTextPool.Count; i++)
        {
            m_towerTextPool[i].SetActive(false);
        }
    }

    GameObject GetTowerTextFromPool()
    {
        for (int i = 0; i < m_towerTextPool.Count; i++)
        {
            if (!m_towerTextPool[i].activeInHierarchy)
            {
                return m_towerTextPool[i];
            }
        }

        GameObject towerText = Instantiate(m_towerTextPrefab);
        towerText.SetActive(false);
        m_towerTextPool.Add(towerText);
        return towerText;
    }

    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token){
    }
    
    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e){
    }

    void SocialInit()
    {
#if UNITY_ANDROID
      PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
      .RequestServerAuthCode(false)
      .Build();
    
	  PlayGamesPlatform.InitializeInstance(config);
	  PlayGamesPlatform.Activate();

#elif UNITY_IOS
        GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
#endif
    }

    public int TestLevel = 23;

    public void CloseNetworkDialog(){
        networkDialog.gameObject.SetActive(false);
    }


    void chanceSetup(){
       
        int rdNumber = UnityEngine.Random.Range(0,100);

        // 15
        // 
        if (rdNumber>=95) {
            // 5 M
            USER_GET_CHANE_TIME=5;
        }
        else if (rdNumber<=25) {
            // 3M
            USER_GET_CHANE_TIME=3;
        }
        else {
            // 1M
            USER_GET_CHANE_TIME=1;
        }


        switch (USER_GET_CHANE_TIME) {
      
        case 1:
            m_canvas.GetChild(24).GetChild(0).GetComponent<Text>().text ="Remove \nAds 1 Min";
            break;
        case 3:
            m_canvas.GetChild(24).GetChild(0).GetComponent<Text>().text="Remove \nAds 3 Min";
            break;
        case 5:
            m_canvas.GetChild(24).GetChild(0).GetComponent<Text>().text ="Remove \nAds 5 Min";
            break;
        };
        m_canvas.GetChild(24).gameObject.SetActive(true);
    }


    void InitializeGame()
    {

        chanceSetup();
        game_stage=PlayerPrefs.GetInt(Values.stage);

        CLEAR_UI_STATUS= ClearUI_STATUS.NONE;

        for(int i =0;i< gemStorage.transform.childCount; i++){
            gemStorage.transform.GetChild(i).transform.localPosition = new Vector3(0,0,0);
            if(gemStorage.transform.GetChild(i).gameObject.activeSelf){
                gemStorage.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        StopCoroutine("oceanWave");
        StartCoroutine("oceanWave");
        userGetCarList();

		OceanObject.transform.position=new Vector3(0, -2.5f, 0);
		m_character.GetComponent<CharacterControl>().JT = CharacterControl.JumpTimming.NONE;
		waterWave.transform.localPosition = new Vector3(0,-4f,5.5f);
		InitBoostAndSlow();
        m_lastDifficulty = -1;
        m_lastRandomIndex = -1;

        m_gameState = GameState.Ready;
        m_canvas.GetChild((int)CanvasChild.Fail).gameObject.SetActive(false);
		m_canvas.GetChild((int)CanvasChild.Fail).gameObject.GetComponent<Image>().color = new Color(0f,0f,0f,0f);

		m_canvas.GetChild((int)CanvasChild.Fail).GetChild(0).gameObject.SetActive(false);
		m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.SetActive(false);

		m_canvas.GetChild((int)CanvasChild.Success).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.TapToPlay).gameObject.SetActive(true);
        m_canvas.GetChild((int)CanvasChild.Score).gameObject.SetActive(true);
        m_canvas.GetChild((int)CanvasChild.Slider).gameObject.SetActive(false);
        if (PlayerPrefs.GetInt(Values.noAds) == 1)
        {
            m_canvas.GetChild((int)CanvasChild.NoAds).gameObject.SetActive(false);
        } else
        {
            m_canvas.GetChild((int)CanvasChild.NoAds).gameObject.SetActive(true);
        }
        
        m_canvas.GetChild((int)CanvasChild.Settings).gameObject.SetActive(true);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).gameObject.SetActive(true);
        m_canvas.GetChild((int)CanvasChild.Shop).gameObject.SetActive(true);
        m_canvas.GetChild((int)CanvasChild.Gem).gameObject.SetActive(true);
        m_canvas.GetChild((int)CanvasChild.Quit).gameObject.SetActive(false);
        // m_canvas.GetChild((int)CanvasChild.Cross).gameObject.SetActive(true);
        // m_canvas.GetChild((int)CanvasChild.Cross).GetChild(1).GetComponent<CrossCode>().startVideo();
        
        m_canvas.GetChild(24).gameObject.SetActive(true);

        if(game_stage == 0){
            game_stage = 1;
        }



        carParent.transform.GetChild(carNumber).transform.GetChild(5).GetComponent<TrailRenderer>().enabled = false;

        if(game_stage > 0){

            if((game_stage%5).Equals(0)){
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().text="BONUS STAGE";
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.EnableKeyword(ShaderUtilities.Keyword_Glow);

                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.EnableKeyword(ShaderUtilities.Keyword_MASK_HARD);
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.EnableKeyword(ShaderUtilities.Keyword_MASK_SOFT);
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.EnableKeyword(ShaderUtilities.Keyword_Bevel);
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.EnableKeyword(ShaderUtilities.Keyword_MASK_TEX);
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.EnableKeyword(ShaderUtilities.Keyword_Ratios);
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.EnableKeyword(ShaderUtilities.Keyword_Underlay);
                m_canvas.GetChild((int)CanvasChild.Score).gameObject.GetComponent<Animation>().Play();
            }
            else {
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().text = game_stage.ToString();
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_Glow);

                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_MASK_HARD);
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_MASK_SOFT);
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_Bevel);
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_MASK_TEX);
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_Ratios);
                m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_Underlay);
            }
        }
        else{
            m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().text = game_stage.ToString();
            m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_Glow);
            m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_MASK_HARD);
            m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_MASK_SOFT);
            m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_Bevel);
            m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_MASK_TEX);
            m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_Ratios);
            m_canvas.GetChild((int)CanvasChild.Score).GetComponent<TextMeshProUGUI>().fontSharedMaterial.DisableKeyword(ShaderUtilities.Keyword_Underlay);
         
        }
        UpdateGemUI();
        ResetObstaclePool();
        ResetBonusGemPool();
        if (!m_isRevived)
        {
            ResetTowerTextPool();
            ResetTowerTextPool();
        }

      
        Camera.main.GetComponent<SmoothFollowCSharp>().InitializeCamera();

        m_towerPieceCount = 0;
        m_currentFloorIndexToCreate = 0;
        b_currentFloorIndexToCreate = 0;
        if (!m_isRevived)
        {
            m_indexOfCurrentFloor = 0;
            b_indexOfCurrentFloor = 0;
            m_floors = new List<Floor>();
            b_floors = new List<BonusFloor>();
        }
        else
        {
            m_indexOfCurrentFloor--;
            b_indexOfCurrentFloor--;
        }
        m_frameCount = 0;

        if(game_stage >0){
            if(game_stage%5 == 0){
                m_sliderFloorStandard=1f/b_stagePatterns[PlayerPrefs.GetInt(Values.stage)].Length;
            }
            else {
                m_sliderFloorStandard=1f/m_stagePatterns[PlayerPrefs.GetInt(Values.stage)].Length;
            }
        }
        else{
            m_sliderFloorStandard=1f/m_stagePatterns[PlayerPrefs.GetInt(Values.stage)].Length;
        }



        CreateTowerPiece();
        CreateTowerPiece();

	
        if (!m_isInfiniteMode)
        {

            if(finishObject == null){
                finishObject =Instantiate(m_towerPieceFinishPrefab);
            }

        }

        m_adsPlayCount++;

        if (PlayerPrefs.GetInt(Values.sound)==0) {
            m_music.Play();
        }


        if (game_stage%2 != 0) {
			if (boostBoxList[0].gameObject.activeSelf) {
				boostBoxList[0].gameObject.SetActive(false);
			}

			if (slowBoxList[0].gameObject.activeSelf) {
				slowBoxList[0].gameObject.SetActive(false);
			}
		}
        PlayerPrefs.SetInt(Values.USER_GET_GEMS,0);
        PlayerPrefs.Save();
	}

    public void PlayGemSound()
    {
        if (PlayerPrefs.GetInt(Values.sound) == 0)
        {
            m_soundGem.Play();
        }
    }

    public int GetCurrentFloor()
    {
        return m_indexOfCurrentFloor;
    }

    public int GetCurrentBonusFloor(){

        return b_indexOfCurrentFloor;

    }

    void SetBackgroundTowerColor(Material fog)
    {
        for (int i = 0; i < m_backgroundTowers.transform.childCount; i++)
        {
            m_backgroundTowers.transform.GetChild(i).GetComponent<MeshRenderer>().material = fog;
        }
    }

    Color m_obstacleColor;
    Color m_stairColor;
    Color m_finishColor;
   


    const float ObstacleAwakeTimerMax = 20f;
    float m_obstacleAwakeTimer = 20f;
    const float ObstacleAwakeDamping = 0.55f;
    Floor m_currentFloor = null;
    BonusFloor b_currentFloor = null;

	IEnumerator oceanWave(){
		while(true){
            if (m_gameState.Equals( GameState.Playing)){
                if(game_stage.Equals(0)){
                    if (OceanObject.transform.position.y<GetCurrentFloor()*2.5f) {
                        OceanObject.transform.position+=new Vector3(0, 1.5f*Time.deltaTime, 0);
                    }
                }
                else{
                     if((game_stage%5).Equals(0)){
                         if (GetCurrentBonusFloor()<3) {
                             if (OceanObject.transform.position.y<GetCurrentBonusFloor()*2.5f) {
                                 OceanObject.transform.position+=new Vector3(0, 1.5f*Time.deltaTime, 0);
                             }
                         }
                         else {
                             if (OceanObject.transform.position.y<GetCurrentBonusFloor()*4f) {
                                 OceanObject.transform.position+=new Vector3(0, 2.5f*Time.deltaTime, 0);
                             }
                         }
                     }
                     else{
                         if(GetCurrentFloor() < 3){
                              if (OceanObject.transform.position.y<GetCurrentFloor()*2.5f) {
                                  OceanObject.transform.position+=new Vector3(0, 1.5f*Time.deltaTime, 0);
                              }
                          }
                         else{
                              if(OceanObject.transform.position.y  < GetCurrentFloor()*4f ){
                                  OceanObject.transform.position+=new Vector3(0, 2.5f*Time.deltaTime, 0);
                              }
                         }
                     }
                }
            }
            else if(m_gameState.Equals(GameState.Ready)){

                if((game_stage%5).Equals(0)){
                    if (GetCurrentBonusFloor().Equals(0)) {
                        OceanObject.transform.position=new Vector3(0, -2.5f, 0);
                    }
                    else {
                        OceanObject.transform.position=new Vector3(0, GetCurrentBonusFloor()*2.5f, 0);
                    }
                }
                else{

                    if(GetCurrentFloor().Equals(0)){
                        OceanObject.transform.position=new Vector3(0, -2.5f, 0);
                    }
                    else {
                        OceanObject.transform.position=new Vector3(0, GetCurrentFloor() * 2.5f, 0);
                    }
                }
                yield return new WaitForSeconds(0.1f);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator fixedCorutine(){
        while(true){
            if (Input.GetKeyDown(KeyCode.Escape)){
                m_canvas.GetChild((int)CanvasChild.Quit).gameObject.SetActive(true);
            }

            if (m_gameState==GameState.Playing) {
                    //wake up obstacles
                if (m_currentFloor!=null&&m_obstacleAwakeTimer<ObstacleAwakeTimerMax) {
                    foreach (Obstacle obstacle in m_currentFloor.Obstacles) {
                        if (obstacle.released) {
                            continue;
                        }

                        if (obstacle.posX<m_obstacleAwakeTimer) {
                            obstacle.gameObject.GetComponent<ObstacleController>().PopStart();
                            obstacle.released=true;

                            if (PlayerPrefs.GetInt(Values.sound)==0) {
                                m_soundPop.Play();
                            }
                        }
                    }

                    m_obstacleAwakeTimer+=ObstacleAwakeDamping;
                }

                    m_frameCount++;
            }

            yield return new WaitForEndOfFrame();
        }

    }


  

    float m_sliderFloorStandard;

	float gameProgressValue =0f;

	float roundValue = 0.0f;


    [Header("NO_ADS_BUFF_OBJECT")]
    public GameObject noAdsBuffObject;


    private void LateUpdate()
    {
        if (m_gameState.Equals( GameState.Playing)){

			charController=m_character.GetComponent<CharacterControl>();

			if (!m_isTestingLevel)
            {
				if(!m_character.GetComponent<CharacterControl>().GetProgressPercentage().Equals(0)){


                    if(game_stage > 0){

                        if((game_stage%5).Equals(0)){
                            gameProgressValue=(b_indexOfCurrentFloor-1+m_character.GetComponent<CharacterControl>().GetProgressPercentage())*m_sliderFloorStandard;
                        }
                        else {
					        gameProgressValue=(m_indexOfCurrentFloor-1+m_character.GetComponent<CharacterControl>().GetProgressPercentage())*m_sliderFloorStandard;
                        }

                    }
                    else{
                        gameProgressValue=(m_indexOfCurrentFloor-1+m_character.GetComponent<CharacterControl>().GetProgressPercentage())*m_sliderFloorStandard;

                    }
                    roundValue=(float)Math.Round(gameProgressValue, 4);
					m_canvas.GetChild((int)CanvasChild.Slider).GetComponent<Slider>().value=roundValue;

                    if(game_stage > 0 && game_stage%5 != 0){ 
					    for(int i =0; i< m_obstaclePool.Count ; i++){
					    	charController.obstacleObject[i].transform.position = m_obstaclePool[i].transform.position;
					    	charController.obstacleObject[i].GetComponent<ObstacleController>().m_section =m_obstaclePool[i].GetComponent<ObstacleController>().m_section;
                        
					    }
                    }
                    else if(game_stage.Equals(0)){
                        for (int i = 0; i<m_obstaclePoolInitialCount; i++) {
                            charController.obstacleObject[i].transform.position=m_obstaclePool[i].transform.position;
                            charController.obstacleObject[i].GetComponent<ObstacleController>().m_section=m_obstaclePool[i].GetComponent<ObstacleController>().m_section;
                        }
                    }
                  
                }
            }

            if (isBannerShowing) {
                isBannerShowing=false;
                MyAdmob.myAdmob.hideBanner();
            }

            m_adsPlayTime+= Time.deltaTime;
        }
        else if(m_gameState.Equals(GameState.Ready)){
            objectResetOption();

           

            if (isBannerShowing){
                isBannerShowing = false;
                MyAdmob.myAdmob.hideBanner();

            }
        }   

        if (m_gameState.Equals( GameState.EndFail))
        {
			double per = gameProgressValue*100;
			per = Math.Round(per,1);
			
            // onceCorutine start..
            


			m_canvas.GetChild((int)CanvasChild.Fail).GetChild(3).GetComponent<Text>().text=per.ToString()+"%  Cleared ";

			if (m_isRevived is false)
            {
                if (UI_STATUS.Equals(OVER_UI_STATUS.DONE)){
                    m_reviveTime+=Time.deltaTime;

                    // �̰����� ���������� ���� �湮 .

                    if (!m_canvas.GetChild((int)CanvasChild.Fail).GetChild(0).gameObject.activeSelf && m_reviveTime > 2f)
					{
					    m_canvas.GetChild((int)CanvasChild.Fail).GetChild(0).gameObject.SetActive(true);
					}
				}

                if(!isBannerShowing){
                    isBannerShowing = true;
                    //.showBanner();
                    MyAdmob.myAdmob.showBanner();
                }
            }
        }
        else if(m_gameState.Equals(GameState.EndSuccess)){

            if (game_stage.Equals(0)){
                


                switch (CLEAR_END) {
                case CLEAR_END_SEQ.INIT:
                    StopCoroutine("oceanWave");
                    CLEAR_END=CLEAR_END_SEQ.MOVE;
                    break;

                case CLEAR_END_SEQ.MOVE:
                    StartCoroutine("clearEndMoveAndSmall");
                    CLEAR_END=CLEAR_END_SEQ.WAIT;
                    break;

                case CLEAR_END_SEQ.WAIT:

                    break;

                case CLEAR_END_SEQ.EFFECT:
                    warpParticle.transform.position=m_character.transform.position;
                    StartCoroutine("portalSmallAction");
                    CLEAR_END=CLEAR_END_SEQ.WAIT;
                    break;

                case CLEAR_END_SEQ.DONE:
                    for (int i = 2; i<4; i++) {
                        if (m_canvas.transform.GetChild(3).GetChild(i).gameObject.activeSelf) {
                            m_canvas.transform.GetChild(3).GetChild(i).gameObject.SetActive(false);
                        }
                    }

                    if (!m_canvas.transform.GetChild(3).GetChild(1).gameObject.activeSelf) {
                        m_canvas.transform.GetChild(3).GetChild(1).gameObject.SetActive(true);
                    }

                    m_gameState=GameState.SHOW_CLEAR_UI;
                    break;
                }

            }
            else{

                switch (CLEAR_END){
                    case CLEAR_END_SEQ.INIT: 
                        StopCoroutine("oceanWave");
                        CLEAR_END = CLEAR_END_SEQ.MOVE;
                    break;

                    case CLEAR_END_SEQ.MOVE:
                        StartCoroutine("clearEndMoveAndSmall");
                        CLEAR_END = CLEAR_END_SEQ.WAIT;
                    break;

                    case CLEAR_END_SEQ.WAIT:

                    break;

                    case CLEAR_END_SEQ.EFFECT:
                        warpParticle.transform.position = m_character.transform.position;
                        StartCoroutine("portalSmallAction");
                        CLEAR_END = CLEAR_END_SEQ.WAIT;
                    break;

                    case CLEAR_END_SEQ.DONE:
                        m_gameState = GameState.SHOW_CLEAR_UI;
                        break;
                }

            }

        // ��� ���������
            if(!isBannerShowing){
                isBannerShowing = true;
                MyAdmob.myAdmob.showBanner();
            }

        }
        else if (m_gameState.Equals( GameState.SHOW_CLEAR_UI)){
            ShowClearDialogs();
            m_gameState=GameState.WAIT_FOR_INPUT;

            

        }
        else if(m_gameState.Equals(GameState.WAIT_FOR_INPUT)){


            int temp_gem = PlayerPrefs.GetInt(Values.USER_GET_GEMS);

            switch (CLEAR_UI_STATUS){
                case ClearUI_STATUS.NONE:

                    if(isClearDebugmode){
                        PlayerPrefs.SetInt(Values.USER_GET_GEMS,DebugNumber);
                        PlayerPrefs.Save();
                    }


                    CLEAR_UI_STATUS = ClearUI_STATUS.SHOW_DIALOG;
                    break;
                case ClearUI_STATUS.SHOW_DIALOG:

                    if(!m_canvas.transform.GetChild(12).gameObject.activeSelf){
                        m_canvas.transform.GetChild(12).gameObject.SetActive(true);
                    }
                    if (!m_canvas.transform.GetChild(3).gameObject.activeSelf) {
                        m_canvas.transform.GetChild(3).gameObject.SetActive(true);
                    }
                    CLEAR_UI_STATUS = ClearUI_STATUS.GEM_INCREASE;
                    break;
                
                case ClearUI_STATUS.GEM_INCREASE:
                    int getNumber = PlayerPrefs.GetInt( Values.USER_GET_GEMS);


                    if(getNumber.Equals(0)){
                        if(!m_canvas.transform.GetChild(3).GetChild(1).gameObject.activeSelf){
                            m_canvas.transform.GetChild(3).GetChild(1).gameObject.SetActive(true);
                        }
                    }
                    else{
                        StartCoroutine("textCount", getNumber);
                        string rewardLabel = getNumber.ToString()+" x 2 = "+(getNumber*2).ToString();
                        m_canvas.transform.GetChild(3).GetChild(4).GetChild(0).GetComponent<Text>().text=rewardLabel;
                        CLEAR_UI_STATUS=ClearUI_STATUS.WAIT;
                     }


                break;

                case ClearUI_STATUS.WAIT:

                    if(isRewarded){
                        if (m_canvas.transform.GetChild(3).GetChild(1).gameObject.activeSelf) {
                            m_canvas.transform.GetChild(3).GetChild(1).gameObject.SetActive(false);
                        }
                    }
                break;

              
                case ClearUI_STATUS.GEM_UPRISE:
                // ��ƼŬ ����Ʈ ����.
                m_canvas.transform.GetChild(3).GetChild(1).gameObject.SetActive(false);

                int getUserGems = PlayerPrefs.GetInt(Values.USER_GET_GEMS);

                    StartCoroutine("spawnGems", getUserGems);
                    CLEAR_UI_STATUS = ClearUI_STATUS.WAIT;
                    break;

                case ClearUI_STATUS.SAVE_DATA:

                    //12/1

                   


                    CLEAR_UI_STATUS = ClearUI_STATUS.SAVE_DELAY;
                    StartCoroutine("saveDelayCorutine");
                break;

                case ClearUI_STATUS.SAVE_DELAY:
                 int g = int.Parse(m_canvas.transform.GetChild(12).GetChild(1).GetComponent<Text>().text);


                    PlayerPrefs.SetInt(Values.gem,g);
                    PlayerPrefs.Save();

                    break;

                case ClearUI_STATUS.NEXT_LEVEL:
                    // �̰����� ����.
                    CLEAR_UI_STATUS = ClearUI_STATUS.DONE;
                    isRewarded = false;

                    ++game_stage;

                    PlayerPrefs.SetInt(Values.stage,game_stage);
                    PlayerPrefs.Save();

                    TapToRestartClick();
                    break;

                case ClearUI_STATUS.DOUBLE_REWARD:
                    
                    isRewarded  = true;
                    m_canvas.transform.GetChild(3).GetChild(4).gameObject.SetActive(false);
                    int getGems= PlayerPrefs.GetInt(Values.USER_GET_GEMS);
                    StartCoroutine("textDoubleCount", getGems*2);
                    CLEAR_UI_STATUS = ClearUI_STATUS.WAIT;
                    break;
            };
        }
    }

    IEnumerator saveDelayCorutine(){
        int count  =0;
        while(CLEAR_UI_STATUS.Equals(ClearUI_STATUS.SAVE_DELAY)){
            if(count >= 2){
                CLEAR_UI_STATUS = ClearUI_STATUS.NEXT_LEVEL;
                break;
            }
            else{
                count++;
                yield return new WaitForSeconds(0.5f);
            }
        }
    }

   


    public void DoubleRewardVideoShow(){
        #if UNITY_EDITOR
            CLEAR_UI_STATUS= ClearUI_STATUS.DOUBLE_REWARD;
#else
            MyAdmob.myAdmob.ShowDoubleRewardAds();

#endif
    }



    IEnumerator spawnGems(int count){
        int startnumber=  0;

        bool isDone =false;

        while(true){
            if( startnumber >= count){
                if (isDone) {
                    CLEAR_UI_STATUS=ClearUI_STATUS.SAVE_DATA;
                    break;
                }
                else {
                    isDone = true;
                    yield return new WaitForSeconds(1.0f);
                }
            }
            else{
               

                if (!gemStorage.transform.GetChild(startnumber).gameObject.activeSelf) {
                    gemStorage.transform.GetChild(startnumber).gameObject.SetActive(true);
                    
                    int rx = UnityEngine.Random.Range(-200, 200);
                    int ry = UnityEngine.Random.Range(-100, 100);

                    Vector2 setup = new Vector2(rx,ry);
                    gemStorage.transform.GetChild(startnumber).transform.localPosition = setup;

                }

                startnumber++;

            }
            yield return new WaitForSeconds(0.03f);
        }
    }


    IEnumerator textDoubleCount(int count){
        int zero = PlayerPrefs.GetInt(Values.USER_GET_GEMS);
    
        while (true) {
            if (zero> count) {
                    PlayerPrefs.SetInt(Values.USER_GET_GEMS,count);
                    PlayerPrefs.Save();

                    yield return new WaitForSeconds(1.0f);

                    CLEAR_UI_STATUS=ClearUI_STATUS.GEM_UPRISE;
                break;
            }
            else {

                if (PlayerPrefs.GetInt(Values.sound).Equals(0)) {
                    clear_textCountSound.Play();
                }

                m_canvas.transform.GetChild(3).GetChild(3).GetChild(0).GetComponent<Text>().text=zero.ToString();
                zero++;

            }
            yield return new WaitForSeconds(0.05f);
        }
    }


    IEnumerator textCount(int number){
        int zero = 0;
        while(true){

            if(CLEAR_UI_STATUS.Equals(ClearUI_STATUS.DOUBLE_REWARD)){

                m_canvas.transform.GetChild(3).GetChild(3).GetChild(0).GetComponent<Text>().text=number.ToString();
                break;
            }
            else{

                if(zero > number){
                    m_canvas.transform.GetChild(3).GetChild(4).gameObject.SetActive(true);
                    m_canvas.transform.GetChild(3).GetChild(2).gameObject.SetActive(true);

                    for(int i =0;i< 15; i++){

                        if(CLEAR_UI_STATUS.Equals(ClearUI_STATUS.DOUBLE_REWARD)){
                            break;
                        }
                        else{
                            yield return new WaitForSeconds(0.1f);
                        }
                    }


                    if (!m_canvas.transform.GetChild(3).GetChild(1).gameObject.activeSelf) {
                        m_canvas.transform.GetChild(3).GetChild(1).gameObject.SetActive(true);
                    }

                    
                    break;
                }
                else{

                    if(zero.Equals(0)){
                        m_canvas.transform.GetChild(3).GetChild(3).gameObject.SetActive(true);
                    }

                    if(PlayerPrefs.GetInt( Values.sound).Equals(0)){
                        clear_textCountSound.Play();
                    }


                    m_canvas.transform.GetChild(3).GetChild(3).GetChild(0).GetComponent<Text>().text = zero.ToString();
                    zero++;

                }
            }
            yield return new WaitForSeconds(0.05f);
        }
    }






    void objectResetOption(){
        carParent.transform.localScale = new Vector3(1f,1f,1f);
        warpParticle.transform.localScale = new Vector3(1f,1f,1f);
        CLEAR_END = CLEAR_END_SEQ.INIT;
        int[] sizeModule = new int[4] { 6, 5, 2, 1 };
        GameObject portal = finishObject.transform.GetChild(0).GetChild(0).gameObject;
        for (int i = 0; i<portal.transform.childCount; i++) {
            ParticleSystem.MainModule ps = portal.transform.GetChild(i).GetComponent<ParticleSystem>().main;
            ps.startSize=sizeModule[i];
        }
    }


    IEnumerator portalSmallAction(){
    

        float[] sizeModule = new float[4]{8f,5f,2f,1f};
        /*0 - 8
         *1 - 5
         *2 - 2
         *3 - 1 
         */
        while (true){
           GameObject portal = finishObject.transform.GetChild(0).GetChild(0).gameObject;
           
           if ( sizeModule[0] <= 0.1f){
               warpParticle.transform.GetComponent<ParticleSystem>().Play();
               CLEAR_END = CLEAR_END_SEQ.DONE;
               break;
           }
           else {
                for (int i = 0; i<portal.transform.childCount; i++) {
                    sizeModule[i]-=0.5f;
                    ParticleSystem.MainModule ps = portal.transform.GetChild(i).GetComponent<ParticleSystem>().main;
                    ps.startSize=sizeModule[i];
                }
            }
            yield return new WaitForSeconds(0.03f);
        }

    }


    IEnumerator clearEndMoveAndSmall(){
    
        while(true){
            // 
            if(carParent.transform.localScale.x <= 0.1f){
                CLEAR_END = CLEAR_END_SEQ.EFFECT;
                break;
            }
            else{
                GameObject portal = finishObject.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
                m_character.transform.position = Vector3.Lerp(m_character.transform.position, portal.transform.position, 0.1f);
                carParent.transform.Rotate(new Vector3(30f,0,30f));
                carParent.transform.localScale -= new Vector3(0.03f,0.03f,0.03f);
            }
            yield return new WaitForSeconds(0.03f);
        }
    }





    float m_pillarColorHue = 0f;
    float m_pillarColorHueChangeRate = 0.0015f;
    float m_pillarColorHueChangeMin = 0f;
    float m_pillarColorHueChangeMax = 0.25f;
    const float m_pillarColorSaturation = 0.4f;
    const float m_pillarColorValue = 1f;
    int m_pillarUpdateCount = 0;
    const int m_pillarUpdateCountMax = 5;
    void UpdatePillarColor()
    {
        if (m_pillarUpdateCount <= 0)
        {
            m_pillarUpdateCount = m_pillarUpdateCountMax;
            SetPillarColor(Color.HSVToRGB(m_pillarColorHue, m_pillarColorSaturation, m_pillarColorValue));
        }
        else
        {
            m_pillarUpdateCount--;
        }

        if (m_pillarColorHue > m_pillarColorHueChangeMax || m_pillarColorHue < m_pillarColorHueChangeMin)
        {
            m_pillarColorHueChangeRate *= -1;
        }
        m_pillarColorHue += m_pillarColorHueChangeRate;
    }

    void SetPillarColor(Color color)
    {
        m_pillar.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.color = color;
    }

    public bool IsLastFloor()
    {
        if (m_indexOfCurrentFloor == m_stagePatterns[PlayerPrefs.GetInt(Values.stage)].Length)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public bool isLastBonusFloor(){
        if (b_indexOfCurrentFloor==m_stagePatterns[PlayerPrefs.GetInt(Values.stage)].Length) {
            return true;
        }
        else {
            return false;
        }
    }


    public void IncreaseFloor()
	{
        if(game_stage >0){
            if((game_stage%5).Equals(0)){
                ++b_indexOfCurrentFloor;
                b_currentFloor=b_floors.Find(BonusFloor => BonusFloor.number==b_indexOfCurrentFloor);
                m_canvas.GetChild((int)CanvasChild.Debug).GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text=b_currentFloor.patternDifficulty+" - "+b_currentFloor.patternIndex;
            }
            else {
                ++m_indexOfCurrentFloor;
                m_currentFloor=m_floors.Find(floor => floor.number==m_indexOfCurrentFloor);
                 m_canvas.GetChild((int)CanvasChild.Debug).GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text = m_currentFloor.patternDifficulty + " - " + m_currentFloor.patternIndex;
            }
        }
        else if(game_stage.Equals(0)){
            ++m_indexOfCurrentFloor;
            m_currentFloor=m_floors.Find(floor => floor.number==m_indexOfCurrentFloor);
            m_canvas.GetChild((int)CanvasChild.Debug).GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text=m_currentFloor.patternDifficulty+" - "+m_currentFloor.patternIndex;

        }
        m_obstacleAwakeTimer= 0f;
    }

    public void CreateTowerPiece()
    {
        GameObject stair = m_stair1;
        if(m_towerPieceCount == 1)
        {
            stair = m_stair2;
        }

        if (!m_isRevived)
        {
        

            int currentStage = game_stage;
            bool gem1 = false, gem2 = false, gem3 = false, gem4 = false;

                if (currentStage<40) {
                    if (m_towerPieceCount==0) {
                        gem2=true;
                        gem4 = true;
                        gem1 = false;
                        gem3=false;

                }
                else {
                        gem1 = true;
                        gem3 = true;
                        gem2 = false;
                        gem4 = false;
                }
                   
                }
                else if (currentStage<80) {
                    if (m_towerPieceCount==0) {
                        gem1 = true;
                        gem2=true; 
                        gem3=true;
                        gem4 = false;
                    }
                    else{
                    
                        gem2 = true;
                        gem4 = true;
                        gem1 = false;
                        gem3 = false;
                    }
                   
                  
                }
                else if (currentStage<120) {
                    if (m_towerPieceCount==0) {
                        gem2=true;
                        gem4=true;
                        gem1 = false;
                        gem3 = false;
                    }
                    else{
                        gem3 = true;
                        gem4 = true;
                        gem1 = false;
                        gem2 = false;
                    }
                   
                    
                }
                else if (currentStage<160) {
                    if (m_towerPieceCount==0) {
                        gem2=true; 
                        gem4=true;
                        gem1 = false;
                        gem3 = false;
                    }
                    else{
                        gem2 = true;
                        gem3 = true;
                        gem1 = false;
                        gem4 = false;

                    }
                   
                    
                }
                else {
                    if (m_towerPieceCount==0) {
                        gem2=true; 
                        gem4=true;
                        gem1 = false;
                        gem3 =false;
                    }
                    else{
                        gem1 = true;
                        gem2 = true;
                        gem3 = false;
                        gem4 =false;
                    }
                   
            }



            if ((currentStage%5).Equals(0)) {
                gem1=false;
                gem2=false;
                gem3=false;
                gem4=false;
            }
            



            if (gem1){ 
                m_gems[0 + m_towerPieceCount * 4].GetComponent<Renderer>().enabled = true;
            }
            else{
                m_gems[0+m_towerPieceCount*4].GetComponent<Renderer>().enabled=false;
            }

            if (gem2){
                m_gems[1 + m_towerPieceCount * 4].GetComponent<Renderer>().enabled = true;
            }
            else{
                m_gems[1+m_towerPieceCount*4].GetComponent<Renderer>().enabled=false;
            }
            if (gem3){
                m_gems[2 + m_towerPieceCount * 4].GetComponent<Renderer>().enabled = true;
            }
            else{
                m_gems[2 + m_towerPieceCount * 4].GetComponent<Renderer>().enabled = false;
            }
            if (gem4){
                m_gems[3 + m_towerPieceCount * 4].GetComponent<Renderer>().enabled = true;
            }
            else{
                m_gems[3+m_towerPieceCount*4].GetComponent<Renderer>().enabled=false;

            }
        }

        if (m_isInfiniteMode)
        {
            m_pillar.transform.localScale = new Vector3(m_pillar.transform.localScale.x, 20f * (m_towerPieceCount + 2), m_pillar.transform.localScale.z);
        }

        if(game_stage > 0){
            if( (game_stage%5).Equals(0)){
                CreateBonusGems();
            }
            else {
                CreateObstacles();
            }
        }
        else if(game_stage.Equals(0)){
            CreateObstacles();
        }
        m_towerPieceCount++;
    }

    int GetDifficultyGroup()
    {
        if (m_isTestingLevel)
        {
            return 0;
        }

        int difficulty = 0;

        if (m_isInfiniteMode)
        {
                Debug.Log("이곳 들어옴?");

            if (m_currentFloorIndexToCreate < 2) // difficulty 0  count:1/5
            {
                difficulty = 1;
            }
            else if (m_currentFloorIndexToCreate < 3)  // difficulty 1  count:1/8
            {
                difficulty = 2;
            }
            else if (m_currentFloorIndexToCreate < 4)  // difficulty 2  count:1/18
            {
                difficulty = 3;
            }
            else if (m_currentFloorIndexToCreate < 7)  // difficulty 3  count:-/18
            {
                difficulty = 4;
            }
            else if (m_currentFloorIndexToCreate < 10)  // difficulty 3  count:-/18
            {
                difficulty = 5;
            }
            else if (m_currentFloorIndexToCreate < 15)  // difficulty 3  count:-/18
            {
                difficulty = 6;
            }
            else if (m_currentFloorIndexToCreate < 20)  // difficulty 3  count:-/18
            {
                difficulty = 7;
            }
            else if (m_currentFloorIndexToCreate < 25)  // difficulty 3  count:-/18
            {
                difficulty = 8;
            }
            else if (m_currentFloorIndexToCreate < 30)  // difficulty 3  count:-/18
            {
                difficulty = 9;
            }
            else if (m_currentFloorIndexToCreate < 35)  // difficulty 3  count:-/18
            {
                difficulty = 10;
            }
            else if (m_currentFloorIndexToCreate < 40)  // difficulty 3  count:-/18
            {
                difficulty = 11;
            }
            else if (m_currentFloorIndexToCreate < 45)  // difficulty 3  count:-/18
            {
                difficulty = 12;
            }
            else if (m_currentFloorIndexToCreate < 50)  // difficulty 3  count:-/18
            {
                difficulty = 13;
            }
            else //if (m_currentFloorIndexToCreate < 35)  // difficulty 3  count:-/18
            {
                difficulty = 13;
            }
        }
        else
        {
            difficulty = m_stagePatterns[PlayerPrefs.GetInt(Values.stage)][m_currentFloorIndexToCreate - 1];
        }

        return difficulty;
    }

    int m_lastDifficulty = -1;
    int m_lastRandomIndex = -1;

  
    int GetRandomIndex(int difficultyGroupIndex)
    {
        if (m_isTestingLevel)
        {
            return 0;
        }
        int randomResult = 0;
        do
        {


            randomResult = UnityEngine.Random.Range(0, m_obstaclePatternGroups[difficultyGroupIndex].ObstaclePatterns.Count);
        } while (m_lastDifficulty == difficultyGroupIndex && m_lastRandomIndex == randomResult);

        m_lastDifficulty = difficultyGroupIndex;
        m_lastRandomIndex = randomResult;

        return randomResult;
    }

    int b_Difficulty = -1;
    int b_RandomIndex = -1;


    int GetBonusRandomIndex(int difficultyGroupIndex) {

        if (m_isTestingLevel) {
            return 0;
        }
        int result = 0;
        do {
            result=UnityEngine.Random.Range(0, b_bonusPatternGroups[difficultyGroupIndex].bonusPatterns.Count);
        }
        while (b_Difficulty==difficultyGroupIndex&&b_RandomIndex==result);


        b_Difficulty=difficultyGroupIndex;
        b_RandomIndex=result;

        return result;
    }

    int GetDifficultyBonus(){
        int dif = 0;
        dif=b_stagePatterns[PlayerPrefs.GetInt(Values.stage)][b_currentFloorIndexToCreate-1];

        return dif;
    }


    const float Deep = 0.8f;
    int dcount = 0;
    void CreateBonusGems() {
        for (int i = 0; i<4; i++) {
            b_currentFloorIndexToCreate++;

            if (m_isRevived&&b_currentFloorIndexToCreate<=b_indexOfCurrentFloor) {
                continue;
            }

            int difficultyGroupIndex = GetDifficultyBonus();
            int randomIndex = GetBonusRandomIndex(difficultyGroupIndex);

            BonusFloor floor = new BonusFloor();
            if (!m_isRevived) {

                floor.number=b_currentFloorIndexToCreate;
                floor.patternDifficulty=difficultyGroupIndex;
                floor.patternIndex=randomIndex;
            }
            else {
                difficultyGroupIndex=b_floors[b_currentFloorIndexToCreate-1].patternDifficulty;
                randomIndex=b_floors[b_currentFloorIndexToCreate-1].patternIndex;
                b_floors[b_currentFloorIndexToCreate-1].bonusGems.Clear();
            }

            List<Vector2> obstacles = b_bonusPatternGroups[difficultyGroupIndex].bonusPatterns[randomIndex].bonusGems;

            foreach (Vector2 obstaclePosition in obstacles) {
                float x = obstaclePosition.x;
                float y = 20f*m_towerPieceCount+i*5f+obstaclePosition.y+Mathf.Lerp(0f, 5f, obstaclePosition.x/20f)+0.4f;
                float z = -20.5f;
                switch (i) {
                case 1:
                    x=20.5f;
                    //y = 20f * towerPieceCount + i * 5f + obstacle.y + Mathf.Lerp(0f, 5f, obstacle.x / 20f);
                    z=obstaclePosition.x-20f;
                    break;
                case 2:
                    x=20f-obstaclePosition.x;
                    //y = 20f * towerPieceCount + i * 5f + obstacle.y + Mathf.Lerp(0f, 5f, obstacle.x / 20f);
                    z=0.5f;
                    break;
                case 3:
                    x=-0.5f;
                    //y = 20f * towerPieceCount + i * 5f + obstacle.y + Mathf.Lerp(0f, 5f, obstacle.x / 20f);
                    z=obstaclePosition.x*-1;
                    break;
                }


                BonusGem obstacle = new BonusGem();
                obstacle.posX=obstaclePosition.x;
                obstacle.obj=GetBonusGemFromPool();
                obstacle.obj.transform.position=new Vector3(x, y, z);
                obstacle.obj.SetActive(true);


                if (!m_isRevived) {
                    floor.bonusGems.Add(obstacle);
                }
                else {
                    b_floors[b_currentFloorIndexToCreate-1].bonusGems.Add(obstacle);
                }

            }

            if (!m_isRevived) {
                b_floors.Add(floor);
            }
        }
    }


    void CreateObstacles()
    {
        for (int i = 0; i < 4; i++)
        {
            m_currentFloorIndexToCreate++;

            if (m_isRevived && m_currentFloorIndexToCreate <= m_indexOfCurrentFloor)
            {
                continue;
            }

            int difficultyGroupIndex = GetDifficultyGroup();
            int randomIndex = GetRandomIndex(difficultyGroupIndex);

            Floor floor = new Floor();
            if (!m_isRevived)
            {
				
                floor.number = m_currentFloorIndexToCreate;
                floor.patternDifficulty = difficultyGroupIndex;
                floor.patternIndex = randomIndex;
            }
            else
            {
                difficultyGroupIndex = m_floors[m_currentFloorIndexToCreate - 1].patternDifficulty;
                randomIndex = m_floors[m_currentFloorIndexToCreate - 1].patternIndex;
                m_floors[m_currentFloorIndexToCreate - 1].Obstacles.Clear();
            }

            List<Vector2> obstacles = m_obstaclePatternGroups[difficultyGroupIndex].ObstaclePatterns[randomIndex].Obstacles;



            if (m_isTestingLevel)
            {
                if (m_isTestingCertainLevel)
                {
                    floor.patternIndex = m_testingLevelIndex;
                    obstacles = m_obstaclePatternGroups[m_testingLevelDifficulty].ObstaclePatterns[m_testingLevelIndex].Obstacles;
                    m_obstaclePatternTest = obstacles;
                }
                else
                {
                    obstacles = m_obstaclePatternTest;
                }
            }
			

			foreach (Vector2 obstaclePosition in obstacles)
            {
                float x = obstaclePosition.x;
                float y = 20f * m_towerPieceCount + i * 5f + obstaclePosition.y + Mathf.Lerp(0f, 5f, obstaclePosition.x / 20f) + 0.4f;
                float z = -20.5f + Deep;
                Quaternion rotation = Quaternion.Euler(90f, 0f, 0f);
                RigidbodyConstraints rigidbodyConstraints = RigidbodyConstraints.FreezePositionZ;
                switch (i)
                {
                    case 1:
                        x = 20.5f - Deep;
                        //y = 20f * towerPieceCount + i * 5f + obstacle.y + Mathf.Lerp(0f, 5f, obstacle.x / 20f);
                        z = obstaclePosition.x - 20f;
                        rotation = Quaternion.Euler(90f, 0f, 90f);
                        rigidbodyConstraints = RigidbodyConstraints.FreezePositionX;
                        break;
                    case 2:
                        x = 20f - obstaclePosition.x;
                        //y = 20f * towerPieceCount + i * 5f + obstacle.y + Mathf.Lerp(0f, 5f, obstacle.x / 20f);
                        z = 0.5f - Deep;
                        rotation = Quaternion.Euler(90f, 0f, 0f);
                        rigidbodyConstraints = RigidbodyConstraints.FreezePositionZ;
                        break;
                    case 3:
                        x = -0.5f + Deep;
                        //y = 20f * towerPieceCount + i * 5f + obstacle.y + Mathf.Lerp(0f, 5f, obstacle.x / 20f);
                        z = obstaclePosition.x * -1;
                        rotation = Quaternion.Euler(90f, 0f, 90f);
                        rigidbodyConstraints = RigidbodyConstraints.FreezePositionX;
                        break;
                }
                Obstacle obstacle = new Obstacle();
                obstacle.posX = obstaclePosition.x;
                obstacle.gameObject = GetObstacleFromPool();
                obstacle.gameObject.transform.position = new Vector3(x, y, z);
                obstacle.gameObject.transform.rotation = rotation;
                obstacle.gameObject.GetComponent<Rigidbody>().constraints = rigidbodyConstraints;
                obstacle.gameObject.GetComponent<ObstacleController>().m_section = (Section)i;
                obstacle.gameObject.GetComponent<Renderer>().material.color = m_obstacleColor;
                obstacle.gameObject.SetActive(true);
                
                charController.obstacleObject.Add(obstacle.gameObject);

				if (obstaclePosition.y > 7)
                {
                    obstacle.gameObject.GetComponent<Renderer>().enabled = false;
                    obstacle.gameObject.transform.GetChild(2).GetComponent<Renderer>().enabled = true;
                }
                else if(obstaclePosition.y > 5)
                {
                    obstacle.gameObject.GetComponent<Renderer>().enabled = false;
                    obstacle.gameObject.transform.GetChild(1).GetComponent<Renderer>().enabled = true;
                }
                else if (obstaclePosition.y > 3)
                {
                    obstacle.gameObject.GetComponent<Renderer>().enabled = false;
                    obstacle.gameObject.transform.GetChild(0).GetComponent<Renderer>().enabled = true;
                }

                if (!m_isRevived)
                {
                    floor.Obstacles.Add(obstacle);
                }
                else
                {
                    m_floors[m_currentFloorIndexToCreate - 1].Obstacles.Add(obstacle);
                }

            }

            if (!m_isRevived)
            {
                m_floors.Add(floor);
            }

            if (!m_isRevived)
            {
                int textCreationCount = UnityEngine.Random.Range(1, 4);

                for (int k = 0; k < textCreationCount; k++)
                {
                    //14~24
                    float text2dX = UnityEngine.Random.Range(14f, 23.5f);
                    //1~2
                    float text2dY = UnityEngine.Random.Range(1, 3);

                    float text3dX = text2dX;
                    float text3dY = 20f * m_towerPieceCount + i * 5f + text2dY + Mathf.Lerp(0f, 5f, text2dX / 20f) + 0.4f;
                    float text3dZ = -20.01f;
                    //Quaternion textRotation = Quaternion.Euler(90f, 0f, 0f);
                    Quaternion textRotation = Quaternion.Euler(0f, 0f, 12.5f);
                    switch (i)
                    {
                        case 1:
                            text3dX = 20.01f;
                            //y = 20f * towerPieceCount + i * 5f + obstacle.y + Mathf.Lerp(0f, 5f, obstacle.x / 20f);
                            text3dZ = text2dX - 20f;
                            //textRotation = Quaternion.Euler(90f, 0f, 90f);
                            textRotation = Quaternion.Euler(0f, -90f, 12.5f);
                            break;
                        case 2:
                            text3dX = 20f - text2dX;
                            //y = 20f * towerPieceCount + i * 5f + obstacle.y + Mathf.Lerp(0f, 5f, obstacle.x / 20f);
                            text3dZ = 0.01f;
                            //textRotation = Quaternion.Euler(90f, 0f, 0f);
                            textRotation = Quaternion.Euler(0f, 180f, 12.5f);
                            break;
                        case 3:
                            text3dX = -0.01f;
                            //y = 20f * towerPieceCount + i * 5f + obstacle.y + Mathf.Lerp(0f, 5f, obstacle.x / 20f);
                            text3dZ = text2dX * -1;
                            //textRotation = Quaternion.Euler(90f, 0f, 90f);
                            textRotation = Quaternion.Euler(0f, 90f, 12.5f);
                            break;
                    }

                    GameObject towerText = GetTowerTextFromPool();
                    towerText.transform.position = new Vector3(text3dX, text3dY, text3dZ);
                    towerText.transform.rotation = textRotation;
                    towerText.SetActive(true);
                    string name = RandomName.GetRandomName();
                    towerText.GetComponent<TextMeshPro>().text = name;
                    float x = -3.9f - (name.Length * 0.2f);
                    float y = 0.64f - (name.Length * 0.015f);
                    towerText.transform.GetChild(0).localPosition = new Vector3(x, y, 0f);
                    string spriteName = "dead" + UnityEngine.Random.Range(0, 10);
                    towerText.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(spriteName);
                }
            }

		}
    }


	// Setup Function Definition
	public void CreateBoostBox(){

		float x= 0f;
		float y = 0f;
		float z = 0f;

		for(int i =0; i< 2; i++){
			switch (i) {
				case 0:
					x = 3;
					y= 3;
					z=-20.5f;
					break;
				case 1:
					x= 16;
					y=13;
					z = 0.5f;
					break;
			}

			boostBoxPrefab.transform.position = new Vector3(x,y,z);
			boostBoxList.Add(Instantiate(boostBoxPrefab, boostBoxPrefab.transform.position, boostBoxPrefab.transform.rotation));
		}
	}
	public void CreateSlowBox(){

		float x = 0f;
		float y = 0f;
		float z = 0f;
		for (int i =0; i< 2 ; i++){

			switch (i) {
				case 0:
					x=3;
					y=3;
					z=-20.5f;
					break;
				case 1:
					x=16;
					y=13;
					z=0.5f;
					break;
			}
			slowBoxPrefab.transform.position=new Vector3(x, y, z);
			slowBoxList.Add(Instantiate(slowBoxPrefab,slowBoxPrefab.transform.position,slowBoxPrefab.transform.rotation));
		}
	}

	public void InitBoostAndSlow(){

		int rd = UnityEngine.Random.Range(0,4);

		int frontXRd = 0;
		float frontYRd = 0;
		float frontZ = -20.5f;

		int backXRd = 0;
		float backYRd = 0;
		float backZ = 0.5f;

		for (int i = 0 ; i< 2; i++){
			switch (i) {
				case 0:
					frontXRd =  UnityEngine.Random.Range(3,6);
					switch(frontXRd){
						case 3:
								frontYRd = UnityEngine.Random.Range(1,5);

								if(frontYRd.Equals(1) ||frontYRd.Equals(2)) {
									frontYRd = 2f;
								}
								break;
						case 4:
								frontYRd=2.5f;
								break;

						case 5:
							frontYRd=UnityEngine.Random.Range(4, 7);
							break;
					}

					boostBoxList[i].transform.position=new Vector3(frontXRd, frontYRd, frontZ);
					slowBoxList[i].transform.position = new Vector3(frontXRd,frontYRd,frontZ);

					break;
				case 1:
					backXRd=UnityEngine.Random.Range(14, 17);

					switch (backXRd) {
						case 14:
							int b = UnityEngine.Random.Range(0, 2);

							switch (b) {
								case 0:
									backYRd=13f;
									break;

								case 1:
									backYRd=14f;
									break;
							}
							break;
						case 15:
							int a = UnityEngine.Random.Range(0,2);

							switch(a){
								case 0:
									backYRd=14f;
									break;

								case 1:
									backYRd=13f;
									break;
							}

							break;
						case 16:
							backYRd=UnityEngine.Random.Range(12, 16);

							if(backYRd.Equals(12)){
								backYRd = 12.5f;
							}

							break;
					}

					boostBoxList[i].transform.position=new Vector3(backXRd, backYRd, backZ);
					slowBoxList[i].transform.position=new Vector3(backXRd, backYRd, backZ);


					break;
			}
			
			if (i.Equals(0)){

				switch(rd){

					case 0:
						// boost True
						// slow False
						if(!boostBoxList[i].gameObject.activeSelf){
							boostBoxList[i].gameObject.SetActive(true);
						}

						if(slowBoxList[i].gameObject.activeSelf){
							slowBoxList[i].gameObject.SetActive(false);
						}

						break;
					case 1:

						// boost True
						// slow False
						if (!boostBoxList[i].gameObject.activeSelf) {
							boostBoxList[i].gameObject.SetActive(true);
						}

						if (slowBoxList[i].gameObject.activeSelf) {
							slowBoxList[i].gameObject.SetActive(false);
						}

						break;

					case 2:
						// boost False
						// slow True
						if (boostBoxList[i].gameObject.activeSelf) {
							boostBoxList[i].gameObject.SetActive(false);
						}

						if (!slowBoxList[i].gameObject.activeSelf) {
							slowBoxList[i].gameObject.SetActive(true);
						}
						break;

					case 3:
						// boost False
						// slow True
						if (boostBoxList[i].gameObject.activeSelf) {
							boostBoxList[i].gameObject.SetActive(false);
						}
						if (!slowBoxList[i].gameObject.activeSelf) {
							slowBoxList[i].gameObject.SetActive(true);
						}
						break;
				}
			}
			else{

				switch (rd) {

					case 0:
						// boost True
						// slow False
						if (!boostBoxList[i].gameObject.activeSelf) {
							boostBoxList[i].gameObject.SetActive(true);
						}

						if (slowBoxList[i].gameObject.activeSelf) {
							slowBoxList[i].gameObject.SetActive(false);
						}

						break;
					case 1:

						// boost True
						// slow False
						if (boostBoxList[i].gameObject.activeSelf) {
							boostBoxList[i].gameObject.SetActive(false);
						}

						if (!slowBoxList[i].gameObject.activeSelf) {
							slowBoxList[i].gameObject.SetActive(true);
						}

						break;

					case 2:
						// boost False
						// slow True
						if (!boostBoxList[i].gameObject.activeSelf) {
							boostBoxList[i].gameObject.SetActive(true);
						}

						if (slowBoxList[i].gameObject.activeSelf) {
							slowBoxList[i].gameObject.SetActive(false);
						}
						break;

					case 3:
						// boost False
						// slow True
						if (boostBoxList[i].gameObject.activeSelf) {
							boostBoxList[i].gameObject.SetActive(false);
						}

						if (!slowBoxList[i].gameObject.activeSelf) {
							slowBoxList[i].gameObject.SetActive(true);
						}
						break;
				}
			}
		}

		// Here is Visiable Or Diable
		
	}





    public void ResetClick()
    {
    }

    public void TapToPlayClick()
    {



        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(0).GetChild(0).gameObject.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(0).GetChild(1).gameObject.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(1).GetChild(0).gameObject.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(2).GetChild(0).gameObject.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(3).GetChild(0).gameObject.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(3).GetChild(1).gameObject.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(4).GetChild(0).gameObject.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
		m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(5).GetChild(0).gameObject.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
        isSettingsOpen = false;

        m_gameState = GameState.Playing;

        carParent.transform.GetChild(carNumber).transform.GetChild(5).GetComponent<TrailRenderer>().enabled=true;

        m_canvas.GetChild((int)CanvasChild.TapToPlay).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Score).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Slider).gameObject.SetActive(true);
        m_canvas.GetChild((int)CanvasChild.Sound).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.NoAds).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Settings).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Shop).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Gem).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Quit).gameObject.SetActive(false);
        //m_canvas.GetChild((int)CanvasChild.Cross).gameObject.SetActive(false);
        //m_canvas.GetChild((int)CanvasChild.Cross).GetChild(1).GetComponent<CrossCode>().stopVideo();

        m_canvas.GetChild(24).gameObject.SetActive(false);

        if (PlayerPrefs.GetInt(Values.sound) == 0)
        {
            m_soundStart.Play();
        }
   
    }



    float m_reviveTime = 0f;
    const float REVIVE_TIME_MAX = 5.0f;
    bool m_isRevived = false;

    public void GameEndFail()
    {
        m_gameState = GameState.EndFail;
        m_canvas.GetChild((int)CanvasChild.Fail).gameObject.SetActive(true);

        PlayFailSound();

        m_canvas.GetChild((int)CanvasChild.Score).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Slider).gameObject.SetActive(false);

		UI_STATUS =OVER_UI_STATUS.END_FAIL;
		// Here is StartCorutine 
		StartCoroutine("GameOverCorutine");

	}

	public enum OVER_UI_STATUS {
		NONE,
        END_FAIL,
        PROGRESS_START,
        PROGRESS_WAIT,
        PROGRESS_DONE,
        SHOW_DIALOG,

        DONE
    };

    [Header("GAME_OVER_STATUS")]
	public OVER_UI_STATUS UI_STATUS = OVER_UI_STATUS.NONE;


	IEnumerator GameOverCorutine(){
	// GameOver Sequence Creator
	// Wave Postiion  -4   \\ 23.56f
	//6.533
		while(true){

            if(UI_STATUS.Equals(OVER_UI_STATUS.END_FAIL)){
                    m_canvas.GetChild((int)CanvasChild.Fail).gameObject.GetComponent<Image>().color=new Color(0, 0, 0, 0.7f);
                    UI_STATUS=OVER_UI_STATUS.PROGRESS_START;
			}
            else if(UI_STATUS.Equals(OVER_UI_STATUS.PROGRESS_START)){

                UI_STATUS=OVER_UI_STATUS.PROGRESS_WAIT;
                m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.GetComponent<Slider>().value = 0f;
                if (!m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.activeSelf){
                    m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.SetActive(true);
                }

            }
            else if(UI_STATUS.Equals(OVER_UI_STATUS.PROGRESS_WAIT)){

                float sliderValue = m_canvas.GetChild((int)CanvasChild.Slider).gameObject.GetComponent<Slider>().value;

                float currentValue = m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.GetComponent<Slider>().value;

                if ( currentValue >= sliderValue){
                     UI_STATUS = OVER_UI_STATUS.PROGRESS_DONE;
                     m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.GetComponent<Slider>().value = sliderValue;
                }
                else{
                    m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.GetComponent<Slider>().value += Time.deltaTime*1.2f;

                }

            }
            else if(UI_STATUS.Equals(OVER_UI_STATUS.PROGRESS_DONE)){
                UI_STATUS = OVER_UI_STATUS.SHOW_DIALOG;
            }
			else if (UI_STATUS.Equals(OVER_UI_STATUS.SHOW_DIALOG)) {
			
                game_stage = PlayerPrefs.GetInt(Values.stage,1);



                if(Values.noAds.Equals(1)){
                // Noads �������� ������ ������ �ٷ� ���´�.
                // ��ư�� �ȳ��´� �Ҹ�.
                    m_isRevived = false;

                    m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.GetComponent<Slider>().value=m_canvas.GetChild((int)CanvasChild.Slider).gameObject.GetComponent<Slider>().value;

                }
                else{
                    if ((!m_isRevived || m_canvas.GetChild((int)CanvasChild.Debug).GetChild(1).GetComponent<Toggle>().isOn) && (m_indexOfCurrentFloor != 1 || b_indexOfCurrentFloor != 1))
				    {
				        m_isRevived = false;
				        m_canvas.GetChild((int)CanvasChild.Fail).GetChild(0).gameObject.SetActive(false);

                        m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.SetActive(true);
				    	m_canvas.GetChild((int)CanvasChild.Fail).GetChild(3).gameObject.SetActive(true);
				    	m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.GetComponent<Slider>().value = m_canvas.GetChild((int)CanvasChild.Slider).gameObject.GetComponent<Slider>().value;
				    }
				    else
				    {
				        m_isRevived = false;
				        m_canvas.GetChild((int)CanvasChild.Fail).GetChild(0).gameObject.SetActive(true);
				        m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.SetActive(true);
				    	m_canvas.GetChild((int)CanvasChild.Fail).GetChild(3).gameObject.SetActive(true);
				    	m_canvas.GetChild((int)CanvasChild.Fail).GetChild(2).gameObject.GetComponent<Slider>().value = m_canvas.GetChild((int)CanvasChild.Slider).gameObject.GetComponent<Slider>().value;
				    }
				    
                    m_reviveTime=0f;
				}
				PlayerPrefs.SetInt(Values.achievementDied, PlayerPrefs.GetInt(Values.achievementDied)+1);
				
				AchievementUpdate();
				
				UI_STATUS = OVER_UI_STATUS.DONE;
			}
			else if(UI_STATUS.Equals(OVER_UI_STATUS.DONE)){
				break;
			}

			yield return new WaitForEndOfFrame();
		}
	}


    public void GameEndSuccess(){
        m_gameState = GameState.EndSuccess;
    }

    public void ShowClearDialogs(){
        PlaySuccessSound();
        if (!m_isTestingLevel) {
            m_canvas.GetChild((int)CanvasChild.Score).gameObject.SetActive(false);
            m_canvas.GetChild((int)CanvasChild.Slider).gameObject.SetActive(false);


            LeaderboardUpdate();
            AchievementUpdate();

            if (PlayerPrefs.GetInt(Values.stage)==20) {
                Social.ReportProgress("CgkI0piHyocJEAIQAg", 100.0f, (bool success) => {
                    // handle success or failure
                });
            }
            else if (PlayerPrefs.GetInt(Values.stage)==50) {
                Social.ReportProgress("CgkI0piHyocJEAIQAw", 100.0f, (bool success) => {
                    // handle success or failure
                });
            }
            else if (PlayerPrefs.GetInt(Values.stage)==100) {
                Social.ReportProgress("CgkI0piHyocJEAIQBA", 100.0f, (bool success) => {
                    // handle success or failure
                });
            }
            else if (PlayerPrefs.GetInt(Values.stage)==150) {
                Social.ReportProgress("CgkI0piHyocJEAIQBQ", 100.0f, (bool success) => {
                    // handle success or failure
                });
            }
            else if (PlayerPrefs.GetInt(Values.stage)==195) {
                Social.ReportProgress("CgkI0piHyocJEAIQBg", 100.0f, (bool success) => {
                    // handle success or failure
                });
            }
            int day = PlayerPrefs.GetInt(Values.day);

            if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
           
            }
            else{

                if (day<2) {
                    if (PlayerPrefs.GetInt(Values.stage)<20) {
                        Firebase.Analytics.FirebaseAnalytics.LogEvent("D"+day+"_"+PlayerPrefs.GetInt(Values.stage));
                    }
                    else if (PlayerPrefs.GetInt(Values.stage)<40) {
                        if (PlayerPrefs.GetInt(Values.stage)%2==0) {
                            Firebase.Analytics.FirebaseAnalytics.LogEvent("D"+day+"_"+PlayerPrefs.GetInt(Values.stage));
                        }
                    }
                    else if (PlayerPrefs.GetInt(Values.stage)<70) {
                        if (PlayerPrefs.GetInt(Values.stage)%3==0) {
                            Firebase.Analytics.FirebaseAnalytics.LogEvent("D"+day+"_"+PlayerPrefs.GetInt(Values.stage));
                        }
                    }
                    else if (PlayerPrefs.GetInt(Values.stage)<110) {
                        if (PlayerPrefs.GetInt(Values.stage)%4==0) {
                            Firebase.Analytics.FirebaseAnalytics.LogEvent("D"+day+"_"+PlayerPrefs.GetInt(Values.stage));
                        }
                    }
                    else if (PlayerPrefs.GetInt(Values.stage)<160) {
                        if (PlayerPrefs.GetInt(Values.stage)%5==0) {
                            Firebase.Analytics.FirebaseAnalytics.LogEvent("D"+day+"_"+PlayerPrefs.GetInt(Values.stage));
                        }
                    }
                    else {
                        if (PlayerPrefs.GetInt(Values.stage)%10==0) {
                            Firebase.Analytics.FirebaseAnalytics.LogEvent("D"+day+"_"+PlayerPrefs.GetInt(Values.stage));
                        }
                    }
                }

                Firebase.Analytics.FirebaseAnalytics.LogEvent("Success", "Level", PlayerPrefs.GetInt(Values.stage));
                //FB.LogAppEvent("Level Achieved", PlayerPrefs.GetInt(Values.stage));
            }

            if (PlayerPrefs.GetInt(Values.stage) >= m_stagePatterns.Count-1) {
                m_canvas.GetChild((int)CanvasChild.Success).GetChild(0).GetComponent<Text>().text="MORE LEVEL COMING SOON!";
                m_canvas.GetChild((int)CanvasChild.Success).GetChild(1).GetChild(0).GetComponent<Text>().text="TAP TO PLAY AGAIN!";
            }
        }
        else {
            m_canvas.GetChild((int)CanvasChild.Score).gameObject.SetActive(false);
            m_canvas.GetChild((int)CanvasChild.Slider).gameObject.SetActive(false);

            m_canvas.GetChild((int)CanvasChild.Success).gameObject.SetActive(true);

            m_canvas.GetChild((int)CanvasChild.Success).GetChild(0).GetComponent<Text>().text="";
            m_canvas.GetChild((int)CanvasChild.Success).GetChild(1).GetChild(0).GetComponent<Text>().text="TAP TO PLAY!";
        }



    }



    public void PlayFailSound()
    {
        if (PlayerPrefs.GetInt(Values.sound) == 0)
        {
            m_music.Pause();
            m_soundFail.Play();
        }
    }

    public void PlaySuccessSound()
    {
        if (PlayerPrefs.GetInt(Values.sound) == 0)
        {
            m_music.Pause();
            m_soundSuccess.Play();
        }
    }

    void mainChanceAdsShow(){
       mainAdsSuccess();
    }



    public void ReviveClick()
    {
        ShowReviveAd();
    }

    public void gotoShop(){
        Destroy(GameObject.Find("GameController"));
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);
    }

    public void ShowMainChanceReWards(){
        // �̰��� ���ÿ�.
        // �׷��߸� ������ �������ֽ��ϴ�.
#if UNITY_EDITOR
            mainAdsSuccess();
#else
        //MyAdmob.myAdmob.ShowMainChanceVideo();
#endif

    }

   



    void ShowReviveAd()
    {
#if UNITY_EDITOR
            ReviveAdSuccess();
#else
            //MyAdmob.myAdmob.ShowReviveAd();
#endif
    }

    public bool IsRevived()
    {
        return m_isRevived;
    }

    public void mainAdsSuccess(){
        //mainNoadsBuffActivator();
        Firebase.Analytics.FirebaseAnalytics.LogEvent("main_RewardedAds");
    }


    public void ReviveAdSuccess()
    {
        //noAdsBuffActivator();
        Firebase.Analytics.FirebaseAnalytics.LogEvent("Die_RewardedAds");
    }

    IEnumerator AfterAdClose()
    {
        yield return new WaitForSeconds(0.5f);

        InitializeGame();
        m_character.GetComponent<CharacterControl>().InitializeCharacter();
    }

    void ShowInterstitialAd()
    {
        if(PlayerPrefs.GetInt(Values.noAds) == 1)
        {
            return;
        }

        if(PlayerPrefs.GetInt(Values.NOADS_REWARD).Equals(10)){
            return;
        }


        if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
            return;

        }
        else {
            MyAdmob.myAdmob.ShowInterstitial();

        }
    }

    public void GetGemsClick(){
        CLEAR_UI_STATUS = ClearUI_STATUS.GEM_UPRISE;
        for(int i =1;i < 5; i++){
            m_canvas.transform.GetChild(3).GetChild(i).gameObject.SetActive(false);
        }
    }


    public void TapToRestartClick()
    {
        if (m_adsPlayCount >= ADS_PLAY_COUNT_MAX && m_adsPlayTime > ADS_PLAY_TIME_MAX)
        {
            ShowInterstitialAd();
            m_adsPlayCount = 0;
            m_adsPlayTime = 0f;
        }

        m_isRevived = false;

        charController.obstacleObject.Clear();

        if (game_stage>0) {
            if ((game_stage%5).Equals(0)) {
                
                if(m_obstaclePool.Count.Equals(0)){
                    InitializeObstaclePool();
                }

            }
            else {


                for(int i =0; i< m_obstaclePool.Count; i++){
                    charController.obstacleObject.Add(m_obstaclePool[i].gameObject);
                }
            }
        }
        else {


            for (int i = 0; i<m_obstaclePool.Count; i++) {
                charController.obstacleObject.Add(m_obstaclePool[i].gameObject);
            }


        }



        InitializeGame();
        m_character.GetComponent<CharacterControl>().InitializeCharacter();
        if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
        }
        else{
            Firebase.Analytics.FirebaseAnalytics.LogEvent("TapToRestartButton");
        }
    }

    public void DebugLevelInputEndEdit()
    {

        int result = 0;
        if (int.TryParse(m_canvas.GetChild((int)CanvasChild.Debug).GetChild(0).GetComponent<InputField>().text, out result))
        {
            if (result < m_stagePatterns.Count && result >= 0)
            {
                PlayerPrefs.SetInt(Values.stage, result);

                m_isRevived = false;
                InitializeGame();
                m_character.GetComponent<CharacterControl>().InitializeCharacter();
            }
        }

        m_canvas.GetChild((int)CanvasChild.Debug).GetChild(0).GetComponent<InputField>().text = "";
    }

    public void DebugPatternDiffInputEnd()
    {
        int input = 0;
        if (int.TryParse(m_canvas.GetChild((int)CanvasChild.Debug).GetChild(2).GetComponent<InputField>().text, out input))
        {
            if (input < 14 && input >= 0)
            {
                m_canvas.GetChild((int)CanvasChild.Debug).GetChild(3).gameObject.SetActive(true);
            }
            else
            {
                m_canvas.GetChild((int)CanvasChild.Debug).GetChild(2).GetComponent<InputField>().text = "";
                m_canvas.GetChild((int)CanvasChild.Debug).GetChild(3).gameObject.SetActive(false);

                m_isTestingLevel = false;
                m_isTestingSecondAlso = false;
                m_isTestingCertainLevel = false;

                m_isRevived = false;
                InitializeGame();
                m_character.GetComponent<CharacterControl>().InitializeCharacter();
            }
        }
        else
        {
            m_canvas.GetChild((int)CanvasChild.Debug).GetChild(2).GetComponent<InputField>().text = "";
            m_canvas.GetChild((int)CanvasChild.Debug).GetChild(3).gameObject.SetActive(false);

            m_isTestingLevel = false;
            m_isTestingSecondAlso = false;
            m_isTestingCertainLevel = false;

            m_isRevived = false;
            InitializeGame();
            m_character.GetComponent<CharacterControl>().InitializeCharacter();
        }

    }

    public void DebugPatternIndexInputEnd()
    {
        int difficulty = 0;
        int index = 0;
        if (int.TryParse(m_canvas.GetChild((int)CanvasChild.Debug).GetChild(2).GetComponent<InputField>().text, out difficulty))
        {
            if (int.TryParse(m_canvas.GetChild((int)CanvasChild.Debug).GetChild(3).GetComponent<InputField>().text, out index))
            {
                if (index < m_obstaclePatternGroups[difficulty].ObstaclePatterns.Count && index >= 0)
                {
                    m_isTestingLevel = true;
                    m_isTestingSecondAlso = true;
                    m_isTestingCertainLevel = true;
                    m_testingLevelDifficulty = difficulty;
                    m_testingLevelIndex = index;

                    m_isRevived = false;
                    InitializeGame();
                    m_character.GetComponent<CharacterControl>().InitializeCharacter();
                }
                else
                {
                    m_canvas.GetChild((int)CanvasChild.Debug).GetChild(3).GetComponent<InputField>().text = "";
                }
            }
            else
            {
                m_canvas.GetChild((int)CanvasChild.Debug).GetChild(3).GetComponent<InputField>().text = "";
            }
        }
        else
        {
            m_canvas.GetChild((int)CanvasChild.Debug).GetChild(2).GetComponent<InputField>().text = "";
            m_canvas.GetChild((int)CanvasChild.Debug).GetChild(3).gameObject.SetActive(false);

            m_isTestingLevel = false;
            m_isTestingSecondAlso = false;
            m_isTestingCertainLevel = false;

            m_isRevived = false;
            InitializeGame();
            m_character.GetComponent<CharacterControl>().InitializeCharacter();
        }
     }



    //Sound related
    public AudioSource GetSoundJump()
    {
        return m_soundJump;
    }

    public void SetSoundOn()
    {
        PlayerPrefs.SetInt(Values.sound, 0);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(0).GetChild(1).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(0).GetChild(0).gameObject.SetActive(true);

        if (!m_music.isPlaying)
        {
            m_music.Play();
        }

        if(!Application.internetReachability.Equals(  NetworkReachability.NotReachable)){
            Firebase.Analytics.FirebaseAnalytics.LogEvent("SoundOnButton");
        }


    }

    public void SetSoundOff()
    {
        PlayerPrefs.SetInt(Values.sound, 1);
        m_canvas.GetChild((int)CanvasChild.Sound).GetChild(1).gameObject.SetActive(true);
        m_canvas.GetChild((int)CanvasChild.Sound).GetChild(0).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(0).GetChild(1).gameObject.SetActive(true);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(0).GetChild(0).gameObject.SetActive(false);

        if (m_music.isPlaying)
        {
            m_music.Pause();
        }
        if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
        }
        else{
            Firebase.Analytics.FirebaseAnalytics.LogEvent("SoundOffButton");
        }
    }

    public void SetNotificationOn()
    {
        PlayerPrefs.SetInt(Values.notification, 0);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(3).GetChild(1).gameObject.SetActive(false);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(3).GetChild(0).gameObject.SetActive(true);

    }

    public void SetNotificationOff()
    {
        PlayerPrefs.SetInt(Values.notification, 1);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(3).GetChild(1).gameObject.SetActive(true);
        m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).GetChild(3).GetChild(0).gameObject.SetActive(false);
    }

    public void ShareClick()
    {
#if UNITY_ANDROID
        new NativeShare().SetText("https://play.google.com/store/apps/details?id=com.hadeul.teartower").Share();
#endif

#if UNITY_IOS
        new NativeShare().SetText("itms://apps.apple.com/app/apple-store/id1485195392?mt=8").Share();
#endif
        if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {

        }
        else{
           Firebase.Analytics.FirebaseAnalytics.LogEvent("ShareButton");
        }
    }

//     public void CrossClick()
//     {
// #if UNITY_ANDROID
//         Application.OpenURL("https://play.google.com/store/apps/details?id=com.hadeul.bouncy.demo");
// #endif
// #if UNITY_IOS
//         Application.OpenURL("itms-apps://itunes.apple.com/app/id1485573178");
// #endif
//         if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
//             // NOt Connected;;
//         }
//         else{
//             Firebase.Analytics.FirebaseAnalytics.LogEvent("CrossButton");

//         }
//     }

    bool isSettingsOpen = false;
    public void SettingsClick()
    {
        if (m_canvas.GetChild((int)CanvasChild.Settings).gameObject.GetComponent<Animation>().isPlaying)
        {
            return;
        }

        if (isSettingsOpen)
        {
            m_canvas.GetChild((int)CanvasChild.Settings).gameObject.GetComponent<Animation>().Play("Settings.Close");
            m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).gameObject.GetComponent<Animation>().Play("Settings.Submenus.Close");
            isSettingsOpen = false;
            if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
            }
            else{
                Firebase.Analytics.FirebaseAnalytics.LogEvent("SettingsButtonClose");
            }
        }
        else
        {
            m_canvas.GetChild((int)CanvasChild.Settings).gameObject.GetComponent<Animation>().Play("Settings.Open");
            m_canvas.GetChild((int)CanvasChild.Settings).GetChild(0).gameObject.GetComponent<Animation>().Play("Settings.Submenus.Open");
            isSettingsOpen = true;

            if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
            }
            else{
                Firebase.Analytics.FirebaseAnalytics.LogEvent("SettingsButtonOpen");
            }
        }
    }

    public void GpgsAuth(){

        if(Social.localUser.authenticated) return;

        Social.localUser.Authenticate((bool suc) => {
            if (suc) {
#if UNITY_ANDROID
                ((GooglePlayGames.PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.TOP);

#endif
                PlayerPrefs.SetInt("isSocialLoggedIn", 1);
                PlayerPrefs.Save();
            }

        });

    }



public void LeaderBoardClick()
    {
        if (Social.localUser.authenticated) {
            Social.ShowLeaderboardUI();
        }
        else {

            GpgsAuth();

            StartCoroutine(RunSocialLoginCheck(true));
            Social.localUser.Authenticate((bool success) =>
            {
                print("Social.localUser.Authenticate : "+success);
                Social.ShowLeaderboardUI();
            });
        }
    }

    float socialLoginTime = 0f;
    const float SocialLoginInterval = 0.3f;
    const float SocialLoginTimeEnd = 5f;
    IEnumerator RunSocialLoginCheck(bool isLeader)
    {
        m_canvas.GetChild((int)CanvasChild.SocialLoadingPanel).gameObject.SetActive(true);
        while (SocialLoginTimeEnd > socialLoginTime)
        {
            socialLoginTime += SocialLoginInterval;
           
            if (!Social.localUser.authenticated) {
              

#if UNITY_ANDROID
                GpgsAuth();

              

#endif
            }
            else {
                Social.ShowAchievementsUI();
            }

           

            yield return new WaitForSeconds(SocialLoginInterval);
        }
        m_canvas.GetChild((int)CanvasChild.SocialLoadingPanel).gameObject.SetActive(false);
        socialLoginTime = 0f;
    }

    public void AchievementClick()
    {

        if (Social.localUser.authenticated)
        {
            Social.ShowAchievementsUI();
        }
        else
        {
            GpgsAuth();
            StartCoroutine(RunSocialLoginCheck(false));
            Social.ShowAchievementsUI();
       
        }
        if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
        
        }
        else{
            Firebase.Analytics.FirebaseAnalytics.LogEvent("AchievementButton");
        }
    }

    void LeaderboardUpdate()
    {
        if (!Social.localUser.authenticated)
        {
            return;
        }

#if UNITY_ANDROID
        Social.ReportScore(PlayerPrefs.GetInt(Values.stage), GPGSIds.leaderboard_highest_level, (bool success) => {

        });
#elif UNITY_IOS
        Social.ReportScore(PlayerPrefs.GetInt(Values.stage), "Highest_Level", (bool success) => {

        });
#endif
    }

    void AchievementUpdate()
    {
        if (!Social.localUser.authenticated)
        {
            return;
        }

#if UNITY_ANDROID
        if (PlayerPrefs.GetInt(Values.stage) >= 20) SendAchievement(GPGSIds.achievement_level_20_clear);
        if (PlayerPrefs.GetInt(Values.stage)>=50) SendAchievement(GPGSIds.achievement_level_50_clear);
        if (PlayerPrefs.GetInt(Values.stage) >= 100) SendAchievement(GPGSIds.achievement_level_100_clear);
        if (PlayerPrefs.GetInt(Values.stage) >= 150) SendAchievement(GPGSIds.achievement_level_150_clear);
        if (PlayerPrefs.GetInt(Values.stage) >= 200) SendAchievement(GPGSIds.achievement_level_200_clear);
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 10) SendAchievement(GPGSIds.achievement_10_died);
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 20) SendAchievement(GPGSIds.achievement_20_died);
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 50) SendAchievement(GPGSIds.achievement_50_died);
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 100) SendAchievement(GPGSIds.achievement_100_died);
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 200) SendAchievement(GPGSIds.achievement_200_died);
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 500) SendAchievement(GPGSIds.achievement_500_died);
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 10) SendAchievement(GPGSIds.achievement_10_gem_earned);
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 20) SendAchievement(GPGSIds.achievement_20_gem_earned);
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 50) SendAchievement(GPGSIds.achievement_50_gem_earned);
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 100) SendAchievement(GPGSIds.achievement_100_gem_earned);
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 200) SendAchievement(GPGSIds.achievement_200_gem_earned);
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 500) SendAchievement(GPGSIds.achievement_500_gem_earned);
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 1000) SendAchievement(GPGSIds.achievement_1000_gem_earned);
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 2000) SendAchievement(GPGSIds.achievement_2000_gem_earned);
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 5000) SendAchievement(GPGSIds.achievement_5000_gem_earned);
#elif UNITY_IOS
        if (PlayerPrefs.GetInt(Values.stage) >= 20) SendAchievement("Level_20_clear");
        if (PlayerPrefs.GetInt(Values.stage) >= 50) SendAchievement("Level_50_clear");
        if (PlayerPrefs.GetInt(Values.stage) >= 100) SendAchievement("Level_100_clear");
        if (PlayerPrefs.GetInt(Values.stage) >= 150) SendAchievement("Level_150_clear");
        if (PlayerPrefs.GetInt(Values.stage) >= 200) SendAchievement("Level_200_clear");
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 10) SendAchievement("10_Died");
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 20) SendAchievement("20_Died");
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 50) SendAchievement("50_Died");
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 100) SendAchievement("100_Died");
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 200) SendAchievement("200_Died");
        if (PlayerPrefs.GetInt(Values.achievementDied) >= 500) SendAchievement("500_Died");
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 10) SendAchievement("10_Gem_Earned");
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 20) SendAchievement("20_Gem_Earned");
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 50) SendAchievement("50_Gem_Earned");
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 100) SendAchievement("100_Gem_Earned");
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 200) SendAchievement("200_Gem_Earned");
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 500) SendAchievement("500_Gem_Earned");
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 1000) SendAchievement("1000_Gem_Earned");
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 2000) SendAchievement("2000_Gem_Earned");
        if (PlayerPrefs.GetInt(Values.achievementGem) >= 5000) SendAchievement("5000_Gem_Earned");
#endif

    }

    void SendAchievement(string id)
    {
        if(PlayerPrefs.GetInt(id) == 1)
        {
            return;
        }

        Social.ReportProgress(id, 100.0f, (bool success) => {
            // handle success or failure
            if (success)
            {
                PlayerPrefs.SetInt(id, 1);
            }
        });
    }

    public void SetCharacterIndex(int index)
    {
        PlayerPrefs.SetInt(Values.characterIndex, 0);
    }

    public void UpdateGemUI()
    {
        m_canvas.GetChild((int)CanvasChild.Gem).GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetInt(Values.gem) + "";
    }

    public void DebugGetMoneyClick()
    {
        PlayerPrefs.SetInt(Values.gem, PlayerPrefs.GetInt(Values.gem) + 100);

        UpdateGemUI();
    }

    public void DebugReset()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.DeleteAll();

        UpdateGemUI();

    }

    public void NoAdsOnPurchaseComplete()
    {
        PlayerPrefs.SetInt(Values.noAds, 1);
        m_canvas.GetChild((int)CanvasChild.NoAds).gameObject.SetActive(false);
        if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
        }
        else{
            Firebase.Analytics.FirebaseAnalytics.LogEvent("NoAdsPurchaseComplete");
        }
    }

    public void NoAdsOnPurchaseFail()
    {

    }

    public void QuitNoButtonClick()
    {
        m_canvas.GetChild((int)CanvasChild.Quit).gameObject.SetActive(false);
    }

    public void QuitYesButtonClick()
    {
        Application.Quit();
    }
}

[System.Serializable]
public class ObstaclePattern
{
    //public int difficulty;
    public List<Vector2> Obstacles = new List<Vector2>();
    //public List<ObstaclePosition> Obstacles = new List<ObstaclePosition>();
}

[System.Serializable]
public class BonusGemPattern {
    public List<Vector2> bonusGems = new List<Vector2>();
}




[System.Serializable]
public class ColorGroup
{
    public Color Pillar;
    public Color Plane;
    public Color Obstalce;
    public Color Stair;
    public Color Text;
    public Color Character;
    public Color FinishTrans;
    public Material SkyBox;
    public Material BackgroundTower;
    public float PillarChangeMin;
    public float PillarChangeMax;
}

[System.Serializable]
public class ObstaclePatternGroup
{
    public List<ObstaclePattern> ObstaclePatterns;
}

[System.Serializable]
public class BonusGemsPatternGroup{
    public List<BonusGemPattern> bonusPatterns;

}


public class BonusGem{
    public GameObject obj;
    public float posX;
}

public class BonusFloor{
    public int number;
    public int patternIndex;
    public int patternDifficulty;

    public List<BonusGem> bonusGems = new List<BonusGem>();
}




public class Obstacle
{
    public GameObject gameObject;
    public float posX;
    public bool released = false;
}

public class Floor
{
    public int number;
    public int patternDifficulty;
    public int patternIndex;
    public List<Obstacle> Obstacles = new List<Obstacle>();
}

enum CanvasChild
{
    TapToPlay,
    Score,
    Fail,
    Success,
    Slider,
    Debug,
    Sound,
    NoAds,
    Title,
    Settings,
    Shop,
    //Leader,
    //Achieve,
    Cross,
	Gem,
	Quit,
	ShopPanel,
    SocialLoadingPanel
}
