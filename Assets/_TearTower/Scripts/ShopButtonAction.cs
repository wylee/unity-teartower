﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class ShopButtonAction : MonoBehaviour{
    // Start is called before the first frame update
    public GameObject shopLogicObject;
    private ShopController shopControl;
    int productNumber;

    public float nomalOffset = 0f;
    public float uniqueOffset = 0.5f;
    public float adsOffset = 1.0f;
    public GameObject scrollGroupParent;

    public GameObject buttonList;


    public void CloseNetworkDialog(){
        this.transform.parent.parent.gameObject.SetActive(false);
    }


    public void showRewardvideo(){
        shopControl=shopLogicObject.GetComponent<ShopController>();

        if (Application.internetReachability.Equals(NetworkReachability.NotReachable)){
            if(!shopControl.networkDialog.gameObject.activeSelf) {
                shopControl.networkDialog.gameObject.SetActive(true);
            }
        }
        else{


            if(shopControl.carNumber.Equals(10)){
                // 두번째
                shopControl.ShopAdmob.GetComponent<ShopAdmob>().showRewardOne();
            }
            else if(shopControl.carNumber.Equals(11)){
                shopControl.ShopAdmob.GetComponent<ShopAdmob>().showRewardTwo();
            }
        }

    }





    void allCanvasHide(){
        for (int i = 0; i<3; i++) {
            if (scrollGroupParent.transform.GetChild(i).gameObject.activeSelf) {
                scrollGroupParent.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        for(int i = 0 ; i < buttonList.transform.childCount; i++){
            if(buttonList.transform.GetChild(i).gameObject.activeSelf){
                buttonList.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        shopControl = shopLogicObject.GetComponent<ShopController>();
        shopControl.selectSoundOn();
        shopControl.isSelectedCatBtn=true;
    }

    public void addGem21(){
         int g = PlayerPrefs.GetInt(Values.gem);
         g += 21;

         PlayerPrefs.SetInt(Values.gem,g);
         PlayerPrefs.Save();
    }


    public void openNomalCanvas() {
        allCanvasHide();
        scrollGroupParent.transform.GetChild(0).gameObject.SetActive(true);
        
    }
    public void openSportsCanvas() {
        allCanvasHide();
        scrollGroupParent.transform.GetChild(1).gameObject.SetActive(true);
    }
    public void openVVIPCanvas(){
        allCanvasHide();
        scrollGroupParent.transform.GetChild(2).gameObject.SetActive(true);
        if(PlayerPrefs.GetInt("VVIP_CLICK").Equals(0)){
            if(this.transform.GetChild(1).gameObject.activeSelf){
                this.transform.GetChild(1).gameObject.SetActive(false);
            }
            PlayerPrefs.SetInt("VVIP_CLICK",1);
            PlayerPrefs.Save();
        }
        else{
            if (this.transform.GetChild(1).gameObject.activeSelf) {
                this.transform.GetChild(1).gameObject.SetActive(false);
            }
        }
    }

    public void gotoGameScene(){
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }


    public void playGameScene(){
        if(PlayerPrefs.GetInt(Values.sound) == 0){
            this.GetComponent<AudioSource>().Play();
        }
        this.GetComponent<Button>().interactable = false;
        Invoke("startSound",1.5f);
    }

    void startSound(){
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void resetButton(){

        PlayerPrefs.SetInt(Values.gem,0);
        for(int i =0 ; i< 12; i++){
            string name = Values.unlockCharacter;
            name= name + i.ToString();
            PlayerPrefs.SetInt(name,0);
        }
        PlayerPrefs.SetInt("VVIP_1",0);
        PlayerPrefs.SetInt("VVIP_2",0);

        PlayerPrefs.SetInt("VC_0",0);
        PlayerPrefs.SetInt("VC_1",0);
        PlayerPrefs.SetInt("VVIP_CLICK",0);

        PlayerPrefs.Save();
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);
    }

    public void debugButtonUnlock(){
        string asd = Values.ADS_COUNT + "0";
        string qwe = Values.ADS_COUNT + "1";
        PlayerPrefs.SetInt(asd,0);
        PlayerPrefs.SetInt(qwe,0);

        for (int i =1; i<12 ; i++){
            string p = Values.unlockCharacter;
            p=p + i.ToString();
            PlayerPrefs.SetInt(p,10);
            PlayerPrefs.Save();
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);
    }

    public void DebugButton(){
        int gem = PlayerPrefs.GetInt(Values.gem);
        gem += 350;
        PlayerPrefs.SetInt(Values.gem,gem);
        PlayerPrefs.Save();
    }

    public void itemEquip(){
        shopControl = shopLogicObject.GetComponent<ShopController>();

        shopControl.equipSoundOn();

        shopControl.isPurchasedAndEquip = false;

        shopControl.isSelectedCatBtn = false;

        int number = shopControl.carNumber;
        PlayerPrefs.SetInt(Values.characterIndex,number);
        PlayerPrefs.Save();
    }

    int[] priceSet  = { 0, 70, 150, 180, 210, 200, 450, 700, 850, 1000};


    public void itemPurchased(){
        shopControl = shopLogicObject.GetComponent<ShopController>();
        shopControl.isSelectedCatBtn=false;
        int gem = PlayerPrefs.GetInt(Values.gem);
      
        string productNumber =  Values.unlockCharacter+shopControl.carNumber.ToString();


        if(gem >= priceSet[shopControl.carNumber]){
            // 살수있음.



            if (!Application.internetReachability.Equals(NetworkReachability.NotReachable)) {

                string itemName = string.Empty;

                if(shopControl.carNumber <=4 ){
                    itemName = "NOC_"+shopControl.carNumber.ToString();
                }
                else if(shopControl.carNumber > 4 &&shopControl.carNumber <= 9){
                    itemName = "SPC_"+shopControl.carNumber.ToString();
                }

                Firebase.Analytics.FirebaseAnalytics.LogEvent(
                      Firebase.Analytics.FirebaseAnalytics.EventSpendVirtualCurrency,
                      new Firebase.Analytics.Parameter[] {
                    new Firebase.Analytics.Parameter(
                      Firebase.Analytics.FirebaseAnalytics.ParameterItemName, itemName),
                    new Firebase.Analytics.Parameter(
                      Firebase.Analytics.FirebaseAnalytics.ParameterValue, 10),
                    new Firebase.Analytics.Parameter(
                      Firebase.Analytics.FirebaseAnalytics.ParameterVirtualCurrencyName, "GEM"),
                      }
                );
            }

            shopControl.purchaseSoundOn();

          gem-= priceSet[shopControl.carNumber];
          PlayerPrefs.SetInt(productNumber,10);
          PlayerPrefs.SetInt(Values.gem,gem);

          int number = shopControl.carNumber;
          PlayerPrefs.SetInt(Values.characterIndex, number);
          PlayerPrefs.Save();
          PlayerPrefs.Save();


            if (shopControl.carNumber <= 4){
            shopControl.nomalGroup.transform.GetChild(0).GetChild(0).GetChild(shopControl.carNumber).GetChild(4).gameObject.GetComponent<ParticleSystem>().Play();

            for(int i =0;i< 4; i++){
                shopControl.nomalGroup.transform.GetChild(0).GetChild(0).GetChild(shopControl.carNumber).GetChild(4).GetChild(i).GetComponent<ParticleSystem>().Play();
            }


            }
            else if(shopControl.carNumber <= 9){
                shopControl.uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(shopControl.carNumber-5).GetChild(4).gameObject.GetComponent<ParticleSystem>().Play();
                for (int i =0;i< 4; i++){
                    shopControl.uniqueGroup.transform.GetChild(0).GetChild(0).GetChild(shopControl.carNumber -5).GetChild(4).GetChild(i).gameObject.GetComponent<ParticleSystem>().Play();
                }
            }
          
          this.gameObject.SetActive(false);
          shopControl.isPurchasedAndEquip=true;
        }

    }



    public void itemSelect(){
        shopControl= shopLogicObject.GetComponent<ShopController>();
        /*
         * CharacterIndex = 현재 장착된 인덱스값을 뜻함.
         * selectIndex = 상점에서 선택된 인덱스를 뜻함
         * 
         * unlock  is 0  = 상품이 열리지 않음 ( 구매하기전)
         * unlock  is 1  = 상품이 열려있음  (구며완료된 상태)
         * 
         */

         int it = int.Parse(this.name);


         if(it >= 10){
         // 느낌표 처리해주는곳
            switch(it){
                case 10:
                    if (PlayerPrefs.GetInt("VVIP_1").Equals(0)){
                        if (this.transform.GetChild(2).gameObject.activeSelf) {
                            this.transform.GetChild(2).gameObject.SetActive(false);
                        }
                         PlayerPrefs.GetInt("VVIP_1",1);
                         PlayerPrefs.Save();
                    }
                break;

                case 11:
                if (PlayerPrefs.GetInt("VVIP_2").Equals(0)) {
                    if (this.transform.GetChild(2).gameObject.activeSelf) {
                        this.transform.GetChild(2).gameObject.SetActive(false);
                    }
                        PlayerPrefs.GetInt("VVIP_2", 1);
                        PlayerPrefs.Save();
                }
                
                break;
            }
         }

        shopControl.isSelectedCatBtn=false;

        if ( shopControl.SELECT_STATUS.Equals(ShopController.SELECT.NONE)){
            productNumber = int.Parse( this.transform.name);
            PlayerPrefs.SetInt(Values.selectIndex, productNumber);
            PlayerPrefs.Save();
            shopControl.SELECT_STATUS = ShopController.SELECT.SELECT;
            shopControl.carNumber = int.Parse(this.transform.name);
        }

        string st = Values.unlockCharacter + it.ToString();
        if(PlayerPrefs.GetInt(st).Equals(10)){
            shopControl.equipSoundOn();

            shopControl.setEquipNumber(it);
            shopControl.startPlayBtn.gameObject.SetActive(true);

            shopControl.isPurchasedAndEquip=false;

            shopControl.isSelectedCatBtn=false;

            int number = shopControl.carNumber;
            PlayerPrefs.SetInt(Values.characterIndex, number);
            PlayerPrefs.Save();

        }
        else{
            shopControl.selectSoundOn();
      
        }


    }
    // 화살표 이전 목록 보여주기.
}

