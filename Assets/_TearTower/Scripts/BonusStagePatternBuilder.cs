﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusStagePatternBuilder : MonoBehaviour
{
    public static List<int[]> BuildStagePattern() {
        List<int[]> stagePattern = new List<int[]>();
        stagePattern.Add(new int[] { 0, 0, 0, 0, 0, 0, 0, 1 });
        stagePattern.Add(new int[] { 0, 0, 0, 0, 0, 0, 1, 1 });
        stagePattern.Add(new int[] { 0, 0, 0, 1, 0, 1, 1, 1 });
        stagePattern.Add(new int[] { 0, 0, 0, 0, 0, 1, 1, 1 });
        stagePattern.Add(new int[] { 0, 0, 0, 1, 1, 0, 1, 1 });
        stagePattern.Add(new int[] { 0, 0, 0, 0, 1, 1, 1, 0 });
        stagePattern.Add(new int[] { 0, 0, 0, 1, 1, 1, 0, 1 });
        stagePattern.Add(new int[] { 0, 1, 0, 1, 1, 1, 1, 0 });
        stagePattern.Add(new int[] { 0, 0, 0, 1, 1, 1, 1, 1 });
        stagePattern.Add(new int[] { 0, 0, 0, 1, 1, 1, 1, 1 });
        stagePattern.Add(new int[] { 1, 0, 1, 1, 1, 1, 1, 1 });
        stagePattern.Add(new int[] { 0, 0, 0, 1, 1, 1, 1, 2 });
        stagePattern.Add(new int[] { 0, 1, 1, 1, 1, 1, 2, 2 });
        stagePattern.Add(new int[] { 0, 0, 0, 1, 2, 1, 0, 2 });
        stagePattern.Add(new int[] { 0, 0, 0, 1, 2, 0, 1, 2 });
        stagePattern.Add(new int[] { 1, 1, 1, 2, 1, 1, 2, 2 });
        stagePattern.Add(new int[] { 1, 1, 1, 1, 2, 1, 1, 1 });
        stagePattern.Add(new int[] { 1, 1, 1, 1, 2, 1, 1, 2 });
        stagePattern.Add(new int[] { 1, 1, 1, 2, 2, 1, 1, 2 });
        stagePattern.Add(new int[] { 1, 1, 1, 2, 2, 1, 1, 2 });
        stagePattern.Add(new int[] { 1, 1, 1, 2, 2, 2, 2, 2 });
        stagePattern.Add(new int[] { 1, 1, 1, 2, 2, 2, 2, 2 });
        stagePattern.Add(new int[] { 1, 0, 2, 2, 2, 0, 2, 2 });
        stagePattern.Add(new int[] { 2, 2, 2, 2, 1, 2, 2, 2 });
        stagePattern.Add(new int[] { 2, 2, 2, 2, 2, 2, 2, 2 });
        stagePattern.Add(new int[] { 2, 2, 1, 2, 0, 2, 1, 2 });
        stagePattern.Add(new int[] { 2, 2, 2, 1, 2, 2, 2, 2 });
        stagePattern.Add(new int[] { 2, 1, 2, 0, 2, 2, 1, 2 });
        stagePattern.Add(new int[] { 2, 1, 2, 2, 2, 2, 1, 2 });
        stagePattern.Add(new int[] { 2, 2, 0, 2, 2, 1, 2, 2 });
        // 1~ 29
        //123  - 3

        stagePattern.Add(new int[] { 3, 3, 3, 3, 3, 3, 3, 4 }); //4
        stagePattern.Add(new int[] { 3, 3, 5, 3, 3, 4, 3, 4 }); //5
        stagePattern.Add(new int[] { 3, 3, 4, 3, 3, 3, 3, 4 }); //6
        stagePattern.Add(new int[] { 4, 3, 3, 4, 3, 3, 3, 4 }); //7
        stagePattern.Add(new int[] { 3, 4, 3, 3, 3, 5, 4, 4 }); //8
        stagePattern.Add(new int[] { 3, 3, 3, 3, 3, 3, 4, 4 }); //9
        stagePattern.Add(new int[] { 5, 3, 4, 3, 3, 3, 5, 4 }); //10
        stagePattern.Add(new int[] { 4, 3, 3, 5, 3, 4, 3, 4 }); //10
        stagePattern.Add(new int[] { 3, 3, 3, 3, 3, 4, 4, 4 }); //10
        stagePattern.Add(new int[] { 4, 3, 3, 3, 4, 3, 3, 4 }); //10
        stagePattern.Add(new int[] { 3, 4, 3, 4, 4, 5, 3, 4 }); //10
        stagePattern.Add(new int[] { 4, 5, 4, 4, 3, 3, 3, 4 }); //10
        stagePattern.Add(new int[] { 5, 5, 4, 5, 3, 5, 3, 4 }); //10
        stagePattern.Add(new int[] { 3, 3, 5, 5, 5, 5, 3, 4 }); //10
        stagePattern.Add(new int[] { 3, 3, 3, 3, 3, 3, 3, 4 }); //10
        stagePattern.Add(new int[] { 3, 3, 3, 5, 5, 3, 3, 5 }); //10
        stagePattern.Add(new int[] { 3, 3, 3, 3, 3, 3, 3, 5 }); //10
        stagePattern.Add(new int[] { 4, 5, 4, 4, 5, 4, 4, 5 }); //10
        stagePattern.Add(new int[] { 4, 5, 4, 5, 4, 4, 5, 5 }); //10
        stagePattern.Add(new int[] { 5, 5, 5, 5, 5, 4, 3, 5 }); //10
        // 30~49
        // 3,4,5   - 4


        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 6, 6, 7 }); //1
        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 6, 7, 7 }); //2
        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 7, 7, 7 }); //6
        stagePattern.Add(new int[] { 6, 6, 6, 6, 7, 7, 7, 7 }); //7
        stagePattern.Add(new int[] { 6, 6, 6, 7, 7, 7, 7, 7 }); //8
        stagePattern.Add(new int[] { 6, 6, 6, 6, 7, 7, 7, 8 }); //6
        stagePattern.Add(new int[] { 6, 6, 6, 7, 7, 7, 7, 8 }); //7
        stagePattern.Add(new int[] { 6, 6, 7, 7, 7, 7, 7, 8 }); //8
        stagePattern.Add(new int[] { 6, 7, 7, 7, 7, 7, 7, 8 }); //9
        stagePattern.Add(new int[] { 7, 7, 7, 7, 7, 7, 7, 8 }); //10
        stagePattern.Add(new int[] { 7, 8, 8, 6, 8, 6, 6, 7 }); //1
        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 6, 7, 7 }); //2
        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 7, 7, 7 }); //6
        stagePattern.Add(new int[] { 6, 6, 6, 6, 7, 7, 7, 7 }); //7
        stagePattern.Add(new int[] { 6, 6, 6, 7, 7, 7, 7, 7 }); //8
        stagePattern.Add(new int[] { 6, 6, 6, 6, 7, 7, 7, 8 }); //6
        stagePattern.Add(new int[] { 6, 6, 6, 7, 7, 7, 7, 8 }); //7
        stagePattern.Add(new int[] { 6, 6, 7, 7, 7, 7, 7, 8 }); //8
        stagePattern.Add(new int[] { 6, 7, 7, 7, 7, 7, 7, 8 }); //9
        stagePattern.Add(new int[] { 7, 7, 7, 7, 7, 7, 7, 8 }); //10
        stagePattern.Add(new int[] { 8, 8, 6, 8, 6, 8, 6, 7 }); //1
        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 8, 7, 7 }); //2
        stagePattern.Add(new int[] { 6, 6, 6, 6, 6, 7, 8, 7 }); //6
        stagePattern.Add(new int[] { 6, 8, 6, 8, 7, 8, 8, 7 }); //7
        stagePattern.Add(new int[] { 8, 6, 6, 7, 7, 7, 7, 7 }); //8
        stagePattern.Add(new int[] { 6, 6, 8, 6, 7, 7, 7, 8 }); //6
        stagePattern.Add(new int[] { 6, 6, 6, 7, 7, 7, 7, 8 }); //7
        stagePattern.Add(new int[] { 8, 6, 7, 8, 7, 7, 7, 8 }); //8
        stagePattern.Add(new int[] { 6, 7, 8, 7, 7, 8, 7, 8 }); //9
        stagePattern.Add(new int[] { 8, 7, 8, 7, 8, 7, 7, 8 }); //10
        stagePattern.Add(new int[] { 8, 8, 8, 6, 6, 6, 7, 7 }); //2
        stagePattern.Add(new int[] { 8, 6, 8, 8, 8, 7, 7, 7 }); //8
        stagePattern.Add(new int[] { 8, 8, 8, 8, 7, 7, 8, 7 }); //7
        stagePattern.Add(new int[] { 8, 8, 8, 7, 8, 7, 8, 7 }); //8
        stagePattern.Add(new int[] { 6, 6, 6, 6, 7, 7, 7, 8 }); //6
        stagePattern.Add(new int[] { 6, 6, 6, 7, 7, 7, 7, 8 }); //7
        stagePattern.Add(new int[] { 6, 6, 7, 7, 7, 7, 7, 8 }); //8
        stagePattern.Add(new int[] { 6, 8, 7, 8, 7, 8, 7, 8 }); //9
        stagePattern.Add(new int[] { 7, 8, 8, 8, 8, 8, 8, 8 }); //10
        //50~89
        //678   5

        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 9, 9, 10 }); //1
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 9, 10, 10 }); //2
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 10, 10, 10 }); //3
        stagePattern.Add(new int[] { 9, 9, 9, 9, 10, 10, 10, 10 }); //9
        stagePattern.Add(new int[] { 9, 9, 9, 10, 10, 10, 10, 10 }); //10
        stagePattern.Add(new int[] { 9, 9, 9, 9, 10, 10, 10, 11 }); //11
        stagePattern.Add(new int[] { 9, 9, 9, 10, 10, 10, 10, 11 }); //7
        stagePattern.Add(new int[] { 9, 9, 10, 10, 10, 10, 10, 11 }); //8
        stagePattern.Add(new int[] { 9, 10, 10, 10, 10, 10, 10, 11 }); //9
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 10, 11 }); //10
        stagePattern.Add(new int[] { 11, 9, 9, 8, 9, 10, 9, 10 }); //1
        stagePattern.Add(new int[] { 11, 9, 8, 9, 9, 10, 10, 10 }); //2
        stagePattern.Add(new int[] { 11, 9, 9, 9, 9, 10, 10, 10 }); //3
        stagePattern.Add(new int[] { 11, 9, 9, 9, 8, 10, 10, 8 }); //9
        stagePattern.Add(new int[] { 11, 9, 9, 10, 10, 10, 10, 8 }); //10
        stagePattern.Add(new int[] { 11, 9, 9, 9, 10, 10, 10, 11 }); //11
        stagePattern.Add(new int[] { 9, 11, 9, 10, 10, 10, 10, 11 }); //7
        stagePattern.Add(new int[] { 11, 9, 10, 10, 10, 10, 10, 11 }); //8
        stagePattern.Add(new int[] { 9, 10, 10, 10, 10, 10, 10, 11 }); //9
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 10, 11 }); //10
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 9, 9, 10 }); //1
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 9, 10, 10 }); //2
        stagePattern.Add(new int[] { 9, 9, 9, 9, 9, 10, 10, 10 }); //3
        stagePattern.Add(new int[] { 9, 9, 9, 9, 10, 10, 10, 10 }); //9
        stagePattern.Add(new int[] { 9, 9, 9, 10, 10, 10, 10, 10 }); //10
        stagePattern.Add(new int[] { 9, 9, 9, 9, 10, 10, 10, 11 }); //11
        stagePattern.Add(new int[] { 9, 9, 9, 10, 10, 10, 10, 11 }); //7
        stagePattern.Add(new int[] { 9, 9, 10, 10, 10, 10, 10, 11 }); //8
        stagePattern.Add(new int[] { 9, 10, 10, 10, 10, 10, 10, 11 }); //9
        stagePattern.Add(new int[] { 10, 10, 10, 10, 10, 10, 10, 11 }); //10
        stagePattern.Add(new int[] { 11, 11, 9, 10, 11, 10, 11, 10 }); //1
        stagePattern.Add(new int[] { 10, 10, 10, 9, 11, 9, 10, 10 }); //2
        stagePattern.Add(new int[] { 11, 9, 9, 11, 11, 10, 11, 10 }); //3
        stagePattern.Add(new int[] { 11, 11, 11, 10, 10, 10, 10, 10 }); //11
        stagePattern.Add(new int[] { 11, 11, 11, 10, 10, 9, 10, 10 }); //10
        stagePattern.Add(new int[] { 11, 11, 9, 11, 10, 10, 9, 11 }); //11
        stagePattern.Add(new int[] { 11, 9, 11, 11, 10, 10, 10, 11 }); //7
        stagePattern.Add(new int[] { 11, 11, 10, 10, 10, 10, 10, 11 }); //8
        stagePattern.Add(new int[] { 11, 10, 10, 10, 10, 9, 10, 11 }); //11
        stagePattern.Add(new int[] { 11, 11, 10, 10, 9, 11, 11, 11 }); //10
        //90~119
        //9 10 11 - 6


        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 12 }); //1
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 12, 13 }); //2
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 13, 12 }); //3
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 13, 12, 12 }); //4
        stagePattern.Add(new int[] { 12, 12, 12, 12, 13, 12, 12, 12 }); //5
        stagePattern.Add(new int[] { 12, 12, 12, 13, 12, 12, 12, 12 }); //6
        stagePattern.Add(new int[] { 12, 12, 13, 12, 12, 12, 12, 12 }); //7
        stagePattern.Add(new int[] { 12, 13, 12, 12, 12, 12, 12, 12 }); //8
        stagePattern.Add(new int[] { 13, 12, 12, 12, 12, 12, 12, 12 }); //9
        stagePattern.Add(new int[] { 13, 13, 12, 12, 12, 12, 12, 12 }); //10
        stagePattern.Add(new int[] { 12, 13, 13, 12, 12, 12, 12, 12 }); //1
        stagePattern.Add(new int[] { 12, 12, 13, 13, 12, 12, 12, 12 }); //2
        stagePattern.Add(new int[] { 12, 12, 12, 13, 13, 12, 12, 12 }); //3
        stagePattern.Add(new int[] { 12, 12, 12, 12, 13, 13, 12, 12 }); //4
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 13, 13, 12 }); //5
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 12, 13, 13}); //6
        stagePattern.Add(new int[] { 12, 12, 12, 12, 12, 13, 12, 13 }); //7
        stagePattern.Add(new int[] { 12, 12, 12, 12, 13, 12, 13, 12 }); //8
        stagePattern.Add(new int[] { 12, 12, 12, 13, 12, 13, 12, 12 }); //9
        stagePattern.Add(new int[] { 12, 12, 13, 12, 13, 12, 12, 12 }); //10
        stagePattern.Add(new int[] { 12, 13, 12, 13, 12, 12, 12, 12 }); //1
        stagePattern.Add(new int[] { 13, 12, 13, 12, 12, 12, 12, 12 }); //2

        stagePattern.Add(new int[] { 13, 13, 13, 12, 12, 12, 12, 12 }); //3
        stagePattern.Add(new int[] { 12, 12, 13, 13, 13, 12, 12, 12 }); //4
        stagePattern.Add(new int[] { 12, 12, 12, 12, 13, 13, 13, 12 }); //5 
        stagePattern.Add(new int[] { 12, 12, 12, 12, 13, 12, 13, 13}); //6
        stagePattern.Add(new int[] { 12, 12, 12, 13, 12, 12, 13, 12 }); //7
        stagePattern.Add(new int[] { 12, 13, 12, 13, 13, 12, 12, 12 }); //8
        stagePattern.Add(new int[] { 13, 13, 12, 12, 13, 12, 12, 12 }); //9
        stagePattern.Add(new int[] { 13, 12, 13, 12, 13, 12, 12, 12 }); //10
        stagePattern.Add(new int[] { 13, 12, 12, 13, 12, 12, 13, 12 }); //1
        stagePattern.Add(new int[] { 13, 12, 12, 13, 13, 12, 12, 13}); //2
        stagePattern.Add(new int[] { 12, 13, 12, 12, 13, 13, 12, 13}); //3
        stagePattern.Add(new int[] { 12, 13, 12, 13, 12, 13, 12, 13 }); //4
        stagePattern.Add(new int[] { 13, 13, 12, 12, 13, 12, 13, 12 }); //5
        stagePattern.Add(new int[] { 12, 13, 12, 13, 13, 12, 12, 13}); //6

        stagePattern.Add(new int[] { 13, 13, 12, 12, 13, 12, 14, 12 }); //7
        stagePattern.Add(new int[] { 12, 13, 12, 12, 12, 14, 13, 12 }); //8
        stagePattern.Add(new int[] { 12, 12, 12, 12, 14, 12, 12, 12 }); //9
        stagePattern.Add(new int[] { 12, 12, 12, 14, 13, 12, 12, 13 }); //10
        stagePattern.Add(new int[] { 12, 12, 14, 12, 12, 13, 13, 12 }); //1
        stagePattern.Add(new int[] { 12, 14, 12, 12, 12, 12, 12, 13 }); //2
        stagePattern.Add(new int[] { 14, 12, 12, 13, 12, 14, 12, 13}); //3
        stagePattern.Add(new int[] { 12, 14, 12, 12, 13, 12, 13, 12 }); //4
        stagePattern.Add(new int[] { 12, 13, 14, 12, 13, 12, 12, 13}); //5
        stagePattern.Add(new int[] { 12, 14, 12, 14, 12, 12, 13, 12 }); //6
        stagePattern.Add(new int[] { 12, 14, 12, 13, 14, 13, 12, 12 }); //7
        stagePattern.Add(new int[] { 12, 12, 14, 12, 12, 14, 13, 14}); //8
        stagePattern.Add(new int[] { 13, 12, 14, 13, 12, 12, 14, 13}); //9
        stagePattern.Add(new int[] { 13, 12, 13, 14, 12, 14, 12, 14 }); //10

        stagePattern.Add(new int[] { 13, 13, 14, 14, 14, 13, 14, 14 }); //1
        stagePattern.Add(new int[] { 14, 14, 13, 14, 14, 14, 13, 14 }); //2
        stagePattern.Add(new int[] { 14, 14, 14, 13, 14, 13, 14, 14 }); //3
        stagePattern.Add(new int[] { 14, 14, 14, 14, 14, 14, 13, 14 }); //4
        stagePattern.Add(new int[] { 14, 14, 14, 14, 13, 14, 14, 14 }); //5
        stagePattern.Add(new int[] { 14, 13, 14, 14, 14, 14, 14, 14 }); //6
        stagePattern.Add(new int[] { 14, 14, 14, 14, 13, 14, 14, 14 }); //7
        stagePattern.Add(new int[] { 14, 14, 13, 14, 14, 14, 14, 14 }); //8
        stagePattern.Add(new int[] { 14, 14, 14, 14, 14, 13, 14, 14 }); //9
        stagePattern.Add(new int[] { 14, 14, 14, 14, 14, 14, 13, 14 }); //10
        // 120~ 169
        // 12  13  14  - 7

        stagePattern.Add(new int[] { 16, 15, 15, 15, 15, 15, 15, 16 }); //1
        stagePattern.Add(new int[] { 15, 15, 15, 15, 15, 15, 16, 15 }); //2
        stagePattern.Add(new int[] { 15, 15, 15, 15, 15, 16, 15, 15 }); //3
        stagePattern.Add(new int[] { 15, 15, 15, 15, 16, 15, 15, 15 }); //4
        stagePattern.Add(new int[] { 15, 15, 15, 16, 15, 15, 15, 15 }); //15
        stagePattern.Add(new int[] { 15, 15, 16, 15, 15, 15, 15, 15 }); //16
        stagePattern.Add(new int[] { 15, 16, 15, 15, 15, 15, 15, 15 }); //17
        stagePattern.Add(new int[] { 16, 15, 15, 15, 15, 15, 15, 15 }); //8
        stagePattern.Add(new int[] { 16, 15, 16, 15, 15, 15, 15, 15 }); //9
        stagePattern.Add(new int[] { 15, 16, 15, 16, 15, 15, 15, 15 }); //10
        stagePattern.Add(new int[] { 15, 15, 16, 15, 16, 15, 15, 15 }); //1
        stagePattern.Add(new int[] { 15, 15, 15, 16, 15, 16, 15, 15 }); //2
        stagePattern.Add(new int[] { 15, 15, 15, 15, 16, 15, 16, 15 }); //3
        stagePattern.Add(new int[] { 15, 15, 15, 15, 15, 16, 15, 16}); //4
        stagePattern.Add(new int[] { 15, 15, 16, 15, 16, 15, 16, 15 }); //15
        stagePattern.Add(new int[] { 15, 15, 15, 16, 15, 16, 15, 16}); //16
        stagePattern.Add(new int[] { 16, 15, 16, 15, 16, 15, 15, 15 }); //17
        stagePattern.Add(new int[] { 15, 15, 15, 16, 15, 15, 15, 16}); //8
        stagePattern.Add(new int[] { 16, 15, 16, 15, 16, 15, 15, 15 }); //9
        stagePattern.Add(new int[] { 15, 16, 15, 16, 15, 16, 15, 15}); //10


        stagePattern.Add(new int[] { 16, 15, 16, 15, 16, 15, 16, 15 }); //1
        stagePattern.Add(new int[] { 15, 16, 15, 16, 15, 16, 15, 16}); //2
        stagePattern.Add(new int[] { 16, 16, 16, 16, 15, 16, 15, 15 }); //3
        stagePattern.Add(new int[] { 15, 15, 16, 15, 16, 16, 16, 16}); //4
        stagePattern.Add(new int[] { 15, 16, 15, 16, 15, 16, 15, 16}); //15
        stagePattern.Add(new int[] { 15, 16, 15, 16, 16, 16, 16, 16 }); //16
        stagePattern.Add(new int[] { 16, 16, 16, 16, 16, 15, 16, 15 }); //17
        stagePattern.Add(new int[] { 15, 16, 15, 16, 15, 16, 16, 16}); //8
        stagePattern.Add(new int[] { 17, 16, 16, 15, 16, 15, 16, 15 }); //9
        stagePattern.Add(new int[] { 16, 17, 16, 15, 16, 15, 16, 15 }); //10
        stagePattern.Add(new int[] { 15, 16, 17, 16, 15, 16, 15, 16}); //1
        stagePattern.Add(new int[] { 16, 15, 16, 17, 15, 16, 16, 16}); //2
        stagePattern.Add(new int[] { 16, 16, 15, 16, 17, 16, 15, 16}); //3
        stagePattern.Add(new int[] { 15, 16, 16, 16, 15, 17, 16, 16 }); //4
        stagePattern.Add(new int[] { 15, 16, 16, 15, 16, 16, 17, 16}); //15
        stagePattern.Add(new int[] { 15, 16, 15, 16, 15, 16, 16, 17}); //16
        stagePattern.Add(new int[] { 16, 16, 16, 15, 17, 16, 17, 16}); //17
        stagePattern.Add(new int[] { 16, 16, 16, 17, 15, 17, 15, 16 }); //8
        
        stagePattern.Add(new int[] { 15, 16, 16, 16, 16, 16, 16, 16 }); //9
        stagePattern.Add(new int[] { 16, 16, 15, 16, 16, 16, 16, 16 }); //10
        stagePattern.Add(new int[] { 16, 17, 16, 15, 16, 15, 16, 17 }); //1
        stagePattern.Add(new int[] { 15, 16, 16, 16, 16, 16, 16, 17 }); //2
        stagePattern.Add(new int[] { 16, 17, 16, 15, 15, 16, 15, 17 }); //3
        stagePattern.Add(new int[] { 16, 16, 16, 15, 16, 15, 15, 17 }); //4
        stagePattern.Add(new int[] { 15, 16, 17, 16, 16, 16, 15, 16 }); //15
        stagePattern.Add(new int[] { 16, 17, 16, 15, 15, 16, 15, 16 }); //16
        stagePattern.Add(new int[] { 15, 16, 15, 16, 16, 15, 16, 17 }); //17
        stagePattern.Add(new int[] { 15, 17, 17, 16, 15, 16, 16, 17 }); //8
        stagePattern.Add(new int[] { 15, 16, 15, 16, 16, 15, 16, 17 }); //9
        stagePattern.Add(new int[] { 15, 17, 16, 17, 16, 16, 15, 16 }); //10
        stagePattern.Add(new int[] { 15, 15, 17, 17, 16, 16, 16, 15 }); //1

        stagePattern.Add(new int[] { 15, 16, 16, 17, 16, 16, 16, 16}); //2
        stagePattern.Add(new int[] { 17, 15, 17, 15, 17, 15, 16, 15 }); //3
        stagePattern.Add(new int[] { 16, 16, 16, 16, 16, 17, 16, 16 }); //4
        stagePattern.Add(new int[] { 16, 16, 16, 16, 16, 16, 17, 16 }); //15
        stagePattern.Add(new int[] { 16, 16, 16, 16, 16, 16, 16, 17 }); //16
        stagePattern.Add(new int[] { 16, 16, 16, 17, 17, 16, 16, 16 }); //17
        stagePattern.Add(new int[] { 16, 16, 17, 16, 16, 17, 16, 16 }); //8
        stagePattern.Add(new int[] { 16, 17, 16, 16, 16, 16, 17, 16 }); //9
        stagePattern.Add(new int[] { 17, 16, 16, 16, 16, 16, 16, 17 }); //10


        stagePattern.Add(new int[] { 17, 16, 16, 17, 16, 17, 16, 16 }); //1
        stagePattern.Add(new int[] { 16, 16, 17, 16, 16, 16, 17, 16 }); //2
        stagePattern.Add(new int[] { 16, 16, 16, 17, 16, 16, 16, 17}); //3
        stagePattern.Add(new int[] { 16, 16, 17, 16, 17, 16, 16, 16 }); //4
        stagePattern.Add(new int[] { 16, 17, 16, 16, 16, 17, 17, 16 }); //15
        stagePattern.Add(new int[] { 16, 16, 16, 16, 16, 17, 16, 17 }); //16
        stagePattern.Add(new int[] { 17, 16, 16, 16, 17, 16, 16, 17 }); //17
        stagePattern.Add(new int[] { 16, 16, 16, 17, 16, 16, 16, 17 }); //8
        stagePattern.Add(new int[] { 16, 17, 16, 16, 17, 16, 16, 17 }); //9

        stagePattern.Add(new int[] { 17, 16, 16, 16, 16, 16, 16, 17 }); //10
        stagePattern.Add(new int[] { 17, 17, 16, 17, 16, 17, 16, 17}); //1
        stagePattern.Add(new int[] { 17, 16, 17, 16, 17, 16, 17, 16 }); //2
        stagePattern.Add(new int[] { 17, 17, 17, 16, 17, 16, 17, 16 }); //3
        stagePattern.Add(new int[] { 17, 16, 17, 16, 17, 16, 17, 17 }); //4
        stagePattern.Add(new int[] { 17, 17, 16, 16, 16, 16, 16, 16 }); //15
        stagePattern.Add(new int[] { 17, 16, 17, 16, 16, 16, 16, 17 }); //16
        stagePattern.Add(new int[] { 17, 16, 16, 17, 16, 17, 16, 17 }); //17
        stagePattern.Add(new int[] { 17, 16, 17, 16, 17, 16, 17, 17 }); //8
        stagePattern.Add(new int[] { 17, 16, 17, 17, 17, 17, 16, 17 }); //9
        stagePattern.Add(new int[] { 17, 17, 17, 17, 17, 17, 17, 17 }); //10

        //15 16  17 - 8




        List<int[]> resultStagePattern = new List<int[]>();
        for (int i = 0; i<30; i++) {
            resultStagePattern.Add(stagePattern[i]);
        }

        for (int i = 30; i<49; i++) {
            for (int k = 0; k<2; k++) {
                resultStagePattern.Add(stagePattern[i]);
            }
        }

        for (int i = 50; i<89; i++) {
            for (int k = 0; k<3; k++) {
                resultStagePattern.Add(stagePattern[i]);
            }
        }

        for (int i = 90; i<119; i++) {
            for (int k = 0; k<4; k++) {
                resultStagePattern.Add(stagePattern[i]);
            }
        }

        for(int i =120 ; i< 169; i++){
            for (int k = 0; k<5; k++) {
                resultStagePattern.Add(stagePattern[i]);
            }
        }

        for (int i = 170; i<stagePattern.Count; i++) {
            for (int k = 0; k<5; k++) {
                resultStagePattern.Add(stagePattern[i]);
            }
        }

        Debug.Log("Bonsus ==== > " + stagePattern.Count);
        return resultStagePattern;
    }
}
