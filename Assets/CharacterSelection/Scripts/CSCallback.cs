﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterSelection))]
public class CSCallback : MonoBehaviour {

    CharacterSelection characterSelection;
    public GameObject currentCharacterParent;

	private CharacterControl CharControl;


    public void OnInit()
    {
        characterSelection = GetComponent<CharacterSelection>();
        if (!characterSelection)
        {
            Debug.LogError("Character Selection component missed!");
        }

        print("ZPlayerPrefs.GetInt(Values.characterIndex) : " + PlayerPrefs.GetInt(Values.characterIndex));

        
        characterSelection.startIndex = PlayerPrefs.GetInt(Values.characterIndex);
        //characterSelection.startIndex = 6;

        LoadCharacterList();

        //GameObject current = Instantiate(characterSelection.characters[characterSelection.startIndex].obj);
        //current.transform.parent = currentCharacterParent.transform;
        //current.transform.localPosition = Vector3.zero;
        //current.SetActive(true);
        ChangeCharacter();
		



		Debug.Log("(CSCallback) " + "OnInit: Init Success!");
    }

    public void ChangeCharacter()
    {
        for(int i=0; i<currentCharacterParent.transform.childCount; i++)
        {
            currentCharacterParent.transform.GetChild(i).gameObject.SetActive(false);
        }

        currentCharacterParent.transform.GetChild(characterSelection.startIndex).gameObject.SetActive(true);
    }

    public void OnShowSelector()
    {
        currentCharacterParent.SetActive(false);
        Debug.Log("(CSCallback) " + "OnShowSelector: Selector Showed!");
    }

    public void OnCloseSelector()
    {
        currentCharacterParent.SetActive(true);
        Debug.Log("(CSCallback) " + "OnCloseSelector: Selector Closed!");
    }

    public void OnSwipe(int index, Character character)
    {
        Debug.Log("(CSCallback) " + "OnSwipe: index = " + index + " Character name = " + character.name);
    }

    public void OnClickToSelectCharacter(int index, Character character)
    {
        if (!character.opened)
        {
            //  If the character is not purchased, buy it

            if(PlayerPrefs.GetInt(Values.gem) >= character.price)
            {
                PlayerPrefs.SetInt(Values.gem, PlayerPrefs.GetInt(Values.gem) - (int)character.price);
                characterSelection.OpenCharacter(index);
				
				SaveCharacterList();

                GameController._instance.UpdateGemUI();

            //    Firebase.Analytics.FirebaseAnalytics.LogEvent("BuyCharacter", character.name, character.price);
            }

        }
        else
        {
            //ZPlayerPrefs.SetInt(Values.characterIndex, index);
            //GameController._instance.SetCharacterIndex(index);
            //PlayerPrefs.SetInt(Values.characterIndex, index);

            //  If the character is purchased, select it
            characterSelection.SelectCharacter(index);

            // set current character object
            //Destroy(currentCharacterParent.transform.GetChild(0).gameObject);
            //GameObject current = Instantiate(character.obj);
            //current.transform.parent = currentCharacterParent.transform;
            //current.transform.localPosition = Vector3.zero;
            ChangeCharacter();

			CharControl=currentCharacterParent.GetComponent<CharacterControl>();
			CharControl.CharacterChangeSetup(index);

			Debug.Log("(CSCallback) " + "OnSelect: index = " + index + " Character name = " + character.name);

        }
        
    }

    void SaveCharacterList()
    {
        for (int i = 1; i < characterSelection.characters.Length; i++)
        {
            if (characterSelection.characters[i].opened == true)
            {
                PlayerPrefs.SetInt("CharacterOpen" + i, 1);
            }
        }
    }

    void LoadCharacterList()
    {
        for(int i=1; i<characterSelection.characters.Length; i++)
        {
            if(PlayerPrefs.GetInt("CharacterOpen"+i) == 1)
            {
                characterSelection.characters[i].opened = true;
            }
        }
    }
}
