﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class BuggySuspension : MonoBehaviour {

	private Vector3 wheelTran;
	private Quaternion lastPos;
	private float trunSpeed = 20f;
	public Transform suspensionFL;
	public Transform suspensionFR;
	public Transform wheelFL;
	public Transform wheelFR;
	public Transform suspensionBL;
	public Transform suspensionBR;
	public Transform wheelBL;
	public Transform wheelBR;
	public Transform wheelLook_FL;
	public Transform wheelLook_FR;
	public Transform sus1A_BL;
	public Transform sus2A_BL;
	public Transform sus1A_BR;
	public Transform sus2A_BR;
	public Transform sus1A_FL;
	public Transform sus2A_FL;
	public Transform sus1A_FR;
	public Transform sus2A_FR;

	public Transform scale1A_FR;
	public Transform scale2A_FR;
	public Transform scale1B_FR;
	public Transform scale2B_FR;
	public Transform scale1C_FR;
	public Transform scale2C_FR;
	public Transform scale1D_FR;
	public Transform scale2D_FR;
	public Transform scale1A_FL;
	public Transform scale2A_FL;
	public Transform scale1B_FL;
	public Transform scale2B_FL;
	public Transform scale1C_FL;
	public Transform scale2C_FL;
	public Transform scale1D_FL;
	public Transform scale2D_FL;

	public Transform scale1A_BR;
	public Transform scale2A_BR;
	public Transform scale1B_BR;
	public Transform scale2B_BR;
	public Transform scale1C_BR;
	public Transform scale2C_BR;
	public Transform scale1D_BR;
	public Transform scale2D_BR;
	public Transform scale1A_BL;
	public Transform scale2A_BL;
	public Transform scale1B_BL;
	public Transform scale2B_BL;
	public Transform scale1C_BL;
	public Transform scale2C_BL;
	public Transform scale1D_BL;
	public Transform scale2D_BL;

	private float originalLengthleft1;
	private float originalLengthleft2;
	private float originalLengthleft3;
	private float originalLengthleft4;
	private float originalLengthleft5;
	private float originalLengthleft6;
	private float originalLengthleft7;
	private float originalLengthleft8;
	private float originalLengthleft9;
	private float originalLengthleft10;
	private float originalLengthleft11;
	private float originalLengthleft12;
	private float originalLengthleft13;
	private float originalLengthleft14;
	private float originalLengthleft15;
	private float originalLengthleft16;

	void Start()  {
		originalLengthleft1 = Vector3.Distance(scale1A_BL.position, scale2A_BL.position);
		originalLengthleft2 = Vector3.Distance(scale1B_BL.position, scale2B_BL.position);
		originalLengthleft3 = Vector3.Distance(scale1C_BL.position, scale2C_BL.position);
		originalLengthleft4 = Vector3.Distance(scale1D_BL.position, scale2D_BL.position);
		originalLengthleft5 = Vector3.Distance(scale1A_BR.position, scale2A_BR.position);
		originalLengthleft6 = Vector3.Distance(scale1B_BR.position, scale2B_BR.position);
		originalLengthleft7 = Vector3.Distance(scale1C_BR.position, scale2C_BR.position);
		originalLengthleft8 = Vector3.Distance(scale1D_BR.position, scale2D_BR.position);

		originalLengthleft9 = Vector3.Distance(scale1A_FL.position, scale2A_FL.position);
		originalLengthleft10 = Vector3.Distance(scale1B_FL.position, scale2B_FL.position);
		originalLengthleft11 = Vector3.Distance(scale1C_FL.position, scale2C_FL.position);
		originalLengthleft12 = Vector3.Distance(scale1D_FL.position, scale2D_FL.position);

		originalLengthleft13 = Vector3.Distance(scale1A_FR.position, scale2A_FR.position);
		originalLengthleft14 = Vector3.Distance(scale1B_FR.position, scale2B_FR.position);
		originalLengthleft15 = Vector3.Distance(scale1C_FR.position, scale2C_FR.position);
		originalLengthleft16 = Vector3.Distance(scale1D_FR.position, scale2D_FR.position);
	}
	void FixedUpdate(){

		float newScale1 = Vector3.Distance (scale1A_BL.position, scale2A_BL.position) / originalLengthleft1;
		scale1A_BL.localScale = new Vector3 (1f, 1f, newScale1);
		float newScale2 = Vector3.Distance (scale1B_BL.position, scale2B_BL.position) / originalLengthleft2;
		scale1B_BL.localScale = new Vector3 (1f, 1f, newScale2);
		float newScale3 = Vector3.Distance (scale1C_BL.position, scale2C_BL.position) / originalLengthleft3;
		scale1C_BL.localScale = new Vector3 (1f, 1f, newScale3);
		float newScale4 = Vector3.Distance (scale1D_BL.position, scale2D_BL.position) / originalLengthleft4;
		scale1D_BL.localScale = new Vector3 (1f, 1f, newScale4);

		float newScale5 = Vector3.Distance (scale1A_BR.position, scale2A_BR.position) / originalLengthleft5;
		scale1A_BR.localScale = new Vector3 (1f, 1f, newScale5);
		float newScale6 = Vector3.Distance (scale1B_BR.position, scale2B_BR.position) / originalLengthleft6;
		scale1B_BR.localScale = new Vector3 (1f, 1f, newScale6);
		float newScale7 = Vector3.Distance (scale1C_BR.position, scale2C_BR.position) / originalLengthleft7;
		scale1C_BR.localScale = new Vector3 (1f, 1f, newScale7);
		float newScale8 = Vector3.Distance (scale1D_BR.position, scale2D_BR.position) / originalLengthleft8;
		scale1D_BR.localScale = new Vector3 (1f, 1f, newScale8);

		float newScale9 = Vector3.Distance (scale1A_FL.position, scale2A_FL.position) / originalLengthleft9;
		scale1A_FL.localScale = new Vector3 (1f, 1f, newScale9);
    	float newScale10 = Vector3.Distance (scale1B_FL.position, scale2B_FL.position) / originalLengthleft10;
		scale1B_FL.localScale = new Vector3 (1f, 1f, newScale10);
		float newScale11 = Vector3.Distance (scale1C_FL.position, scale2C_FL.position) / originalLengthleft11;
		scale1C_FL.localScale = new Vector3 (1f, 1f, newScale11);
		float newScale12 = Vector3.Distance (scale1D_FL.position, scale2D_FL.position) / originalLengthleft12;
		scale1D_FL.localScale = new Vector3 (1f, 1f, newScale12);

		float newScale13 = Vector3.Distance (scale1A_FR.position, scale2A_FR.position) / originalLengthleft13;
		scale1A_FR.localScale = new Vector3 (1f, 1f, newScale13);
		float newScale14 = Vector3.Distance (scale1B_FR.position, scale2B_FR.position) / originalLengthleft14;
		scale1B_FR.localScale = new Vector3 (1f, 1f, newScale14);
		float newScale15 = Vector3.Distance (scale1C_FR.position, scale2C_FR.position) / originalLengthleft15;
		scale1C_FR.localScale = new Vector3 (1f, 1f, newScale15);
		float newScale16 = Vector3.Distance (scale1D_FR.position, scale2D_FR.position) / originalLengthleft16;
		scale1D_FR.localScale = new Vector3 (1f, 1f, newScale16);

	}

	void LateUpdate (){
		
		if (sus1A_BL != null && sus2A_BL != null) {
			sus1A_BL.LookAt (sus2A_BL.position, sus1A_BL.up);
			sus2A_BL.LookAt (sus1A_BL.position, sus2A_BL.up);
		}
		if (scale1A_BL != null && scale2A_BL != null) {
			scale1A_BL.LookAt (scale2A_BL.position, scale1A_BL.up);
			scale2A_BL.LookAt (scale1A_BL.position, scale2A_BL.up);
		}
		if (scale1B_BL != null && scale2B_BL != null) {
			scale1B_BL.LookAt (scale2B_BL.position, scale1B_BL.up);
			scale2B_BL.LookAt (scale1B_BL.position, scale2B_BL.up);
		}
		if (scale1C_BL != null && scale2C_BL != null) {
			scale1C_BL.LookAt (scale2C_BL.position, scale1C_BL.up);
			scale2C_BL.LookAt (scale1C_BL.position, scale2C_BL.up);
		}
		if (scale1D_BL != null && scale2D_BL != null) {
			scale1D_BL.LookAt (scale2D_BL.position, scale1D_BL.up);
			scale2D_BL.LookAt (scale1D_BL.position, scale2D_BL.up);
		}

		if (sus1A_BR != null && sus2A_BR != null) {
			sus1A_BR.LookAt (sus2A_BR.position, sus1A_BR.up);
			sus2A_BR.LookAt (sus1A_BR.position, sus2A_BR.up);
		}
		if (scale1A_BR != null && scale2A_BR != null) {
			scale1A_BR.LookAt (scale2A_BR.position, scale1A_BR.up);
			scale2A_BR.LookAt (scale1A_BR.position, scale2A_BR.up);
		}
		if (scale1B_BR != null && scale2B_BR != null) {
			scale1B_BR.LookAt (scale2B_BR.position, scale1B_BR.up);
			scale2B_BR.LookAt (scale1B_BR.position, scale2B_BR.up);
		}
		if (scale1C_BR != null && scale2C_BR != null) {
			scale1C_BR.LookAt (scale2C_BR.position, scale1C_BR.up);
			scale2C_BR.LookAt (scale1C_BR.position, scale2C_BR.up);
		}
		if (scale1D_BR != null && scale2D_BR != null) {
			scale1D_BR.LookAt (scale2D_BR.position, scale1D_BR.up);
			scale2D_BR.LookAt (scale1D_BR.position, scale2D_BR.up);
		}
		if (sus1A_FR != null && sus2A_FR != null) {
			sus1A_FR.LookAt (sus2A_FR.position, sus1A_FR.up);
			sus2A_FR.LookAt (sus1A_FR.position, sus2A_FR.up);
		}
		if (scale1A_FR != null && scale2A_FR != null) {
			scale1A_FR.LookAt (scale2A_FR.position, scale1A_FR.up);
			scale2A_FR.LookAt (scale1A_FR.position, scale2A_FR.up);
		}
		if (scale1B_FR != null && scale2B_FR != null) {
			scale1B_FR.LookAt (scale2B_FR.position, scale1B_FR.up);
			scale2B_FR.LookAt (scale1B_FR.position, scale2B_FR.up);
		}
		if (scale1C_FR != null && scale2C_FR != null) {
			scale1C_FR.LookAt (scale2C_FR.position, scale1C_FR.up);
			scale2C_FR.LookAt (scale1C_FR.position, scale2C_FR.up);
		}
		if (scale1D_FR != null && scale2D_FR != null) {
			scale1D_FR.LookAt (scale2D_FR.position, scale1D_FR.up);
			scale2D_FR.LookAt (scale1D_FR.position, scale2D_FR.up);
		}
		if (sus1A_FL != null && sus2A_FL != null) {
			sus1A_FL.LookAt (sus2A_FL.position, sus1A_FL.up);
			sus2A_FL.LookAt (sus1A_FL.position, sus2A_FL.up);
		}
		if (scale1A_FL != null && scale2A_FL != null) {
			scale1A_FL.LookAt (scale2A_FL.position, scale1A_FL.up);
			scale2A_FL.LookAt (scale1A_FL.position, scale2A_FL.up);
		}
		if (scale1B_FL != null && scale2B_FL != null) {
			scale1B_FL.LookAt (scale2B_FL.position, scale1B_FL.up);
			scale2B_FL.LookAt (scale1B_FL.position, scale2B_FL.up);
		}
		if (scale1C_FL != null && scale2C_FL != null) {
			scale1C_FL.LookAt (scale2C_FL.position, scale1C_FL.up);
			scale2C_FL.LookAt (scale1C_FL.position, scale2C_FL.up);
		}
		if (scale1D_FL != null && scale2D_FL != null) {
			scale1D_FL.LookAt (scale2D_FL.position, scale1D_FL.up);
			scale2D_FL.LookAt (scale1D_FL.position, scale2D_FL.up);
		}
		suspensionFL.position = wheelFL.position;
		suspensionFR.position = wheelFR.position;
		suspensionBL.position = wheelBL.position;
		suspensionBR.position = wheelBR.position;

		wheelTran = wheelLook_FL.position - suspensionFL.position;
		wheelTran.y = 0f;
		lastPos = Quaternion.LookRotation (-wheelTran);
		suspensionFL.rotation = Quaternion.Slerp (suspensionFL.rotation, lastPos, Time.deltaTime * trunSpeed);

		wheelTran = wheelLook_FR.position - suspensionFR.position;
		wheelTran.y = 0f;
		lastPos = Quaternion.LookRotation (-wheelTran);
		suspensionFR.rotation = Quaternion.Slerp (suspensionFR.rotation, lastPos, Time.deltaTime * trunSpeed);
	}
}
